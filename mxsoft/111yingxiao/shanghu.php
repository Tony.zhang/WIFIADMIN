<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=1;  
$PurviewLevel_Others="daili_";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function ConfirmDel()
{
   if(confirm("确定要删除选中的信息吗？"))
     return true;
   else
     return false;
	 
}
function postdo(va){
	document.form2.jobs.value=va
	document.form2.submit();
}

var xmlobj; 
function CreateXMLHttpRequest()
{
	if(window.XMLHttpRequest)
	{//Mozilla浏览器
		xmlobj=new XMLHttpRequest();
		if(xmlobj.overrideMimeType)
		{//设置MIME类别
		xmlobj.overrideMimeType("text/xml");
		}
	}
	else if(window.ActiveXObject)
	{ //IE浏览器
		try
		{xmlobj=new ActiveXObject("Msxml2.XMLHttp");}
		catch(e)
		{
			try
			{xmlobj=new ActiveXobject("Microsoft.XMLHttp");}
			catch(e)
			{}
		}
	}

}

function StatHandler_xucode() { //用于处理状态的函数
	if(xmlobj.readyState == 4 && xmlobj.status == 200) //如果URL成功访问，则输出网页 
	{
	    if (xmlobj.responseText=="1"){
			alert("此代理编号已经存在");
		}
	}
}

function check_daili(sname){
	if (sname!=""){
		CreateXMLHttpRequest(); 
		var showurl = 'check_sname.php?sname='+sname; 
		xmlobj.open("GET", showurl, true);
	
		xmlobj.onreadystatechange = StatHandler_xucode;
		xmlobj.send(null); 
	}
}


function trim(str){   
    str = str.replace(/^(\s|\u00A0)+/,'');   
    for(var i=str.length-1; i>=0; i--){   
        if(/\S/.test(str.charAt(i))){   
            str = str.substring(0, i+1);   
            break;   
        }   
    }   
    return str;   
}  

</SCRIPT>
</head>
<body> 
<?php
if ($actions=="saveadd"){
	if(trim($sname)==""){
		$fun->popmassage("用户名不能为空！","","popback");
		exit();
	}
	$havedl=$db->getinfo("select top 1 * from mx_shanghu  WHERE sname='$sname'");
	if(is_array($havedl)){
		$fun->popmassage("此用户名已经存在！","","popback");
		exit();
	}
	if(trim($spassword)==""){
		$fun->popmassage("登录密码不能为空！","","popback");
		exit();
	}
	if(trim($spassword2)==""){
		  $fun->popmassage("密码确认不能为空！","","popback");
		  exit();
	}
	if ($spassword!=$spassword2){
		$fun->popmassage("两次输入密码不一致，请重新输入！","","popback");
		exit();
	}
	if(trim($srealname)==""){
		  $fun->popmassage("商户名称不能为空！","","popback");
		  exit();
	}

	$flvurlstr=trim($flvurlstr);
	if($flvurlstr==""){
			  $fun->popmassage("至少绑定一台设备","","popback");
			  exit();
	}
	
	$isok=1;
	
  	$files=array(
  	"dailiid"         =>1,
  	"sname"             =>$sname,
  	"spassword"         =>md5($spassword),
  	"srealname"         =>$srealname,
  	"reurl"               =>$reurl,
  	"gotourl"            =>$gotourl,
  	"startdate"         =>"2014-01-01",
  	"enddate"           =>"2014-12-31",
  	"isok"              =>$isok,
  	"createdate"        =>date("Y-m-d H:i:s"),
  	"createid"          =>$_SESSION['mxwifi']['userid'],
  	);
	$db->excu("BEGIN TRANSACTION DEPS02_DEL");
  	$db->exe_insert("mx_shanghu",$files);
	$shangid=$db->getsingle("select @@IDENTITY");
	$flvurlarr=explode("#@@#",$flvurlstr);
	foreach($flvurlarr as $key =>$value){
		$files="";
		$value1=explode("@##@",$value);
		if (strlen($value1[0])!=12){
			  $fun->popmassage("MAC地址填写错误","","popback");
			  exit();
		}
		$ishave="";
		$ishave=$db->getinfo("select * from mx_shebei where rmac='$value1[0]'");
		if (is_array($ishave)){
			  $fun->popmassage("此设备已经被其他商户使用","","popback");
			  exit();
		}
		$files=array(
		"dailiid"             =>1,
		"shanghuid"         =>$shangid,
		"pici"         =>1,
		"gonghuoshid"               =>1,
		"ssid"            =>$value1[1],
		"rmac"                =>$value1[0],
		"wanopen"            =>1,
		"startdate"         =>"2014-01-01",
		"enddate"           =>"2014-12-31",
		"iscandoing"          =>1,
		"isok"          =>1,
		"ischuku"         =>1,
		"isupgrade"          =>1,
		"ischange"          =>1,
		"changestr"         =>"getupgrade",
		"createdate"        =>date("Y-m-d H:i:s"),
		"createid"          =>$_SESSION['mxwifi']['userid'],
		);
		$db->exe_insert("mx_shebei",$files);
	}
	$db->excu("COMMIT TRANSACTION DEPS02_DEL");
	
	jump2("商户添加成功","shanghu.php",2);
	exit();
}
elseif ($actions=="savemodi"){
	$id=intval($id);
	$sex=intval($sex);
	if ($id==0){
		$fun->popmassage("请选择要修改的信息","","popback");
		exit();
	}
	$spassword=trim($spassword);
	$spassword2=trim($spassword2);
	if ($spassword!="" and $spassword2!=""){
		if ($spassword != $spassword2){
			$fun->popmassage("两次输入密码不一致，请重新输入","","popback");
			exit();
		}
	}
	if(trim($srealname)==""){
		  $fun->popmassage("商户名称不能为空！","","popback");
		  exit();
	}
	if(trim($mobile)==""){
		  $fun->popmassage("手机号码不能为空！","","popback");
		  exit();
	}
	if (trim($mobile)!=""){
		if(!eregi("^1[0-9]{10}$",$mobile))
		{
			  $fun->popmassage("手机号码格式不正确","","popback");
			  exit();
		}
	}

	$isok=intval($isok);
  	$files=array(
  	"srealname"         =>$srealname,
  	"tel"               =>$tel,
  	"mobile"            =>$mobile,
  	"qq"                =>$qq,
  	"weixin"            =>$weixin,
  	"province"          =>$province,
  	"city"              =>$city,
  	"devicenum"         =>$devicenum,
  	"mone"              =>dvHTMLEncode($mone),
  	"isok"              =>$isok,
  	"createdate"        =>date("Y-m-d H:i:s"),
  	"createid"          =>$_SESSION['mxwifi']['userid'],
  	);
	if (trim($spassword)!=""){
	$files["spassword"]=md5($spassword);
	}
  	$db->exe_update("mx_shanghu",$files,"id='$id'");

    $url=url_code($url);
	jump2("商户修改成功","$url",2);
    exit();
}
elseif ($actions=="delete"){
	  global $db,$fun;
	  $id=intval($id);
	  if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $shanginfo=$db->getinfo("select top 1 * from mx_shanghu where id='$id'");
	  if (!is_array($shanginfo)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $db->excu("BEGIN TRANSACTION DEPS02_DEL");
	  $db->excu("delete from mx_shanghu where id='$id' ");
	  $db->excu("delete from mx_shebei where shanghuid='$id' ");
	  $db->excu("COMMIT TRANSACTION DEPS02_DEL");
	  $url=$fromurl?$fromurl:$FROMURL;
	  jump2("商户删除成功",$url,1);
	  exit();
}
elseif ($actions=="delall"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	
	$db->excu("BEGIN TRANSACTION DEPS02_DEL");
	foreach( $id as $key=>$value){
	  $shanginfo=$db->getinfo("select top 1 * from mx_shanghu where id='$key'");
	  if (!is_array($shanginfo)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $db->excu("delete from mx_shanghu where id='$key' ");
	  $db->excu("delete from mx_shebei where shanghuid='$key' ");
	}
	$db->excu("COMMIT TRANSACTION DEPS02_DEL");
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("商户删除成功",$url,1);
	exit();
}

require("../mx_head.php");?>
<?php
  if ($actions=="add"){
?>
<script language="javascript">
   var pis=1;
   function addblog(pi){
		var vs = new Array(); //预存值对象
		//获取 xx DIV 中的所有 INPUT 标签元素
		var inputs = document.getElementById("bblog").getElementsByTagName("input");
		//遍历存储其 value
		for (var i = 0; i < inputs.length; i++) {
		　　vs[i] = inputs[i].value;
		}
	var str='<table width="100%" height="30" cellpadding="0" cellspacing="0" align="center" id="daili'+pi+'"><tr><td>MAC:<input type="text" name="flvurl_j['+pi+']" id="flvurl_j'+pi+'" size="12" />&nbsp;SSID:<input type="text" name="flvurl['+pi+']" id="flvurl'+pi+'" size="30" /><span style="cursor:pointer;  margin-left:10px;" onclick="javascript:delblog('+pi+')">删除</span></td></tr></table>';
	document.getElementById('bblog').innerHTML=document.getElementById('bblog').innerHTML+str;
		inputs = document.getElementById("bblog").getElementsByTagName("input");
		//为其赋原先的值
		for (var i = 0; i < vs.length; i++) {
		　　inputs[i].value = vs[i];
		}
	pi++;
	pis=pi;
   }
   function delblog(i){
   document.getElementById("bblog").removeChild(document.getElementById('daili'+i));
   }

setFormName("form1");
function CheckAdd(){
	if (!CheckLength("sname","商户编号"))
		return false;
	
	
	if (!CheckLength("spassword","登录密码"))
		return false;
	if (!CheckLength("spassword2","密码确认"))
		return false;
	if(document.form1.spassword.value!=document.form1.spassword2.value){
		alert("两次输入的密码不一致！");
		document.form1.spassword2.focus();
		return false;
	}
	if (!CheckLength("srealname","商户名称"))
		return false;
//	if (!CheckLength("ssid","SSID"))
//		return false;
//	if (!CheckNum2("reurl","认证页面"))
//		return false;
//		
//	if (!CheckNum2("gotourl","跳转页面"))
//		return false;

	var flvurlstr="";
	var vs = new Array();
	var inputs = document.getElementById("bblog").getElementsByTagName("input");
	var ishaveji=false;
	var jishu_i=0;
	for (var i = 0; i < inputs.length; i+=2) {
		var nexti=i+1;
		if (trim(inputs[nexti].value)!=""){
			jishu_i++;
			if ((trim(inputs[i].value)=="" && ishaveji==true) || (trim(inputs[i].value)!="" && ishaveji==false && i>0)){
				alert("设备配置填写不完整");
				return false;
			}
			if (trim(inputs[i].value)!=""){
				jishu_i=inputs[i].value;
				ishaveji=true;
			}
			if (flvurlstr==""){flvurlstr=jishu_i+"@##@"+trim(inputs[nexti].value);}
			else{flvurlstr+="#@@#"+jishu_i+"@##@"+trim(inputs[nexti].value);}
		}
	}
	if(flvurlstr==""){
		alert("必须配置至少一个设备");
		return false;
	}
	document.form1.flvurlstr.value=flvurlstr;
		
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">新增商户</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="shanghu.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">用&nbsp;户&nbsp;名：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="sname" id="sname" class="xtgk5" onblur="check_daili(this.value)" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">登陆密码：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="spassword" id="spassword" class="xtgk5" /></td>
          <td align="right" bgcolor="#eff7ff">密码确认：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="spassword2" id="spassword2" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">商户名称（单位）</td>
          <td  bgcolor="#FFFFFF" colspan="3"><input type="text" name="srealname" id="srealname" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">认证页面：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="reurl" id="reurl" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">跳转页面：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="gotourl" id="gotourl" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="center" bgcolor="#eff7ff" colspan="4">设备配置</td>
        </tr>
		<tr bgcolor="#FFFFFF"><td colspan="4">
<div style="width:500px; margin:auto;">
  <div id="bblog">
  <table width="100%" height="30" cellpadding="0" cellspacing="0" align="center">
  <tr><td>MAC:<input type="text" name="flvurl_j[0]" id="flvurl_j" size="12"/>&nbsp;SSID:<input type="text" name="flvurl[0]" id="flvurl" size="30" /></td></tr>
  </table>
  </div>
  
  <table width="100%" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td><input type="button" name="tj" value="添加" onclick="addblog(pis)" /><input type="hidden" name="flvurlstr" id="flvurlstr" /></td>
  </tr>
  </table>
</div>
		</td></tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="actions" value="saveadd" />	
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="modipurview"){
//修改管理员
  global $db,$fun;
  $id=trim($id);
  if ($id==0){
  	$fun->popmassage("此信息不存在！","","popback");
	exit();
  }
  $rsdb=$db->getinfo("select top 1 * from mx_shanghu where id='$id'");
  if (!is_array($rsdb)){
    $fun->popmassage("此信息不存在！","","popback");
    exit();
  }
  
  $isokdb[intval($rsdb[isok])]=' checked ';
  if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("sname","商户编号"))
		return false;
	
	
	if (document.form1.dpassword.value!="" || document.form1.dpassword2.value!=""){
		if (!CheckLength("dpassword","登录密码"))
			return false;
		if (!CheckLength("dpassword2","密码确认"))
			return false;
		if(document.form1.dpassword.value!=document.form1.dpassword2.value){
			alert("两次输入的密码不一致！");
			document.form1.dpassword2.focus();
			return false;
		}
	}
	
	if (!CheckLength("srealname","商户名称"))
		return false;
	if (!CheckLength("ssid","SSID"))
		return false;
	if (!CheckNum2("reurl","认证页面"))
		return false;
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改商户</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="shanghu.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">商户编号：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo $rsdb["sname"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">登陆密码：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="spassword" id="spassword" class="xtgk5" /></td>
          <td align="right" bgcolor="#eff7ff">密码确认：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="spassword2" id="spassword2" class="xtgk5" /></td>
        </tr>
        <tr>
          <td colspan="4" bgcolor="#FFFFFF" align="center" style="color:#FF0000;">密码不修改请留空</td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">商户名称（单位）</td>
          <td  bgcolor="#FFFFFF" colspan="3"><input type="text" name="srealname" id="srealname" class="xtgk5" value="<?php echo trim($rsdb["srealname"]);?>" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">SSID：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="ssid" id="ssid" class="xtgk5" value="<?php echo trim($rsdb["ssid"]);?>" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">认证页面：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="reurl" id="reurl" class="xtgk5" value="<?php echo trim($rsdb["reurl"]);?>" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">跳转页面：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="gotourl" id="gotourl" class="xtgk5" value="<?php echo trim($rsdb["gotourl"]);?>" /></td>
        </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="url" value="<?php echo $gotourl?>">
			<input type="hidden" name="id" value="<?php echo $rsdb[id]?>">
			<input type="hidden" name="actions" value="savemodi">
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="view"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("此商户信息不存在！","","popback");
		exit();
	}
	$rsdb=$db->getinfo("select top 1 * from mx_shanghu where id='$id'");
	if (!is_array($rsdb)){
		$fun->popmassage("此商户信息不存在！","","popback");
		exit();
	}
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">商户信息</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="shanghu.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">商户编号：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo $rsdb["sname"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">商户名称（单位）</td>
          <td  bgcolor="#FFFFFF"><?php echo trim($rsdb["srealname"]);?></td>
          <td  align="right" bgcolor="#EFF7FF">联系电话：</td>
          <td  bgcolor="#FFFFFF"><?php echo trim($rsdb["tel"]);?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">手机：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["mobile"]);?></td>
          <td align="right" bgcolor="#EFF7FF">QQ：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["qq"]);?></td>
        </tr>
        <tr style="display:none;">
          <td align="right" bgcolor="#eff7ff">代理区域</td>
          <td colspan="3" bgcolor="#FFFFFF">
            <select name="select">
              <option>北京市</option>
              <option>天津市</option>
              <option selected="selected">山东省</option>
              <option>广东省</option>
              <option>新疆维吾尔自治区</option>
            </select>
            <select name="select2">
              <option>济南市</option>
              <option>章丘市</option>
            </select>
            <select name="select3">
              <option>历城区</option>
              <option>历下区</option>
            </select></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">设备总数量：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["devicenum"]);?></td>
          <td align="right" bgcolor="#eff7ff">已使用设备：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["usenum"]);?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">未使用设备：</td>
          <td bgcolor="#FFFFFF" colspan="3"><?php echo trim($rsdb["unusenum"]);?></td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">备注信息：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo $rsdb["mone"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">商户状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <?php if ($rsdb["isok"]==1){echo "正常";}else{echo "锁定";}?>
		  </td>
		</tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
		  <input type="button" name="back" value=" 返 回 " class="n_button" onclick="history.back();">
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">商户列表</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="shanghu.php">商户管理</a></div>
		<div class="an2"><a href="shanghu.php?actions=add">添加商户</a></div>
		</td>
		</tr>
		</table>
	</div>
      </div>
	<?php
  $str="";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }

  if ($sname!=""){
	$str.=" and sname like '%$sname%' ";
	$s.="sname=$sname&";
  }
  if ($srealname!=""){
	$str.=" and srealname like '%$srealname%' ";
	$s.="srealname=$srealname&";
  }
  
  if ($mobile!=""){
	$str.=" and mobile like '%$mobile%' ";
	$s.="mobile=$mobile&";
  }
  if($memberpur!=""){
  	$memberpur=intval($memberpur);
	$str.=" and Purview='$memberpur' ";
	$s.="memberpur=$memberpur&";
  }
  
  if ($minnum!=""){
  	$minnum=intval($minnum);
	$str.=" and devicenum>='%$minnum%' ";
	$s.="minnum=$minnum&";
  }
  if ($maxnum!=""){
  	$minnum=intval($minnum);
	$str.=" and devicenum<='%$maxnum%' ";
	$s.="maxnum=$maxnum&";
  }
  if ($isok!=""){
  	$isok=intval($isok);
	$str.=" and isok='$isok' ";
	$s.="isok=$isok&";
  }
  
  $table=" mx_shanghu ";
  $order=" order by createdate desc, id desc";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);

 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2">
      <tr bgcolor="#eff7ff">
        <td width="5%" align="center" bgcolor="#eff7ff">选择</td>
        <td width="15%" align="center" bgcolor="#eff7ff">编号</td>
        <td width="15%" align="center" bgcolor="#eff7ff">姓名（单位）</td>
        <td width="15%" align="center" bgcolor="#eff7ff">手机号码</td>
        <td width="10%" align="center" bgcolor="#eff7ff">总设备数</td>
        <td width="10%" align="center" bgcolor="#eff7ff">已使用</td>
        <td width="10%" align="center" bgcolor="#eff7ff">未使用</td>
        <td width="10%" align="center" bgcolor="#eff7ff">状态</td>
        <td width="15%" align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="shanghu.php">
  <?PHp
  if (!is_array($list) or $c==0){
  echo "<tr class=\"daili1\"><td colspan=9><center>暂无信息</center></td></tr>";
  }
  else{
  for($i=0;$i<count($list);$i++){
	$list[$i][isok]=$list[$i][isok]?"<A HREF='?actions=noisok&id=".$list[$i]["id"]."' style='color:red;' title='锁定用户'>正常</A>":"<A HREF='?actions=isok&id=".$list[$i]["id"]."' style='color:blue;' title='设为正常'>锁定</A>";
  ?>
  <tr class="daili1" align="center"> 
    <td width="5%"><input type="checkbox" name="id[<?php echo $list[$i]["id"] ?>]" id="id" value="<?php echo $list[$i]["id"] ?>" class="input_check"></td>
    <td><?php echo $list[$i]["sname"] ?></td>
    <td><?php echo $list[$i]["srealname"] ?></td>
    <td><?php echo $list[$i]["mobile"] ?></td>
    <td><?php echo (float)$list[$i]["devicenum"] ?></td>
    <td><?php echo (float)$list[$i]["usenum"] ?></td>
    <td><?php echo (float)$list[$i]["unusenum"] ?></td>
    <td><?php echo $list[$i]["isok"] ?></td>
    <td><!--<a href="shanghu.php?actions=modipurview&id=<?php //echo $list[$i]["id"] ?>">修改</a>&nbsp;&nbsp;--><a href="shanghu.php?actions=delete&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要删除此商户吗?')">删除</a></td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php if ($c>0){?>
      <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a href="javascript:CheckAll(document.form1,'')">反选</a></td>
          <td width="19"><img src="../images/del.jpg" width="14" height="14" /></td>
          <td><a href="javascript:if (confirm('确定要删除选中的信息吗？')){postdo('delall');}"/>删除选中</a></td>
        </tr>
      </table>
	  <?php }
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>
<?php 
  }

?>

<?php require("../mx_foot.php");?>
</body>
</html>
