<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=1;  
$PurviewLevel_Others="daili_";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<script language="javascript" src="../js/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript">
$(function(){
	$("#sub").click(function(){
		var title = $("#title").val();
		if (title == ''){
			alert ("标题不能为空！");
			return false;
		}	
	})
})
</SCRIPT>
</head>
<body> 
<?php
require("../mx_head.php");
if ($actions=="saveadd"){
	if(trim($title)==""){
		$fun->popmassage("标题不能为空！","","popback");
		exit();
	}
  	$files=array(
  	"title"             =>$title,
  	"type"              =>$type,
  	"ncontent"          =>$ncontent,
  	"state"             =>$state,
  	"adddatetime"       =>date("Y-m-d H:i:s"),
  	"modidatetime"      =>date("Y-m-d H:i:s"),
  	"adminid"           =>$_SESSION['mxwifi']['userid']
  	);
  	$db->exe_insert("mx_notice",$files);
	jump2("公告发布成功","notice.php",2);
	exit();
}
elseif ($actions=="savemodi"){
	if(trim($id)==""){
		$fun->popmassage("参数错误！","","popback");
		exit();
	}
	if(trim($title)==""){
		$fun->popmassage("标题不能为空！","","popback");
		exit();
	}
  	$files=array(
  	"title"             =>$title,
  	"type"              =>$type,
  	"ncontent"          =>$ncontent,
  	"state"             =>$state,
  	"modidatetime"      =>date("Y-m-d H:i:s")
  	);
  	$db->exe_update("mx_notice",$files,"id='$id'");
	jump2("公告修改成功","notice.php",2);
    exit();
}elseif ($actions=="delete"){
	  $id=intval($id);
	  if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $db->excu("delete from mx_notice where id='$id' ");
	  jump2("公告删除成功","notice.php",1);
	  exit();
}elseif ($actions=="delall"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("delete from mx_notice where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("通知删除成功",$url,1);
	exit();
}elseif ($actions=="add"){
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/264.gif" width="16" height="16" /></td>
        <td class="xtgk1">发布通知</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form method="post" action="notice.php">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff" width="15%">标题：</td>
          <td bgcolor="#FFFFFF" width="85%"><input type="text" name="title" id="title" class="xtgk5"/></td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">公告内容：</td>
          <td bgcolor="#FFFFFF">
            		<script charset="utf-8" src="/webedito/kindeditor-min.js"></script>
                    <script charset="utf-8" src="/webedito/lang/zh_CN.js"></script>
                    <script>
                    var editor;
                    KindEditor.ready(function(K) {
                          editor = K.create('textarea[id="ncontent"]', {
                          allowFileManager : true,
                          filterMode : false,
                          afterBlur : function() {this.sync();}
                        });
                    });
                    </script>
                    <textarea name="ncontent" id="ncontent" style="width:90%;height:300px;visibility:hidden;display:none;"></textarea></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">发布状态：</td>
          <td bgcolor="#FFFFFF">
		  <input type="radio" name="state" value="1" class="input_check" checked="checked"/>&nbsp;发布&nbsp;<input type="radio" name="state" value="0" class="input_check"/>&nbsp;不发布
          </tr>
        <tr>
          <td colspan="2" align="center" bgcolor="#FFFFFF">
			<input type="hidden" name="actions" value="saveadd" />	
			<input type="hidden" name="type" value="1" />	
            <input type="image" name="imageField" src="../images/dls_1.jpg" id="sub" />
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
}elseif ($actions=="modi"){
	$id=trim($id);
	if ($id==0){
		$fun->popmassage("此信息不存在！","","popback");
		exit();
	}
	$rsdb=$db->getinfo("select top 1 * from mx_notice where id='$id'");
	if (!is_array($rsdb)){
		$fun->popmassage("此信息不存在！","","popback");
		exit();
	}
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/264.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改通知</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form method="post" action="notice.php">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff" width="15%">标题：</td>
          <td bgcolor="#FFFFFF" width="85%"><input type="text" name="title" id="title" class="xtgk5" value="<?php echo $rsdb['title'];?>" /></td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">公告内容：</td>
          <td bgcolor="#FFFFFF">
            		<script charset="utf-8" src="/webedito/kindeditor-min.js"></script>
                    <script charset="utf-8" src="/webedito/lang/zh_CN.js"></script>
                    <script>
                    var editor;
                    KindEditor.ready(function(K) {
                          editor = K.create('textarea[id="ncontent"]', {
                          allowFileManager : true,
                          filterMode : false,
                          afterBlur : function() {this.sync();}
                        });
                    });
                    </script>
                    <textarea name="ncontent" id="ncontent" style="width:90%;height:300px;visibility:hidden;display:none;"><?php echo $rsdb['ncontent'];?></textarea></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">发布状态：</td>
          <td bgcolor="#FFFFFF">
		  <input type="radio" name="state" value="1" class="input_check" <?php if($rsdb['state']){?>checked="checked"<?php }?> />&nbsp;发布&nbsp;<input type="radio" name="state" value="0" class="input_check" <?php if(!$rsdb['state']){?>checked="checked"<?php }?> />&nbsp;不发布
          </tr>
        <tr>
          <td colspan="2" align="center" bgcolor="#FFFFFF">
			<input type="hidden" name="actions" value="savemodi" />	
			<input type="hidden" name="type" value="<?php echo $rsdb['type'];?>" />	
			<input type="hidden" name="id" value="<?php echo $rsdb['id'];?>" />	
            <input type="image" name="imageField" src="../images/dls_1.jpg" id="sub" />
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="read"){
	$id=trim($id);
	if ($id==0){
		$fun->popmassage("此信息不存在！","","popback");
		exit();
	}
	$rsdb=$db->getinfo("select top 1 * from mx_notice where id='$id'");
	if (!is_array($rsdb)){
		$fun->popmassage("此信息不存在！","","popback");
		exit();
	}
	$author = $db->getsingle("select username from mx_members where uid = '".$rsdb['adminid']."'");
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/264.gif" width="16" height="16" /></td>
        <td class="xtgk1">公告信息</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="center" valign="middle" bgcolor="#eff7ff"><?php echo trim($rsdb["title"]);?><br /><span style="font-size:12px;">作者：<?php echo $author;?>&nbsp;&nbsp; 发布时间：<?php echo $rsdb["adddatetime"];?></span></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td align="left" valign="top" bgcolor="#FFFFFF"><?php echo $rsdb["ncontent"];?></td>
          </tr>
        <tr>
          <td align="center" bgcolor="#FFFFFF">
		  <input type="button" name="back" value=" 返 回 " class="n_button" onclick="history.back();">
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
}else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/264.gif" width="16" height="16" /></td>
        <td class="xtgk1">通知列表</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="notice.php">通知管理</a></div>
		<div class="an2"><a href="notice.php?actions=add">发布通知</a></div>
		<div class="an2"><a href="notice.php?state=1">已发布</a></div>
		<div class="an2"><a href="notice.php?state=0">未发布</a></div>
		</td>
		</tr>
		</table>
	</div>
      <table border="0" align="left" cellpadding="3" cellspacing="0">
	<form id="search_form" name="search_form" method="get" action="notice.php">
        <tr>
        <td>&nbsp;关键词：</td>
        <td><input name="skeyword" type="text" id="skeyword" value="<?php echo $skeyword;?>" size="16" class="search_input"/></td>
      	<td>&nbsp;发布日期：</td>
        <td><input name="mindate" type="text" id="mindate" value="<?php echo $mindate;?>" size="8" class="search_input" style="width:80px;"/></td>
        <td> 至 </td>
        <td><input name="maxdate" type="text" id="maxdate" value="<?php echo $maxdate;?>" size="8" class="search_input" style="width:80px;"/></td>
	  <script language="javascript" type="text/javascript">
	  Calendar.setup({
	  	  inputField     :    "mindate",
	  	  ifFormat       :    "%Y-%m-%d",
	  	  showsTime      :    false,
	  	  timeFormat     :    "24"
	  });
	  Calendar.setup({
	  	  inputField     :    "maxdate",
	  	  ifFormat       :    "%Y-%m-%d",
	  	  showsTime      :    false,
	  	  timeFormat     :    "24"
	  });
	  </script>
      	<td>&nbsp;发布状态：</td>
      	<td>
		<select name="state" id="state">
			<option value="" <?php if ($state == ""){echo "selected";}?>>不限</option>
			<option value="1" <?php if ($state == 1){echo "selected";}?>>已发布</option>
			<option value="0" <?php if ($state == 0){echo "selected";}?>>未发布</option>
		</select>
		</td>
      	<td><input type="image" name="imageField" src="../images/search.jpg" /></td>
        </tr>
	 </form>
      </table>
	 </div>
<?php
  $str=" and type = 1";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }

  if ($skeyword!=""){
	$str.=" and (title like '%$skeyword%' or ncontent like '%$skeyword%')";
	$s.="skeyword=$skeyword&";
  }
  if ($mindate!=""){
  	$mindate=formatdate($mindate);
	$str.=" and adddatetime>='$mindate 00:00:00' ";
	$s.="mindate=$mindate&";
  }
  if ($maxdate!=""){
  	$maxdate=formatdate($maxdate);
	$str.=" and adddatetime<='$maxdate 23:59:59' ";
	$s.="maxdate=$maxdate&";
  }
  if ($state!=""){
  	$state=intval($state);
	$str.=" and state='$state' ";
	$s.="state=$state&";
  }
  
  $table=" mx_notice ";
  $order=" order by adddatetime desc, id desc";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);

 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2">
      <tr bgcolor="#eff7ff">
        <td align="center" bgcolor="#eff7ff">选择</td>
        <td align="center" bgcolor="#eff7ff">标题</td>
        <td align="center" bgcolor="#eff7ff">发布状态</td>
        <td align="center" bgcolor="#eff7ff">发布时间</td>
        <td align="center" bgcolor="#eff7ff">最后修改</td>
        <td align="center" bgcolor="#eff7ff">发布人</td>
        <td  align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="notice.php">
  <?PHp
  if (!is_array($list) or $c==0){
  	echo "<tr class=\"daili1\"><td colspan=7><center>暂无信息</center></td></tr>";
  }else{
  for($i=0;$i<count($list);$i++){
	  $author = $db->getsingle("select username from mx_members where uid = '".$list[$i]['adminid']."'");
  ?>
  <tr class="daili1" align="center"> 
    <td><input type="checkbox" name="id[<?php echo $list[$i]["id"] ?>]" id="id" value="<?php echo $list[$i]["id"] ?>" class="input_check"></td>
    <td align="left"><a href="notice.php?actions=read&id=<?php echo $list[$i]["id"]; ?>"><?php echo $list[$i]["title"] ?></a></td>
    <td><?php if(trim($list[$i]["state"])){echo "已发布";}else{echo "未发布";}?></td>
    <td><?php echo $list[$i]["adddatetime"];?></td>
    <td><?php echo $list[$i]["modidatetime"];?></td>
    <td><?php echo $author;?></td>
    <td><?php if($_SESSION['mxwifi']['userid'] == $list[$i]['adminid']){?><a href="notice.php?actions=modi&id=<?php echo $list[$i]["id"] ?>">修改</a>&nbsp;&nbsp;<a href="notice.php?actions=delete&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('确定要删除此信息吗?')">删除</a><?php }?></td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php if ($c>0){?>
      <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a href="javascript:CheckAll(document.form1,'')">反选</a></td>
          <td width="19"><img src="../images/del.jpg" width="14" height="14" /></td>
          <td width="80"><a href="javascript:if (confirm('确定要删除选中的信息吗？')){postdo('delall');}"/>删除选中</a></td>
          <td></td>
        </tr>
      </table>
	  <?php }
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>
<?php 
  }

?>

<?php require("../mx_foot.php");?>
</body>
</html>
