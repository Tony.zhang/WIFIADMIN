var formName = 0;	

function setFormName(theFormName)
{
	formName = theFormName;
}


function CheckNull(val)
{
	var str = Trim(document.forms[formName].elements[val].value);
	if ( str.length > 0 )
		return false;
	else
		return true;
}


function CheckLength(val,name,maxlen)
{
	if (name == null || name == '')
		name = "此项";
	else
		name = "“" + name + "” ";
    var str = Trim(document.forms[formName].elements[val].value);

    if ( str == "" )
	{
        fhint(name + "不可为空！");
        document.forms[formName].elements[val].focus();
        return false;
	} else if(str!="" && maxlen!=null)
	{
        if (str.length>maxlen)
		{
            fhint(name + "信息超长(现有" + str.length + "字符)，最多可输入" + maxlen +"个字符，请重新输入！");
            document.forms[formName].elements[val].focus();
            document.forms[formName].elements[val].select();
            return false;
        } 
    }
    document.forms[formName].elements[val].value = str;
    return true;
}


function CheckSelect(val,name,n,hintstr)
{
	if (n == null)
		n = 0;
	if (hintstr == null) {
		if (name == null)
			str = "此项不可为空！";
		else
			str = "请选择“" + name + "”！";
	}
	else
		str = hintstr;

   var value = Trim(document.forms[formName].elements[val].value);

    if ( value == "" || value < n)
	{
        fhint(str);
        document.forms[formName].elements[val].focus();
        return false;
	}
	return true;
}


function CheckName(val)
{
	if(!CheckNull(val))
	{
	    var input = document.forms[formName].elements[val].value;
        var charset = "0123456789";
        if (!CheckChar(charset, input, false))
		{
      	    fhint ("请正确输入姓名！不能包含数字");
            document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
            return false;          
        }
    }
    return true;
}


function CheckNum(val,name)
{
	if(!CheckNull(val))
	{
		if (name == null)
			str = "请输入数字！";
		else
			str = "“" + name + "” 应为数字！";
		if(isNaN(document.forms[formName].elements[val].value))	{
		   fhint (str);
		   document.forms[formName].elements[val].focus();
		   document.forms[formName].elements[val].select();
		   return false;
		}
	}
	return true;
}


function CheckNum2(val,name,minvalue,maxvalue)
{
	if(!CheckNull(val))
	{
		if (name == null)
			str = "请输入正整数！";
		else
			str = "“" + name + "” 应为正整数！";

		var input = document.forms[formName].elements[val].value;
		var charset = "1234567890";
		if (!CheckChar(charset, input, true)) {
			fhint (str);
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		if (minvalue != null && input < minvalue) {
			fhint ("“" + name + "” 的值不能小于 " + minvalue + "！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		if (maxvalue != null && input > maxvalue) {
			fhint ("“" + name + "” 的值不能大于 " + maxvalue + "！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
	}
	return true;
}
    

function CheckPhone(val)
{
	if(!CheckNull(val))
	{
		var input = document.forms[formName].elements[val].value;
		if (input.length < 3)
		{
			fhint ("电话号码不应少于 3 位！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		if (input.length > 20)
		{
			fhint ("电话号码不应大于 20 位！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		var charset = "1234567890-()";
		if (!CheckChar(charset, input, true)) 
		{
		   fhint ("请正确输入电话号码，只可包含 “ - ” “ () ” 和  数字！");
		   document.forms[formName].elements[val].focus();
		   document.forms[formName].elements[val].select();
		   return false;      
		}
	}
   return true;
}
    
function CheckMobile(val)
{
	if(!CheckNull(val))
	{
		var input = document.forms[formName].elements[val].value;
    	regExp = /^[0-9]{11,11}$/ ;
		if (!regExp.test (input) )
		{
			fhint ("手机号码格式错误！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
	}
   return true;
}
    

function CheckFax(val)
{
	if(!CheckNull(val))
	{
		var input = document.forms[formName].elements[val].value;
		if (input.length < 3)
		{
			fhint ("传真号码不应少于 3 位！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		if (input.length > 20)
		{
			fhint ("传真号码不应大于 20 位！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		var charset = "1234567890-()";
		if (!CheckChar(charset, input, true)) 
		{
		   fhint ("请正确输入传真号码，只可包含 “ - ” “ () ” 和  数字！");
		   document.forms[formName].elements[val].focus();
		   document.forms[formName].elements[val].select();
		   return false;      
		}
	}
   return true;
}
    

function CheckZip(val)
{
	if(!CheckNull(val))
	{
		var input = document.forms[formName].elements[val].value;
		if (input.length != 6)
		{
			fhint ("邮政编码的长度应该为 6 位！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		var charset = "1234567890";
		if (!CheckChar(charset, input, true)) 
		{
		   fhint ("请正确输入邮政编码，只可包含数字！");
		   document.forms[formName].elements[val].focus();
		   document.forms[formName].elements[val].select();
		   return false;      
		}
	}
   return true;
}
    
    
function CheckID(val)
{
	if(!CheckNull(val))
	{
		var input = document.forms[formName].elements[val].value;
		var reg = /^([0-9X]{15}|[0-9X]{18})$/;
		var flag = reg.test(input);
		if(!flag){
		    alert("请输入合法的身份证号码");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
		    return false;
		}
	}
    return true;
}
  
  
function CheckMoney(val,num)
{
	if(!CheckNull(val))
	{
		var input = document.forms[formName].elements[val].value;
		var pos1 = input.indexOf(".");
		var pos2 = input.lastIndexOf(".");
		var charset = "1234567890.";
		if ((pos1 != pos2)||(!CheckChar(charset, input, true)))
		{
			fhint("请输入金额，只可包含数字和一个“.”！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
		if(num==null || num=="")
		{
			var output = eval((Math.round(input*100))/100);
			document.forms[formName].elements[val].value=output;
		}
		else
		{
			var i_exp=Math.pow(10,num)
			var output = eval((Math.round(input*i_exp))/i_exp);
			document.forms[formName].elements[val].value=output;
		}
	}
    return true;
}


function CheckMail(val)
{
	if(!CheckNull(val))
	{
		var str	 = document.forms[formName].elements[val].value;
		var re = /^[_\.0-9a-z-]+@([0-9a-z-]+\.)+[a-z]{2,3}$/i;
		if (!re.test(str)) 
		{
			fhint("请输入合法的电子邮件地址！");
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
	}
    return true;
}


function CheckURL(val,name)
{
	if(!CheckNull(val))
	{
		var str	 = document.forms[formName].elements[val].value;
		var re = /^http:\/\/[^/]/i;	
		if (!re.test(str)) 
		{
			if (name == null)
				hstr = "请输入正确的WEB地址！";
			else
				hstr = "“" + name + "”应输入正确的WEB地址！";
			fhint(hstr);
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
	}
    return true;
}


function CheckUnURL(val,name)
{
	if(!CheckNull(val))
	{
		var str	 = document.forms[formName].elements[val].value;
		var re = /^http:\/\/[^/]/i;
		if (re.test(str)) 
		{
			if (name == null)
				hstr = "请不要输入WEB地址！";
			else
				hstr = "“" + name + "”不支持WEB地址！";
			fhint(hstr);
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		}
	}
    return true;
}

function CheckPwd(val1,val2)
{
	if(!CheckNull(val1) || !CheckNull(val2))
	{
		var pwd1=document.forms[formName].elements[val1].value; 
		var pwd2=document.forms[formName].elements[val2].value;
		if (pwd1.length<4){
			fhint('密码长度必须大于4');
			document.forms[formName].elements[val1].focus();
			return false;
		}
		if (pwd2.length<4){
			fhint('密码确认长度必须大于4');
			document.forms[formName].elements[val2].focus();
			return false;
		}
		if (pwd1!=pwd2) {
			fhint("两次密码输入不一致！");
			document.forms[formName].elements[val1].focus();
			document.forms[formName].elements[val1].value="";
			document.forms[formName].elements[val2].value=""
			return false;
		}
	}
    return true;
}


function CheckTheBox(fldid,name)
{
	if (name == null)
		msg = "未选择记录！";
	else
		msg = "请选择“" + name + "”！";
	var tmpint;
	var fld = eval("document.all."+fldid);
	var i,j=0;

	if (fld)
	{
		tmpint = eval("document.all."+fldid+".length");
		if(tmpint==null)
			tmpint=1;
    
		for(i=0;i<tmpint;i++)
		{
			if (parseInt(tmpint)!=1)
			{
				if(fld[i].checked)
					j++;
			}
			else
			{
				if(fld.checked && fld.value.indexOf("{") < 0)
					j=1;
			}
		}
		if (j==0)
		{
			hint(msg);
			return false;
		}
	}
	else
	{
		hint("未选择记录或系统内部发生错误！");
		return false;
	}
	return true;
}

function CheckYear(val, mini_year)
{
 	if(!CheckNull(val))
	{
	   if(mini_year == null)
			mini_year = 1900;
		var the_val = document.forms[formName].elements[val];
		var the_year = the_val.value;
		var charset = "1234567890";
		if (! CheckChar(charset, the_year, true)){
			fhint('年份应为数字！');
			the_val.focus();
			the_val.select();
			return false;
		}
		if (the_year.length != 4){
			fhint('年份必须为4位数字！');
			the_val.focus();
			the_val.select();
			return false;
		}
		if ( the_year < mini_year){
			fhint("年份不得小于 " + mini_year +"！");
			the_val.focus();
			the_val.select();
			return false;
		}
	}
    return true;
}


function CheckMonth(val)
{
 	if(!CheckNull(val))
	{
		var the_val = document.forms[formName].elements[val];
		var the_month = the_val.value;
		var charset = "1234567890";
		if (! CheckChar(charset, the_month, true)){
			fhint('月份应为数字！');
			the_val.focus();
			the_val.select();
			return false;
		}
		if (the_month.length > 2){
			fhint('月份不能大于2位！');
			the_val.focus();
			the_val.select();
			return false;
		}else if (the_month.length == 1){
			the_month = '0'+the_month;
		}
		if (the_month < 01 || the_month > 12){
			fhint("请输入正确的月份！")
			the_val.focus();
			the_val.select();
		   return false;
		}
	}
    return true;
}


function CheckDay(val)
{
	if(!CheckNull(val))
	{
		var the_val = document.forms[formName].elements[val];
		var the_day = the_val.value;
		var charset = "1234567890";
		if (! CheckChar(charset, the_day, true)){
			fhint('日期应为数字！');
			the_val.focus();
			the_val.select();
			return false;
		}
		if (the_day.length > 2){
			fhint('日期不能大于2位！');
			the_val.focus();
			the_val.select();
			return false;
		}else if (the_day.length == 1){
			the_day = '0'+the_day;
		}
		if (the_day >31 || the_day < 01){
			fhint("请输入正确的日期！")
			the_val.focus();
			the_val.select();
		   return false;
		}
	}
    return true;
}

function CheckDate2(strDate, mini_year, name)
{
	if (name == null)
		prestr = '';
	else
		prestr = name + '输入有误，';

	if(mini_year == null)
		mini_year = 1900;
    var i_countSeparater = 0;
    var charset = "1234567890";
	var the_date = strDate;
	var the_dateLength=the_date.length;
	var i_firstSepLoc = the_date.indexOf('-',0);
	var i_lastSepLoc = the_date.lastIndexOf('-');
    if (i_firstSepLoc < 0 || i_firstSepLoc == i_lastSepLoc){
	    fhint(prestr + '请输入“年-月-日”格式的正确时间！');
	    return false;
	
	}	

	var the_year = the_date.substring(0,i_firstSepLoc);
	var the_month = the_date.substring(i_firstSepLoc+1,i_lastSepLoc);
	var the_day = the_date.substring(i_lastSepLoc+1,the_dateLength);
	if (! CheckChar(charset, the_year, true)){
	    fhint('年份应为数字！');
	    return false;
	}

	if (! CheckChar(charset, the_month, true)){
	    fhint('月份应为数字！');
	    return false;
	}
	if (! CheckChar(charset, the_day, true)){
	    fhint('日期应为数字！');
	    return false;
	}

	if (the_year.length >4){
	    fhint('年份不能大于4位！');
	    return false;
	}else if (the_year.length == 1){
	    the_year = '200'+the_year;
	}else if (the_year.length == 2){
	    the_year = '20'+the_year;
	}else if (the_year.length == 3){
	    the_year = '2'+the_year;
	}else if (the_year.length == 0){
	    fhint('请输入“年-月-日”格式的正确时间！');
	    return false;
	}			
    
	if (the_month.length > 2){
	    fhint('月份不能大于2位！');
	    return false;
	}else if (the_month.length == 1){
	    the_month = '0'+the_month;
	}else if (the_month.length ==0){
	    fhint('请输入由“-”分隔的正确的时间！');
	    return false;
	}	

	if (the_day.length > 2){
	    fhint('日期不能大于2位！');
	    return false;
	}else if (the_day.length == 1){
	    the_day = '0'+the_day;
	}else if (the_day.length == 0){
	    fhint('请输入由“-”分隔的正确的时间！');
	    return false;
	}	

    if ( the_year < mini_year){
        fhint("年份不得小于 " + mini_year +"！");
        return false;
    }
    if (the_month < 01 || the_month > 12){
        fhint("请输入正确的月份！")
        return false;
    }
    if (the_day >31 || the_day < 01){
        fhint("请输入正确的日期！")
        return false;
        
    }else{
        switch(eval(the_month)) {
            case 4:
            case 6:
            case 9:
            case 11:
                if (the_day < 31){
                    the_date=the_year+'-'+the_month+'-'+the_day;
                    return the_date; 
                }    
                break;
            case 2:
                var num = Math.floor(the_year/4) * 4;
                if(the_year == num) {
                    if (the_day < 30){
                        the_date=the_year+'-'+the_month+'-'+the_day;
                        return the_date;
                     }   
                } else {
                    if (the_day < 29){
                        the_date=the_year+'-'+the_month+'-'+the_day;
                        return the_date;
                    }    
                }
                break;
            default:
                if (the_day < 32){
                    the_date=the_year+'-'+the_month+'-'+the_day;
                    return the_date; 
                }    
                break;
        }
    }
    fhint("请输入正确的日期！");
    return false;
	
}     

function CheckDate(val, name) 
{
	if(!CheckNull(val))
	{
		if(!CheckDate2(document.forms[formName].elements[val].value,'',name))
		{
			document.forms[formName].elements[val].focus();
			document.forms[formName].elements[val].select();
			return false;
		 }
		 else 
			 document.forms[formName].elements[val].value = CheckDate2(document.forms[formName].elements[val].value);
	}
	return true;
}


function CheckSEdate(sval, eval, hintstr) 
{
	if (hintstr == null)
		hintstr = "开始日期不能在结束日期之后！";

	if (CheckDate(sval) && CheckDate(eval))
	{
		var stime = document.forms[formName].elements[sval].value;
		var etime = document.forms[formName].elements[eval].value;
		if (stime > etime)
		{
			fhint(hintstr);
			return false;
		}
		return true;
	}
	else
		return false;
}


function CheckHHMM(strHHMM)
{      
    	var the_HHMM = strHHMM;
    	var charset = "1234567890";
    	var the_HHMMLength = the_HHMM.length;
    	var the_sepLoc = the_HHMM.indexOf(':',0);
    	if (the_sepLoc < 0){
	        fhint('请输入由“:”分隔的正确的小时，分钟！');
	        return false;
    	
    	}	
    	
    	var the_HH = the_HHMM.substring(0,the_sepLoc);
    	var the_MM = the_HHMM.substring(the_sepLoc+1,the_HHMMLength);
    	if (! CheckChar(charset, the_HH, true)){
	        fhint('小时应为数字！');
	        return false;
	    }

    	if (! CheckChar(charset, the_MM, true)){
	        fhint('分钟应为数字！');
	        return false;
	    }
	    
	    if (the_HH.length > 2){
	        fhint('小时不能大于2位！');
	        return false;
	    }else if (the_HH.length == 1){
	        the_HH = '0'+the_HH;
	    }else if (the_HH.length ==0){
	        fhint('请输入由“:”分隔的正确的小时，分钟！');
	        return false;
	    }	

	    if (the_MM.length > 2){
	        fhint('分钟不能大于2位！');
	        return false;
	    }else if (the_MM.length == 1){
	        the_MM = '0'+the_MM;
	    }else if (the_MM.length ==0){
	        fhint('请输入由“:”分隔的正确的小时，分钟！');
	        return false;
	    }	
	    
	    if (the_HH > 23 || the_HH < 00){
	        fhint('请输入正确的小时！');
	        return false;
	    }	
	        
	    if (the_MM > 60 || the_HH < 00){
	        fhint('请输入正确的分钟！');
	        return false;
	    }	
    	
    	the_HHMM=the_HH + ":" + the_MM;
    	return the_HHMM;
}	


function TrimForm(a_formName)
{
	if(a_formName!=null)
		formName=a_formName; 
	var i_length = document.forms[formName].elements.length
	for(var i=0; i<i_length; i++)
	{
		if (document.forms[formName].elements[i].type == "text"||document.forms[formName].elements[i].type =="textarea")
			document.forms[formName].elements[i].value = Trim(document.forms[formName].elements[i].value);
	}
}

function CheckTextareaLength(val, max_length)
{
	with (document.forms[formName])
	{
		if (elements[val].outerText.length > max_length)
		{
			fhint("字段文字超长！不能多于 " + max_length +" 个字符。[ 现长度：" + elements[val].outerText.length + "]");
			elements[val].focus();
			elements[val].select();
			return false;
		}
	}
    return true;
}


function CheckChar(charset, val, should_in)
{
    var num = val.length;
    for (var i=0; i < num; i++)
	{
       var strchar = val.charAt(i);
       strchar = strchar.toUpperCase();
       if ((charset.indexOf(strchar) > -1) && (!should_in))
          return false;
       else if ((charset.indexOf(strchar) == -1) && (should_in))
          return false;
    }
    return true;
}


function Trim(str)
{
    var num = str.length; 
    var i = 0;
    for(i = 0; i < num;i++) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(i);
    num = str.length; 
    for(i = num-1; i > -1;i--) {
        if(str.charAt(i) != " ")
            break;
    }
    str = str.substring(0, i+1);
    return str;
}

function formatFloat(val,len)
{
	if (val == '')
		val = 0;
	else
	{
		if (len == null)
			len = 2;
		dotpos = val.indexOf(".");
		if(dotpos > 0)
		{
			intPart = val.substring(0,dotpos);
			decPart = val.substring(dotpos+1,val.length);
			if(decPart.length > len)
			{
				decPart = decPart.substring(0,len);
				val = intPart + "." + decPart;
			}
		}
	}
	return val;
}

function roundValue(val,len)
{
	if (val == '')
		val = 0;
	else
	{
		if (len == null)
			len = 2;
		dotpos = val.indexOf(".");
		if(dotpos > 0)
		{
			intPart = val.substring(0,dotpos);
			decPart = val.substring(dotpos+1,val.length);
			if(decPart.length > len)
			{
				var subInt = decPart.substring(0,len);
				var subDec = decPart.substring(len,decPart.length);
				var newStr = subInt + "." + subDec;
				var newValue = new Number(newStr);
				var zero = '';
				newValue = Math.round(newValue);
				newDecPart = newValue.toString();
				for (i = 0;i < (len - newDecPart.length) ;i++ )
				{
					zero = '0' + zero;
				}
				val = intPart + "." + zero + newDecPart;
			}
		}
	}
	return val;
}


function showOneHideOther(n,count)
{
	var i;
	for (i = 1; i <= count; i++)
	{
		if (i == n)
		{
			eval("document.all.t" + i +".style.display = \"block\"");
			eval("document.all.t" + i +".className = \"tab-page\"");
			eval("document.all.b" + i +".className = \"midbutton-s\"");
			eval("document.all.b" + i +".blur()");
		}
		else
		{
			eval("document.all.t" + i +".style.display = \"none\"");
			eval("document.all.b" + i +".className = \"midbutton\"");
		}
	}
}

function chgcss(obj, act, exp)
{
	if (obj.className != exp)
	{
		if (act == 'over')
			obj.className = 'midbutton-o';
		else if (act == 'out')
			obj.className = 'midbutton';
	}
}

function showHide(val)
{
	var tval = eval("document.all." + val);
	if (tval.style.display == "block")
		tval.style.display = "none";
	else
		tval.style.display = "block"
}


function fhint(msg)
{
	if (top.Merlin && top.Merlin.Visible == true)
	{
		top.Merlin.Speak(msg); 
		top.Merlin.Play("Confused");
	}
	else
		alert(msg);
}

function commFunc(type, action, str)
{
	switch (type)
	{
		case 6:
			event.srcElement.disabled = true;
			objId = event.srcElement.id
			setTimeout("unDisabled(objId)",5000);
			url = action;
			var i_top	 = 160;	//上
			var i_left	 = 250;	//左
			var i_width	 = 360;	//宽
			var i_heigth = 50;	//高
			msg = window.open(url,'',"toolbar=no,menubar=no,top=" + i_top + ",left=" + i_left + ",resizable=1,scrollbars=no,width=" + i_width + ",height=" + i_heigth);
			msg.focus();
			break;
	}
}

function unDisabled(obj)
{
	eval("document.all." + obj).disabled = false;
}