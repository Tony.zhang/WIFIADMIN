function show_chart(type, title, ytitle, datagrid){
	var series = [];
	var xlabels = [];
	var xtitle = datagrid[0][0];
 
	for(i=1; i<datagrid[0].length-1; i++){
		var y = datagrid[0][i];
		if(y == '' || y == null){
			break;
		}
		series.push({
			name: y,
			data: []
		});
	}
	for(i=1; i<datagrid.length-1; i++){
		var x = datagrid[i][0];
		if(x == '' || x == null){
			break;
		}
		xlabels.push(x);
		for(j=1; j<datagrid[i].length-1; j++){
			if(datagrid[0][j].length == 0){
				continue;
			}
			var y = parseFloat(datagrid[i][j]);
			if(!isNaN(y)){
				series[j-1].data.push([x, y]);
			}else{
				series[j-1].data.push([x, null]);
			}
		}
	}
	var tmp = [];
	for(i=0; i<series.length; i++){
		if(series[i].data.length > 0){
			tmp.push(series[i]);
		//	alert(series[i].data);
		}
	}
	series = tmp;
	//alert(xlabels);
	//alert(series[0].data);
 
	$('#charts').html('');
	var charts = new Highcharts.Chart({
		chart: {
			renderTo: 'charts',
			type: type
		},
		title: {
			text: title
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			title: {
				text: xtitle,
				style: {
					color: '#666',
					"font-size": '120%'
				}
			},
			categories: xlabels,
			min: 0, //
			//minRange: 1,
			minPadding: 1, //
			labels: {
				formatter: function() {
					return this.value + '';

				}
			}
		},
		yAxis: {
			title: {
				text: ytitle,
				style: {
					color: '#666',
					"font-size": '120%'
				}
			},
			labels: {
				formatter: function() {
					return this.value;
				}
			}
		},
		tooltip: {
			enabled: true,
			formatter: function() {
				return this.series.name + ': '+this.y +''; //
			}
		},
		plotOptions: {
			series:{
				connectNulls: true
			},
			line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: true
			},
			spline: {
				dataLabels: {
					enabled: true
				}
			},
			bar: {
				dataLabels: {
					enabled: true
				}
			},
			column: {
				dataLabels: {
					enabled: true
				}
			},
			area: {
				dataLabels: {
					enabled: true
				}
			},
			pie: {
				allowPointSelect: true,
				showInLegend: true,
				dataLabels: {
					enabled: true,
					formatter: function() {
						var p = this.percentage + '';
						var pos = p.indexOf('.');
						if(pos != -1){
							p = p.substr(0, pos + 2);
						}
						return '<b>'+ this.point.name +'</b>: ' + p +' %';
					}
				}
			}
		},
		series: series
	});
}
