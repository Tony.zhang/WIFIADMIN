<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=1;  
$PurviewLevel_Others="daili_";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function ConfirmDel()
{
   if(confirm("确定要删除选中的信息吗？"))
     return true;
   else
     return false;
	 
}
function postdo(va){
	document.form2.jobs.value=va
	document.form2.submit();
}

var xmlobj; 
function CreateXMLHttpRequest()
{
	if(window.XMLHttpRequest)
	{//Mozilla浏览器
		xmlobj=new XMLHttpRequest();
		if(xmlobj.overrideMimeType)
		{//设置MIME类别
		xmlobj.overrideMimeType("text/xml");
		}
	}
	else if(window.ActiveXObject)
	{ //IE浏览器
		try
		{xmlobj=new ActiveXObject("Msxml2.XMLHttp");}
		catch(e)
		{
			try
			{xmlobj=new ActiveXobject("Microsoft.XMLHttp");}
			catch(e)
			{}
		}
	}

}

function StatHandler_xucode() { //用于处理状态的函数
	if(xmlobj.readyState == 4 && xmlobj.status == 200) //如果URL成功访问，则输出网页 
	{
	    if (xmlobj.responseText=="1"){
			alert("此代理编号已经存在");
		}
	}
}

function check_daili(name){
	if (name!=""){
		CreateXMLHttpRequest(); 
		var showurl = 'check_name.php?name='+name; 
		xmlobj.open("GET", showurl, true);
	
		xmlobj.onreadystatechange = StatHandler_xucode;
		xmlobj.send(null); 
	}
}


function trim(str){   
    str = str.replace(/^(\s|\u00A0)+/,'');   
    for(var i=str.length-1; i>=0; i--){   
        if(/\S/.test(str.charAt(i))){   
            str = str.substring(0, i+1);   
            break;   
        }   
    }   
    return str;   
}  

</SCRIPT>
</head>
<body> 
<?php
if ($actions=="saveadd"){
	if(trim($title)==""){
		$fun->popmassage("模板名称不能为空！","","popback");
		exit();
	}
	$havedl=$db->getinfo("select top 1 * from mx_renzheng_moban  WHERE title='$title'");
	if(is_array($havedl)){
		$fun->popmassage("此模板名称已经存在！","","popback");
		exit();
	}
	if(trim($tempath)==""){
		$fun->popmassage("模板路径不能为空！","","popback");
		exit();
	}
	$havedl="";
	$havedl=$db->getinfo("select top 1 * from mx_renzheng_moban  WHERE tempath='$tempath'");
	if(is_array($havedl)){
		$fun->popmassage("此模板路径已经存在！","","popback");
		exit();
	}
	
	$img="";
	$file=$_FILES['picurl'];
	if (!empty($file["name"])){
	$iname = date("YmdHis");
	$picType = explode(".",$file["name"]);
			$picType = $picType[1];		
			$picPath = "../../{$webdb[updir]}/renzhengpic/".date("Ym")."/".$iname.".".$picType;
			$img="renzhengpic/".date("Ym")."/".$iname.".".$picType;
			movefile("","","../../{$webdb[updir]}/renzhengpic/".date("Ym")."/");
			if(move_uploaded_file($file['tmp_name'], $picPath))
			{
				chmod($picPath,0755);
			}else{
			}	
			@unlink("../../$webdb[updir]/$postdb[oldpicurl]");
	}
	if($img==""){
		  $fun->popmassage("请上传模板图！","","popback");
		  exit();
	}
	
	if($orderid==""){
		$orderid=666;
	}
	
	$canshowStr = '';
	if($canshow){
		foreach($canshow as $k => $v){
			$canshowStr .= $v.',';
		}
		$canshowStr = trim($canshowStr,',');
	}
	
	$isok=intval($isok);
	
  	$files=array(
  	"title"             =>$title,
  	"tempath"         =>$tempath,
  	"picurl"           =>$img,
  	"canshow"      =>$canshowStr,
  	"mone"               =>$mone,
  	"isok"               =>$isok,
  	"orderid"               =>$orderid,
  	"createdate"       =>date("Y-m-d H:i:s"),
  	"createid"         =>$_SESSION['mxwifi']['userid'],
  	);
  	$db->exe_insert("mx_renzheng_moban",$files);
	
	jump2("模板添加成功","sys_renzheng.php",2);
	exit();
}
elseif ($actions=="savemodi"){
	$id=intval($id);
	if ($id<=0){
		$fun->popmassage("请选择要修改的信息","","popback");
		exit();
	}
	if(trim($title)==""){
		$fun->popmassage("模板名称不能为空！","","popback");
		exit();
	}
	$havedl=$db->getinfo("select top 1 * from mx_renzheng_moban  WHERE title='$title' and id<>'$id'");
	if(is_array($havedl)){
		$fun->popmassage("此模板名称已经存在！","","popback");
		exit();
	}
	if(trim($tempath)==""){
		$fun->popmassage("模板路径不能为空！","","popback");
		exit();
	}
	$havedl="";
	$havedl=$db->getinfo("select top 1 * from mx_renzheng_moban  WHERE tempath='$tempath' and id<>'$id'");
	if(is_array($havedl)){
		$fun->popmassage("此模板路径已经存在！","","popback");
		exit();
	}
	
	$img="";
	$file=$_FILES['picurl'];
	if (!empty($file["name"])){
	$iname = date("YmdHis");
	$picType = explode(".",$file["name"]);
			$picType = $picType[1];		
			$picPath = "../../{$webdb[updir]}/renzhengpic/".date("Ym")."/".$iname.".".$picType;
			$img="renzhengpic/".date("Ym")."/".$iname.".".$picType;
			movefile("","","../../{$webdb[updir]}/renzhengpic/".date("Ym")."/");
			if(move_uploaded_file($file['tmp_name'], $picPath))
			{
				chmod($picPath,0755);
			}else{
			}	
			@unlink("../../$webdb[updir]/$oldpicurl");
	}
	else{
		$img=$oldpicurl;
	}
	
	if($img==""){
		  $fun->popmassage("请上传模板图！","","popback");
		  exit();
	}

	if($orderid==""){
		$orderid = 666;
	}

	$canshowStr = '';
	if($canshow){
		foreach($canshow as $k => $v){
			$canshowStr .= $v.',';
		}
		$canshowStr = trim($canshowStr,',');
	}
	$isok=intval($isok);
	
  	$files=array(
  	"title"             =>$title,
  	"tempath"         =>$tempath,
  	"picurl"           =>$img,
  	"canshow"        =>$canshowStr,
  	"mone"      =>$mone,
  	"isok"               =>$isok,
  	"orderid"               =>$orderid,
  	"modidate"        =>date("Y-m-d H:i:s"),
  	"modiid"          =>$_SESSION['mxwifi']['userid'],
  	);
  	$db->exe_update("mx_renzheng_moban",$files,"id='$id'");

    $url=url_code($url);
	jump2("模板修改成功","$url",2);
    exit();
} elseif ($actions=="delpic"){
	if(!$id){
		$fun->popmassage("请选择要删除的图片","","popback");
		exit();
	}
	$pic=$db->getsingle("select picurl from mx_renzheng_moban WHERE id='$id'");
	if ($pic!=""){@unlink("../../$webdb[updir]$pic");}
	//删除附件
	$db->excu("update mx_renzheng_moban set picurl='' WHERE id='$id'");
	jump2("操作成功","sys_renzheng.php?actions=modi&id=".$id."&gotourl=".url_encode($gotourl),1);
	exit();
}
elseif ($actions=="delete"){
	  global $db,$fun;
	  $id=intval($id);
	  if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $mubaninfo=$db->getinfo("select top 1 * from mx_renzheng_moban where id='$id'");
	  if (!is_array($mubaninfo)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  @unlink("../../$webdb[updir]$mubaninfo[picurl]");
	  $db->excu("delete from mx_renzheng_moban where id='$id' ");
	  $url=$fromurl?$fromurl:$FROMURL;
	  jump2("模板删除成功",$url,1);
	  exit();
}
elseif ($actions=="delall"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	
	
	foreach( $id as $key=>$value){
	  $mubaninfo=$db->getinfo("select top 1 * from mx_renzheng_moban where id='$key'");
	  if (!is_array($mubaninfo)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  @unlink("../../$webdb[updir]$mubaninfo[picurl]");
	  $db->excu("delete from mx_renzheng_moban where id='$key' ");
	}
	
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("模板删除成功",$url,1);
	exit();
}
elseif ($actions=="isok"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要操作的信息","","popback");
		exit();
	}
	$upsql="update mx_renzheng_moban set isok='1',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$id'";
	
	$db->excu($upsql);
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="noisok"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要操作的信息","","popback");
		exit();
	}
	$upsql="update mx_renzheng_moban set isok='0',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$id'";
	$db->excu($upsql);
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="pisok"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("update mx_renzheng_moban set isok='1',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="pnoisok"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("update mx_renzheng_moban set isok='0',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}


require("../mx_head.php");?>
<?php
  if ($actions=="add"){
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("name","模板名称"))
		return false;
	
	if (!CheckLength("filename","模板路径"))
		return false;
		
	if (!CheckLength("fid","所属行业"))
		return false;
		
	if (!CheckLength("picurl","模板图"))
		return false;
		
	if (!CheckLength("shejicode","设计编号"))
		return false;
		
	if (!CheckLength("chengxucode","程序编号"))
		return false;

}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">新增模板</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="sys_renzheng.php" onsubmit="return CheckAdd()" enctype="multipart/form-data">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">模板名称：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="title" id="title" class="xtgk5" />&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff" width="15%">模板路径：</td>
          <td colspan="3" bgcolor="#FFFFFF" width="35%"><input type="text" name="tempath" id="tempath" class="xtgk5" />&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模 板 图：</td>
          <td  bgcolor="#FFFFFF" colspan="3"><input name="picurl" id="picurl" type="file" />&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">展示内容：</td>
          <td bgcolor="#FFFFFF">
          <?php global $$RENTEMTYPE; foreach($RENTEMTYPE as $k => $v){?>
          	<input type="checkbox" name="canshow[]" value="<?php echo $k;?>" /><?php echo $v;?>
          <?php }?>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模板备注：</td>
          <td bgcolor="#FFFFFF"> <textarea style="width:400px; height:100px;" name="mone" id="mone"></textarea></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模板排序：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="orderid" value="666" id="orderid"/>&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模板状态：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="radio" name="isok" id="isok" value="1" checked="checked" /> 
          &nbsp;显示&nbsp;
          <input type="radio" name="isok" id="isok" value="0" />&nbsp;隐藏</td>
        </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="actions" value="saveadd" />	
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="modi"){
//修改管理员
  global $db,$fun;
  $id=trim($id);
  if ($id==0){
  	$fun->popmassage("此信息不存在！","","popback");
	exit();
  }
  $rsdb=$db->getinfo("select top 1 * from mx_renzheng_moban where id='$id'");
  if (!is_array($rsdb)){
    $fun->popmassage("此信息不存在！","","popback");
    exit();
  }
  
  $isokdb[intval($rsdb[isok])]=' checked ';
  if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("title","模板名称"))
		return false;
	
	if (!CheckLength("tempath","模板路径"))
		return false;
		
	if (document.form1.picurl.value==""){
		if (document.form1.oldpicurl.value==""){
			alert("模板图不能为空");
			return false;
		}
	}
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改模板</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="sys_renzheng.php" onsubmit="return CheckAdd()" enctype="multipart/form-data">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">模板名称：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="title" id="title" class="xtgk5" value="<?php echo trim($rsdb["title"]);?>" />&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff" width="15%">模板路径：</td>
          <td colspan="3" bgcolor="#FFFFFF" width="35%"><input type="text" name="tempath" id="tempath" class="xtgk5" value="<?php echo trim($rsdb["tempath"]);?>" />&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模 板 图：</td>
          <td  bgcolor="#FFFFFF" colspan="3"><input name="picurl" type="file" /><input type="hidden" name="oldpicurl" value="<?php echo trim($rsdb[picurl])?>">&nbsp;<span class="red14">*</span></td>
        </tr>
        <?php if(trim($rsdb[picurl])!=""){?>
        <tr>
			<td align="right" valign="top" bgcolor="#eff7ff">预览图片：</td>
			<td bgcolor="#FFFFFF" colspan="3"><img src="<?php echo get_photourl($rsdb[picurl]); ?>" style="max-width:150px; max-height:150px;">&nbsp;<a href="sys_renzheng.php?actions=delpic&id=<?php echo $rsdb["id"]?>&gotourl=<?php echo $gotourl;?>">删除图片</a></td>
        </tr>
        <?php }?>
        <tr>
          <td align="right" bgcolor="#eff7ff">展示内容：</td>
          <td bgcolor="#FFFFFF">
          <?php global $RENTEMTYPE; $canshow = explode(',', $rsdb[canshow]); foreach($RENTEMTYPE as $k => $v){?>
          	<input type="checkbox" <?php if(in_array($k,$canshow)){echo "checked";} ?> name="canshow[]" value="<?php echo $k;?>" /><?php echo $v;?>
          <?php }?>
          </td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模板备注：</td>
          <td bgcolor="#FFFFFF"> <textarea style="width:400px; height:100px;" name="mone" id="mone"><?php echo trim($rsdb["mone"]);?></textarea></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模板排序：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="orderid" value="<?php echo trim($rsdb["orderid"]);?>" id="orderid"/>&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模板状态：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="radio" name="isok" id="isok" value="1" <?php if ($rsdb["isok"]==1){echo "checked";}?> /> 
          &nbsp;显示&nbsp;
          <input type="radio" name="isok" id="isok" value="0" <?php if ($rsdb["isok"]==0){echo "checked";}?>/>&nbsp;隐藏</td>
        </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="url" value="<?php echo $gotourl?>">
			<input type="hidden" name="id" value="<?php echo $rsdb[id]?>">
			<input type="hidden" name="actions" value="savemodi">
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="view"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("此模板信息不存在！","","popback");
		exit();
	}
	$rsdb=$db->getinfo("select top 1 * from mx_renzheng_moban where id='$id'");
	if (!is_array($rsdb)){
		$fun->popmassage("此模板信息不存在！","","popback");
		exit();
	}
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">模板信息</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="sys_renzheng.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">模板名称：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo trim($rsdb["title"]);?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff" width="15%">模板路径：</td>
          <td colspan="3" bgcolor="#FFFFFF" width="35%"><?php echo trim($rsdb["tempath"]);?></td>
        </tr>
        <?php if(trim($rsdb[picurl])!=""){?>
        <tr>
			<td align="right" valign="top" bgcolor="#eff7ff">模 板 图</td>
			<td bgcolor="#FFFFFF" colspan="3"><img src="<?php echo get_photourl($rsdb[picurl]); ?>" style="max-width:150px; max-height:150px;"></td>
        </tr>
        <?php }?>
        <tr>
          <td align="right" bgcolor="#eff7ff">展示内容：</td>
          <td colspan="3" bgcolor="#FFFFFF">
          <?php global $RENTEMTYPE; $show=''; $canshow = explode(',', $rsdb[canshow]); foreach($RENTEMTYPE as $k => $v){
          	if(in_array($k,$canshow)){
          		$show .= $v.',';
          	}
          }
          echo trim($show,',');?>
          </td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">模板状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <?php if ($rsdb["isok"]==1){echo "显示";}else{echo "隐藏";}?>
		  </td>
		</tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">创建时间：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["createdate"]);?></td>
          <td align="right" bgcolor="#eff7ff">创 建 人：</td>
          <td bgcolor="#FFFFFF"><?php
		  $cinfo=$db->get_members($rsdb["createid"]);
		  echo trim($cinfo["realname"]);?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">修改时间：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["modidate"]);?></td>
          <td align="right" bgcolor="#eff7ff">修 改 人：</td>
          <td bgcolor="#FFFFFF"><?php
		  $cinfo=$db->get_members($rsdb["modiid"]);
		  echo trim($cinfo["realname"]);?></td>
        </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
		  <input type="button" name="back" value=" 返 回 " class="n_button" onclick="history.back();">
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">模板列表</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="sys_renzheng.php">模板管理</a></div>
		<div class="an2"><a href="sys_renzheng.php?actions=add">添加模板</a></div>
		</td>
		</tr>
		</table>
	</div>
      </div>
	<?php
  $str="";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }


  if ($isok!=""){
  	$isok=intval($isok);
	$str.=" and isok='$isok' ";
	$s.="isok=$isok&";
  }
  
  $table=" mx_renzheng_moban ";
  if ($px=="usenumdesc"){
  	$order="order by usenum desc, id desc";
  }
  elseif ($px=="usenumasc"){
  	$order="order by usenum asc, id asc";
  }
  else{
  	$order=" order by createdate desc, id desc";
  }
  $s.="px=$px&";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);

 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2">
      <tr bgcolor="#eff7ff">
        <td width="5%" align="center" bgcolor="#eff7ff">选择</td>
        <td width="15%" align="center" bgcolor="#eff7ff">模板名称</td>
        <td width="15%" align="center" bgcolor="#eff7ff">模板路径</td>
        <td width="10%" align="center" bgcolor="#eff7ff">创建人</td>
        <td width="12%" align="center" bgcolor="#eff7ff">创建时间</td>
        <td width="10%" align="center" bgcolor="#eff7ff">状态</td>
        <td width="10%" align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="sys_renzheng.php">
  <?PHp
  if (!is_array($list) or $c==0){
  echo "<tr class=\"daili1\"><td colspan=8><center>暂无信息</center></td></tr>";
  }
  else{
  for($i=0;$i<count($list);$i++){
	$list[$i][isok]=$list[$i][isok]?"<A HREF='sys_renzheng.php?actions=noisok&id=".$list[$i]["id"]."' style='color:red;' title='设为隐藏'>显示</A>":"<A HREF='sys_renzheng.php?actions=isok&id=".$list[$i]["id"]."' style='color:blue;' title='设为显示'>隐藏</A>";
  ?>
  <tr class="daili1" align="center"> 
    <td width="5%"><input type="checkbox" name="id[<?php echo $list[$i]["id"] ?>]" id="id" value="<?php echo $list[$i]["id"] ?>" class="input_check"></td>
    <td><a href="sys_renzheng.php?actions=view&id=<?php echo $list[$i]["id"]?>"><?php echo $list[$i]["title"]?></a></td>
    <td><a href="sys_renzheng.php?actions=view&id=<?php echo $list[$i]["id"]?>"><?php echo $list[$i]["tempath"] ?></a></td>
    <td><?php  $cinfo=$db->get_members($list[$i]["createid"]);
		  echo trim($cinfo["realname"]);?></td>
    <td><?php echo $list[$i]["createdate"] ?></td>
    <td><?php echo $list[$i]["isok"] ?></td>
    <td><a href="sys_renzheng.php?actions=modi&id=<?php echo $list[$i]["id"] ?>">修改</a>&nbsp;&nbsp;<a href="sys_renzheng.php?actions=delete&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要删除此模板吗?')">删除</a></td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php if ($c>0){?>
      <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a href="javascript:CheckAll(document.form1,'')">反选</a></td>
          <td width="19"><img src="../images/del.jpg" width="14" height="14" /></td>
          <td width="80"><a href="javascript:postdo('pisok');"/>设为显示</a></td>
          <td width="19"><img src="../images/stop.jpg" width="14" height="14" /></td>
          <td width="80"><a href="javascript:postdo('pnoisok');"/>设为隐藏</a></td>
          <td width="19"><img src="../images/stop.jpg" width="14" height="14" /></td>
         <td><a href="javascript:if (confirm('确定要删除选中的信息吗？')){postdo('delall');}"/>删除选中</a></td>
        </tr>
      </table>
	  <?php }
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>
<?php 
  }

?>

<?php require("../mx_foot.php");?>
</body>
</html>
