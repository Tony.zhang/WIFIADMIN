<?php
@session_start();
$PurviewLevel = 2;
$CheckChannelID = 0;
$PurviewLevel_Others = "sys_shebei";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
    <TITLE>铭讯EOS网站后台管理</TITLE>
    <Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
    <Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
    <link rel="stylesheet" href="../images/css.css" type="text/css">
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/zDialog.js"></script>
    <script type="text/javascript" src="../js/zDrag.js"></script>
</head>
<SCRIPT language=javascript>
    function closeme() {
        var diag = new Dialog();
        diag.Title = "从服务选择固件包";
        diag.URL = "select_images.php?f=form1.picname&noeditor=yes&imgstick=smallundefined";
        diag.Width = 850;
        diag.Height = 400;
        diag.show();

    }
    function uppic2(url, name, size, b) {
        get_obj('classflashurl').value = url;
    }
    function uppic3(url, name, size, b) {
        get_obj('flashurl').value = url;
    }
    function getImg(imgUrl) {
        if (imgUrl) {
            $('#menuMa').val(imgUrl);
            $('#menuMaBtn').html('删除素材图片').removeClass().attr('href', 'javascript:delImg();');
            $('#menuMaShow').attr('src', imgUrl).show();
            $('#fileField').hide();
        }
        fancyboxClose();
    }
    function delImg() {
        $('#menuMa').val('');
        $('#menuMaBtn').hide();
        $('#menuMaShow').attr('src', '').hide();
        $('#fileField').show();
        $('#recover').show();
    }

    function recover() {
        var imgUrl = $('#recoverImgUrl').val();
        getImg(imgUrl);
        $('#recover').hide();
        $('#menuMaBtn').show();
    }
    $(function () {
        $('#tui').hide();
        $('#fre').click(function () {
            $("#tui").show();
        })
        $('#nofre').click(function () {
            $('#tui').hide();
        })
    })
    function ConfirmDel() {
        if (confirm("确定要删除选中的新闻吗？一旦删除将不能恢复！"))
            return true;
        else
            return false;

    }
    function CheckAdd() {
        if (document.form1.typecode.value == "") {
            alert("用设备型号不能为空！");
            document.form1.typecode.focus();
            return false;
        }
        if (document.form1.drivecode.value == "") {
            alert("设备固件版本号不能为空！");
            document.form1.drivecode.focus();
            return false;
        }
        if (document.form1.driveurl.value == '' && document.form1.picname.value == '') {
            alert("设备固件包地址不能为空！");
            document.form1.driveurl.focus();
            return false;
        }
        if (document.form1.Purview[1].checked == true) {
            GetClassPurview();
        }
    }
</SCRIPT>
</head>
<body>
<?php
if ($actions == "saveadd") {
//添加设备类型
    if ($typecode == "" || empty($typecode)) {
        $fun->popmassage("设备型号不能为空，请正确填写！", "", "popback");
        exit();
    }
    if ($drivecode == "" || empty($drivecode)) {
        $fun->popmassage("设备固件版本号不能为空，请正确填写！", "", "popback");
        exit();
    }
    if ($driveurl == "" || empty($driveurl)) {
        $fun->popmassage("设备固件包地址不能为空，请正确填写！", "", "popback");
        exit();
    }
	if (trim($orderid)==""){
		$orderid=666;	
	}
	else{
		$orderid=intval($orderid);
	}

    $ip = $_SERVER['REMOTE_ADDR'];
    $files = array(
        "typecode" => $typecode,
        "drivecode" => $drivecode,
        "mone" => $mone,
        "driveurl" => $driveurl,
        "createdate" => date("Y-m-d H:i:s"),
        "createid" => $_SESSION['mxwifi']['userid'],
    );
    $db->exe_insert("mx_shebei_type", $files);
    jump2("操作成功", "sys_shebei.php", 2);
    exit();

} elseif ($actions == "savemodi") {
//保存修改后的设备信息
    global $db, $fun;
    $uid = intval($uid);
    if ($uid <= 0) {
        $fun->popmassage("非法访问！", "", "popback");
        exit();
    }
	$thisinfo=$db->getinfo("select top 1 typecode from mx_shebei_type where id='$uid'");
    if ($typecode == "" || empty($typecode)) {
        $fun->popmassage("设备型号不能为空，请正确填写！", "", "popback");
        exit();
    }
    if ($driveurl == '' || empty($driveurl)) {
        if ($picname == '' || empty($picname)) {
            $fun->popmassage('请选择固件包', '', 'popback');
        } else {
            $driveurl = $picname;
        }
    }
	if (trim($orderid)==""){
		$orderid=666;	
	}
	else{
		$orderid=intval($orderid);
	}

    $files = array(
        "typecode" => $typecode,
        "drivecode" => $drivecode,
        "mone" => $mone,
        "driveurl" => $driveurl,
        "modidate" => date("Y-m-d H:i:s"),
        "modiid" => $_SESSION['mxwifi']['userid'],
    );


    $db->exe_update("mx_shebei_type", $files, "id='$uid'");
	if (trim($thisinfo["typecode"])!=trim($typecode)){
		//更新设备型号名称
		$files2 = array(
			"xinghao" => $typecode,
		);
		$db->exe_update("mx_shebei", $files2, "xinghaoid='$uid'");
	}
	
    $url = url_code($url);
    if (trim($url) == "") {
        $url = "sys_shebei.php";
    }
    jump2("操作成功", "$url", 2);
    exit();
} elseif ($actions == "delall") {
    global $db, $fun;
    if (!is_array($uid)) {
        $fun->popmassage("您没有选择要操作信息，请重新输入！", "", "popback");
        exit();
    }
    foreach ($uid as $key => $value) {
		//判断是否有设备
		$havenum="";
		$havenum=$db->getinfo("select top 1 id from mx_shebei where xinghaoid='$key'");
		if (is_array($havenum)){
			$fun->popmassage("此型号有未删除设备，不能删除此设备型号", "", "popback");
			exit();
		}
        $db->excu("delete from mx_shebei_type where id='$key' ");
    }
    jump2("操作成功", "$url", 2);
    exit();
} elseif ($actions == "delete") {
    global $db, $fun;
    $uid = intval($uid);
    if ($uid == 0) {
        $fun->popmassage("您没有选择要操作信息，请重新输入！", "", "popback");
        exit();
    }
	//判断是否有设备
	$havenum=$db->getinfo("select top 1 id from mx_shebei where xinghaoid='$uid'");
	if (is_array($havenum)){
        $fun->popmassage("此型号有未删除设备，不能删除此设备型号", "", "popback");
        exit();
	}
    $db->excu("delete from mx_shebei_type where id='$uid' ");
    jump2("操作成功", "$FROMURL", 2);
    exit();
}


require("../mx_head.php");?>

<?php
if ($actions == "add") {
//添加设备
    ?>
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
        <tr>
            <td width="4"><img src="../images/gk_1.jpg" width="4" height="39"/></td>
            <td background="../images/gk_2.jpg">
                <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="23" class="xtgk2"><img src="../images/166.gif" width="16" height="16"/></td>
                        <td class="xtgk1">添加设备型号</td>
                    </tr>
                </table>
            </td>
            <td width="5"><img src="../images/gk_3.jpg" width="5" height="39"/></td>
        </tr>
    </table>
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
        <tr>
            <td valign="top" bgcolor="#FFFFFF">
                <div class="main_topdiv">
                    <div class="t">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="an1"><a href="sys_shebei.php">设备型号列表</a></div>
                                    <div class="an2"><a href="sys_shebei.php?actions=add">添加设备</a></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <table width="96%" border="0" align="center" cellpadding="0" cellspacing="1">
                    <tr>
                        <td valign="top" bgcolor="#FFFFFF">
                            <form name="form1" method="post" action="sys_shebei.php" onSubmit="return CheckAdd();">
                                <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1"
                                       bgcolor="#d8e8f2" class="marb15 mart15">
                                    <tr>
                                        <td width="20%" align="right" bgcolor="#eff7ff">设备型号：</td>
                                        <td width="80%" bgcolor="#FFFFFF"><input type="text" name="typecode"
                                                                                 id="typecode" class="xtgk5"/></td>
                                    </tr>
                                    <tr style="padding: 10px 0;">
                                        <td width="20%" align="right" bgcolor="#eff7ff">设备备注：</td>
                                        <td width="80%" bgcolor="#FFFFFF"><textarea name="mone" id="mone"
                                                                                    style="width: 80%; height: 60px;resize: none;"></textarea>
                                        </td>
                                    </tr>
                                    <tr style="padding-bottom: 10px;">
                                        <td width="20%" align="right" bgcolor="#eff7ff">固件本号：</td>
                                        <td width="80%" bgcolor="#FFFFFF"><input type="text" name="drivecode"
                                                                                 id="drivecode" class="xtgk5"/></td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right" bgcolor="#eff7ff">固件地址：</td>
                                        <td width="80%" bgcolor="#FFFFFF">
                                            <input type="text"  class="xtgk5" id="web_adpic" name="driveurl"/><a
                                                href="javascript:closeme()"><font color="#FF0000">从服务器找固件包</font> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right" bgcolor="#eff7ff">设备排序：</td>
                                        <td width="80%" bgcolor="#FFFFFF">
                                            <input type="text" id="orderid"  class="xtgk5" name="orderid" value="666"/>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="8" align="center" bgcolor="#FFFFFF"><input type="image"
                                                                                                name="imageField"
                                                                                                src="../images/dls_1.jpg"/><input
                                                type="hidden" name="actions" value="saveadd"></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php
}

elseif ($actions == "modipurview"){
//修改设备型号
global $db, $fun;
$uid = intval($uid);
if ($uid == 0) {
    $fun->popmassage("此设备型号不存在！", "", "popback");
    exit();
}
$rsmenber = $db->getinfo("select top 1 * from mx_shebei_type where id='$uid'");
if (!is_array($rsmenber)) {
    $fun->popmassage("此设备型号不存在！", "", "popback");
    exit();
}
if ($gotourl == "") {
    $gotourl = url_encode($_SERVER['HTTP_REFERER']);
}
?>
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
        <tr>
            <td width="4"><img src="../images/gk_1.jpg" width="4" height="39"/></td>
            <td background="../images/gk_2.jpg">
                <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="23" class="xtgk2"><img src="../images/166.gif" width="16" height="16"/></td>
                        <td class="xtgk1">修改设备型号</td>
                    </tr>
                </table>
            </td>
            <td width="5"><img src="../images/gk_3.jpg" width="5" height="39"/></td>
        </tr>
    </table>
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
        <tr>
            <td valign="top" bgcolor="#FFFFFF">
                <div class="main_topdiv">
                    <div class="t">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="an1"><a href="sys_shebei.php">设备型号列表</a></div>
                                    <div class="an2"><a href="sys_shebei.php?actions=add">添加设备</a></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <table width="96%" border="0" align="center" cellpadding="" cellspacing="1" style="margin-bottom: 15px;">
                    <tr>
                        <td valign="top" bgcolor="#FFFFFF">
                            <form name="form1" method="post" action="sys_shebei.php" onSubmit="return CheckAdd();">
                                <table width="96%" border="0" align="center" cellpadding="10" cellspacing="1"
                                       bgcolor="#c9e5f6">
                                    <tr>
                                        <td width="20%" align="right" bgcolor="#eff7ff">设备型号：</td>
                                        <td width="80%" bgcolor="#FFFFFF"><input type="text" name="typecode"
                                                                                 id="typecode" class="xtgk5"
                                                                                 value="<?php echo $rsmenber['typecode'] ?>"/>
                                        </td>
                                    </tr>
                                    <tr style="padding:10px 0;">
                                        <td width="20%" align="right" bgcolor="#eff7ff">设备备注：</td>
                                        <td width="80%" bgcolor="#FFFFFF"><textarea name="mone" id="mone"
style="width:80%; height:60px; resize: none;"><?php echo $rsmenber['mone'] ?></textarea>
                                        </td>
                                    </tr>
                                    <tr style="padding:10px 0;">
                                        <td width="20%" align="right" bgcolor="#eff7ff">固件本号：</td>
                                        <td width="80%" bgcolor="#FFFFFF"><input type="text" name="drivecode"
                                                                                 id="drivecode" class="xtgk5"
                                                                                 value="<?php echo $rsmenber['drivecode'] ?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" align="right" bgcolor="#eff7ff">固件地址：</td>
                                        <td width="80%" bgcolor="#FFFFFF">
                                            <input type="text" id="web_adpic"  class="xtgk5" name="driveurl" value="<?php echo $rsmenber['driveurl'] ?>"/><a
                                                href="javascript:closeme()" ><font color="#FF0000">从服务器找固件包</font> </a>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td width="20%" align="right" bgcolor="#eff7ff">设备排序：</td>
                                        <td width="80%" bgcolor="#FFFFFF">
                                            <input type="text" id="orderid"  class="xtgk5" name="orderid" value="<?php echo intval($rsmenber['orderid']) ?>"/>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="8" align="center" bgcolor="#FFFFFF"><input type="image"
                                                                                                name="imageField"
                                                                                                src="../images/dls_1.jpg"/>
                                            <input type="hidden" name="url" value="<?php echo $gotourl ?>">
                                            <input type="hidden" name="uid" value=" <?php echo $rsmenber['id'] ?>">
                                            <input type="hidden" name="actions" value="savemodi">
                                        </td>
                                    </tr>
                                </table>


                            </form>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php
}

else{
?>
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
        <tr>
            <td width="4"><img src="../images/gk_1.jpg" width="4" height="39"/></td>
            <td background="../images/gk_2.jpg">
                <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16"/></td>
                        <td class="xtgk1">设备型号列表</td>
                    </tr>
                </table>
            </td>
            <td width="5"><img src="../images/gk_3.jpg" width="5" height="39"/></td>
        </tr>
    </table>
    <table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
        <tr>
            <td valign="top" bgcolor="#FFFFFF">
                <div class="main_topdiv">
                    <div class="t">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="an1"><a href="sys_shebei.php">设备型号列表</a></div>
                                    <div class="an2"><a href="sys_shebei.php?actions=add">添加设备</a></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table border="0" align="left" cellpadding="3" cellspacing="0">
                        <form id="search_form" name="search_form" method="get" action="sys_shebei.php">
                            <tr>
                                <td>
                                    请输入设备型号:
                                </td>
                                <td><input name="keywords" type="text" id="keywords" value="" size="10"
                                           class="search_input"/></td>
                                <td><input type="image" name="imageField" src="../images/search.jpg"/></td>
                            </tr>
                        </form>
                    </table>
                </div>
                <?php
                $str = "";
                $s = "";
                if (empty($pageindex)) {
                    $pageindex = 1;
                }
                $pagesize = intval($pagesize);
                if ($pagesize == 0) {
                    $pagesize = 20;
                }

                if ($keywords != "") {
                    $str .= " and typecode like '%$keywords%'";
                }
                $table = " mx_shebei_type ";
                $order = " order by orderid asc,id";
                $column = " * ";
                $biaoid = "id";
                $c = $db->listcount($table, $str);

                $list = $db->list1($pageindex, $pagesize, $table, $column, $str, $order, $biaoid);

                ?>
                <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1"
                       bgcolor="#d8e8f2">
                    <tr bgcolor="#eff7ff">
                        <td width="5%" align="center" bgcolor="#eff7ff">选择</td>
                        <td width="5%" align="center" bgcolor="#eff7ff">ID</td>
                        <td width="10%" align="center" bgcolor="#eff7ff">设备型号</td>
                        <td width="10%" align="center" bgcolor="#eff7ff">固件版本号</td>
                        <td width="10%" align="center" bgcolor="#eff7ff">创建人</td>
                        <td width="10%" align="center" bgcolor="#eff7ff">创建时间</td>
                        <td width="10%" align="center" bgcolor="#eff7ff">修改人</td>
                        <td width="10%" align="center" bgcolor="#eff7ff">修改时间</td>
                        <td width="10%" align="center" bgcolor="#eff7ff">操作</td>
                    </tr>
                    <form name="form1" id="from1" method="post" action="sys_shebei.php">
                        <?PHp
                        if (!is_array($list) or $c == 0) {
                            echo "<tr class=\"daili1\"><td colspan=9><center>暂无信息</center></td></tr>";
                        } else {
                            for ($i = 0; $i < count($list); $i++) {
								if (!is_array($adminuser[$list[$i]['createid']])){
										$adminuser[$list[$i]['createid']]=$db->get_members($list[$i]['createid']);
								}
								if (!is_array($adminuser[$list[$i]['modiid']])){
										$adminuser[$list[$i]['modiid']]=$db->get_members($list[$i]['modiid']);
								}
                                $adduser=$adminuser[$list[$i]['createid']];
                                $modiuser=$adminuser[$list[$i]['modiid']];
								
                                ?>
                                <tr class="daili1" align="center">
                                    <td width="5%"><input type="checkbox" name="uid[<?php echo $list[$i]["id"] ?>]"
                                                          id="uid" value="<?php echo $list[$i]["id"] ?>"
                                                          class="input_check"></td>
                                    <td><?php echo $list[$i]["id"] ?></td>
                                    <td><?php echo $list[$i]["typecode"]; ?></td>
                                    <td><?php echo $list[$i]["drivecode"] ?></td>
                                    <td align="center"><?php echo $adduser['realname']; ?></td>
                                    <td align="center"><?php echo formatdate($list[$i]["createdate"]) ?></td>
                                    <td align="center"><?php echo $adduser['realname']; ?></td>
                                    <td align="center"><?php echo formatdate($list[$i]["modidate"]) ?></td>
                                    <td align="center"><a
                                            href="sys_shebei.php?actions=modipurview&uid=<?php echo $list[$i]["id"] ?>">修改</a>&nbsp;&nbsp;<a
                                            href="sys_shebei.php?actions=delete&uid=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要删除此设备吗?')">删除</a></td>
                                </tr>
                            <?PHP
                            }
                        }
                        ?>
                        <input type="hidden" name="actions" value=""/>
                    </form>
                </table>
                <?php if ($c > 0) { ?>
                    <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a
                                    href="javascript:CheckAll(document.form1,'')">反选</a></td>
                            <td width="19"><img src="../images/del.jpg" width="14" height="14"/></td>
                            <td width="80"><a
                                    href="javascript:if (confirm('确定要删除选中的信息吗？')){postdo('delall');}"/>删除选中</a></td>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                    </table>
                <?php
                }
                if ($c > $pagesize) {
                    ?>
                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
                        <tr>
                            <td>
                                <?PHP
                                echo $db->ThePage_mxsoft($s, $pageindex, $c, $pagesize)
                                ?>
                            </td>
                        </tr>
                    </table>
                <?php } ?>
            </td>
        </tr>
    </table>
    <script language="javascript">
        window.onload = function showtable() {
            var tablename = document.getElementById("mytable");
            var li = tablename.getElementsByTagName("tr");
            for (var i = 0; i <= li.length; i++) {
                li[i].style.backgroundColor = "#fff";
                li[i].onmouseover = function () {
                    this.style.backgroundColor = "#e8f4ff";
                }
                li[i].onmouseout = function () {
                    this.style.backgroundColor = "#fff"
                }
            }
        }


        function postdo(va) {
            document.form1.actions.value = va;
            document.form1.submit();
        }

    </script>

<?php
}
?>

<?php
require("../mx_foot.php");
?>
<div class="clear"></div>
<div class="height10"></div>
</body>
</html>
