<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=1;  
$PurviewLevel_Others="sys_danye";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function ConfirmDel()
{
   if(confirm("确定要删除选中的信息吗？"))
     return true;
   else
     return false;
	 
}
function postdo(va){
	document.form2.jobs.value=va
	document.form2.submit();
}

var xmlobj; 
function CreateXMLHttpRequest()
{
	if(window.XMLHttpRequest)
	{//Mozilla浏览器
		xmlobj=new XMLHttpRequest();
		if(xmlobj.overrideMimeType)
		{//设置MIME类别
		xmlobj.overrideMimeType("text/xml");
		}
	}
	else if(window.ActiveXObject)
	{ //IE浏览器
		try
		{xmlobj=new ActiveXObject("Msxml2.XMLHttp");}
		catch(e)
		{
			try
			{xmlobj=new ActiveXobject("Microsoft.XMLHttp");}
			catch(e)
			{}
		}
	}

}

function StatHandler_xucode() { //用于处理状态的函数
	if(xmlobj.readyState == 4 && xmlobj.status == 200) //如果URL成功访问，则输出网页 
	{
	    if (xmlobj.responseText=="1"){
			alert("此代理编号已经存在");
		}
	}
}

function check_daili(dname){
	if (dname!=""){
		CreateXMLHttpRequest(); 
		var showurl = 'check_dname.php?dname='+dname; 
		xmlobj.open("GET", showurl, true);
	
		xmlobj.onreadystatechange = StatHandler_xucode;
		xmlobj.send(null); 
	}
}

</SCRIPT>
</head>
<body> 
<?php
if ($actions=="saveadd"){
	$title=trim($title);
	if($title==""){
		$fun->popmassage("单页标题不能为空！","","popback");
		exit();
	}

	$isok=intval($isok);
  	$files=array(
  	"pid"             =>0,
  	"name"            =>$title,
  	"content"         =>$content,
  	"orderid"         =>666,
  	"isok"            =>$isok,
  	"createdate"      =>date("Y-m-d H:i:s"),
  	"createid"        =>$_SESSION['mxwifi']['userid'],
  	);
  	$db->exe_insert("mx_other_infor",$files);
	jump2("单页添加成功","sys_danye.php",2);
	exit();
}
elseif ($actions=="savemodi"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要修改的信息","","popback");
		exit();
	}
	$title=trim($title);
	if($title==""){
		$fun->popmassage("单页标题不能为空！","","popback");
		exit();
	}

	$isok=intval($isok);
  	$files=array(
  	"pid"             =>0,
  	"name"            =>$title,
  	"content"         =>$content,
  	"orderid"         =>666,
  	"isok"            =>$isok,
  	"modidate"        =>date("Y-m-d H:i:s"),
  	"modiid"          =>$_SESSION['mxwifi']['userid'],
  	);
  	$db->exe_update("mx_other_infor",$files,"id='$id'");

    $url=url_code($url);
	jump2("单页修改成功","$url",2);
    exit();
}
elseif ($actions=="delete"){
	  global $db,$fun;
	  $id=intval($id);
	  if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $db->excu("delete from mx_other_infor where id='$id' ");
	  $url=$fromurl?$fromurl:$FROMURL;
	  jump2("单页删除成功",$url,1);
	  exit();
}
elseif ($actions=="delall"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("delete from mx_other_infor where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("单页删除成功",$url,1);
	exit();
  }
elseif ($actions=="isok"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要操作的信息","","popback");
		exit();
	}
	$upsql="update mx_other_infor set isok='1',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$id'";
	
	$db->excu($upsql);
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="noisok"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要操作的信息","","popback");
		exit();
	}
	$upsql="update mx_other_infor set isok='0',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$id'";
	$db->excu($upsql);
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="pisok"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("update mx_other_infor set isok='1',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="pnoisok"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("update mx_other_infor set isok='0',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}


require("../mx_head.php");?>
<?php
  if ($actions=="add"){
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("title","单页标题"))
		return false;
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">新增单页</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="sys_danye.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td width="15%" align="right" bgcolor="#eff7ff">单页标题：</td>
          <td width="85%" colspan="3" bgcolor="#FFFFFF"><input type="text" name="title" id="title" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">详细内容：</td>
          <td colspan="3" bgcolor="#FFFFFF">
          <script charset="utf-8" src="/webedito/kindeditor-min.js"></script>
          <script charset="utf-8" src="/webedito/lang/zh_CN.js"></script>
          <script>
          var editor;
          KindEditor.ready(function(K) {
			  editor = K.create('textarea[id="content"]', {
				  allowFileManager : true,
				  filterMode : false,
				  afterBlur : function() {this.sync();}
			  });
          });
          </script>
          <textarea name="content" id="content" style="width:650px;height:400px;visibility:hidden;display:none;"><?php
$value=htmlspecialchars($rsdb[content]);
echo $value?></textarea>
		  </td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">审核状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <input type="radio" name="isok" value="1" class="input_check" checked="checked"/>&nbsp;已审核&nbsp;<input type="radio" name="isok" value="0" class="input_check"/>&nbsp;未审核
          </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="actions" value="saveadd" />	
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="modipurview"){
//修改管理员
  global $db,$fun;
  $id=trim($id);
  if ($id==0){
  	$fun->popmassage("此信息不存在！","","popback");
	exit();
  }
  $rsdb=$db->getinfo("select top 1 * from mx_other_infor where id='$id'");
  if (!is_array($rsdb)){
    $fun->popmassage("此信息不存在！","","popback");
    exit();
  }
  
  $isokdb[intval($rsdb[isok])]=' checked ';
  if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
  
	foreach ($rsdb as $key =>$value){
		$rsdb[$key]=htmlspecialchars($value);
	}
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("title","单页标题"))
		return false;
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改单页</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="sys_danye.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td width="15%" align="right" bgcolor="#eff7ff">单页标题：</td>
          <td width="85%" colspan="3" bgcolor="#FFFFFF"><input type="text" name="title" id="title" class="xtgk5" value="<?php echo trim($rsdb["name"]);?>"/></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">详细内容：</td>
          <td colspan="3" bgcolor="#FFFFFF">
          <script charset="utf-8" src="/webedito/kindeditor-min.js"></script>
          <script charset="utf-8" src="/webedito/lang/zh_CN.js"></script>
          <script>
          var editor;
          KindEditor.ready(function(K) {
			  editor = K.create('textarea[id="content"]', {
				  allowFileManager : true,
				  filterMode : false,
				  afterBlur : function() {this.sync();}
			  });
          });
          </script>
          <textarea name="content" id="content" style="width:650px;height:400px;visibility:hidden;display:none;"><?php
$value=stripslashes($rsdb["content"]);
echo $value?></textarea>
		  </td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">审核状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <input type="radio" name="isok" value="1" class="input_check" <?php echo $isokdb[1];?>/>&nbsp;已审核&nbsp;<input type="radio" name="isok" value="0" class="input_check" <?php echo $isokdb[0];?>/>&nbsp;未审核
          </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="url" value="<?php echo $gotourl?>">
			<input type="hidden" name="id" value="<?php echo $rsdb[id]?>">
			<input type="hidden" name="actions" value="savemodi">
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">单页列表</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="sys_danye.php">所有单页</a></div>
		<div class="an2"><a href="sys_danye.php?actions=add">添加单页</a></div>
		<div class="an2"><a href="sys_danye.php?isok=1">已审核</a></div>
		<div class="an2"><a href="sys_danye.php?isok=0">未审核</a></div>
		</td>
		</tr>
		</table>
	</div>
	 </div>
<?php
  $str="";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }

  if ($dname!=""){
	$str.=" and dname like '%$dname%' ";
	$s.="dname=$dname&";
  }
  if ($drealname!=""){
	$str.=" and drealname like '%$drealname%' ";
	$s.="drealname=$drealname&";
  }
  
  if ($mobile!=""){
	$str.=" and mobile like '%$mobile%' ";
	$s.="mobile=$mobile&";
  }
  if($memberpur!=""){
  	$memberpur=intval($memberpur);
	$str.=" and Purview='$memberpur' ";
	$s.="memberpur=$memberpur&";
  }
  
  if ($minnum!=""){
  	$minnum=intval($minnum);
	$str.=" and devicenum>='%$minnum%' ";
	$s.="minnum=$minnum&";
  }
  if ($maxnum!=""){
  	$minnum=intval($minnum);
	$str.=" and devicenum<='%$maxnum%' ";
	$s.="maxnum=$maxnum&";
  }
  if ($isok!=""){
  	$isok=intval($isok);
	$str.=" and isok='$isok' ";
	$s.="isok=$isok&";
  }
  
  $table=" mx_other_infor ";
  $order=" order by createdate desc, id desc";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);

 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2">
      <tr bgcolor="#eff7ff">
        <td width="5%" align="center" bgcolor="#eff7ff">ID</td>
        <td width="75%" align="center" bgcolor="#eff7ff">标题</td>
        <td width="10%" align="center" bgcolor="#eff7ff">状态</td>
        <td width="15%" align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="sys_danye.php">
  <?PHp
  if (!is_array($list) or $c==0){
  echo "<tr class=\"daili1\"><td colspan=4><center>暂无信息</center></td></tr>";
  }
  else{
  for($i=0;$i<count($list);$i++){
	$list[$i][isok]=$list[$i][isok]?"<A HREF='?actions=noisok&id=".$list[$i]["id"]."' style='color:red;' title='取消审核'>已审核</A>":"<A HREF='?actions=isok&id=".$list[$i]["id"]."' style='color:blue;' title='设为审核'>未审核</A>";
  ?>
  <tr class="daili1" align="center" > 
    <td width="5%"><?php echo $list[$i]["id"] ?></td>
    <td align="left"><a href="sys_danye.php?actions=modi&id=<?php echo $list[$i]["id"]; ?>"><?php echo $list[$i]["name"] ?></a></td>
    <td><?php echo $list[$i]["isok"] ?></td>
    <td><a href="sys_danye.php?actions=modipurview&id=<?php echo $list[$i]["id"] ?>">修改</a>&nbsp;&nbsp;<a href="sys_danye.php?actions=delete&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要删除此会员吗?')">删除</a></td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>
<?php 
  }

?>

<?php require("../mx_foot.php");?>
</body>
</html>
