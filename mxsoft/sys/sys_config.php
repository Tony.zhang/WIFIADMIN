<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=0;  
$PurviewLevel_Others="sys_config";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link href="../images/css.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php
if ($actions=="setsave"){
	if (is_array($webdbs)){
		foreach ($webdbs as $key =>$value){
			$configinfo=$db->getinfo("select top 1 * from mx_config where c_key='$key'");
			if (!is_array($configinfo)){
				$files=array(
				"c_key"    =>$key,
				"c_value"  =>$value
				);
				$db->exe_insert("mx_config",$files);
			}
			else{
				$files=array(
					"c_value"=>$value
				);
				$db->exe_update("mx_config",$files,"c_key='$key'");
			}
		}
	}
	jump2("操作成功","sys_config.php",2);
	exit();
}
require("../mx_head.php");

$webdb[web_open]?$web_open1='checked':$web_open0='checked';
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/166.gif" width="16" height="16" /></td>
        <td class="xtgk1">系统设置</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	
    <form name="form1" id="form1" action="sys_config.php" method="post" onSubmit="return check_form();">
    <table width="96%" border="0" align="center" cellspacing="1" cellpadding="5" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr bgcolor="#FFFFFF">
            <td width="15%" align="right" bgcolor="#eff7ff">系统名称：</td>
            <td>
            <input type='text' name='webdbs[webname]' id='webname' size='60' value='<?php echo trim($webdb[webname])?>'>
            </td>
        </tr>
        <tr bgcolor="#FFFFFF">
            <td align="right" bgcolor="#eff7ff">系统标题：</td>
            <td>
            <input type='text' name='webdbs[webtitle]' id='webtitle' size='60' value='<?php echo trim($webdb[webtitle])?>'>
            </td>
        </tr>
		<tr bgcolor="#FFFFFF"> 
		  <td align="right" bgcolor="#eff7ff">系统网址：</td>
		  <td><input type='text' name='webdbs[weburl]' size='50' value='<?php echo $webdb[weburl]?>'></td>
		</tr>
		<tr bgcolor="#FFFFFF"> 
		  <td align="right" bgcolor="#eff7ff">系统开启状态：</td>
		  <td>
		  <input type="radio" name="webdbs[web_open]" value="1" <?php echo $web_open1?> style="border:0px;">&nbsp;开放&nbsp;
		  <input type="radio" name="webdbs[web_open]" value="0" <?php echo $web_open0?> style="border:0px;">&nbsp;关闭
		  </td>
		</tr>
		<tr bgcolor="#FFFFFF"> 
		  <td align="right" bgcolor="#eff7ff">上传文件大小：</td>
		  <td><input type='text' size='10' value='<?php echo $webdb[upfileMaxSize]?>' name='webdbs[upfileMaxSize]'> K (留空或为0则不做限制)</td>
		</tr>
		<tr bgcolor="#FFFFFF"> 
      	  <td align="right" bgcolor="#eff7ff">允许上传文件的类型：</td>
      	  <td><input type='text' size='50' value='<?php echo $webdb[upfileType]?>' name='webdbs[upfileType]'>(每个用英文空格隔开,表单留空则不能上传文件)</td>
		</tr>
				<tr bgcolor="#FFFFFF"> 
      	  <td align="right" bgcolor="#eff7ff">wifi管家简介：</td>
      	  <td>  <script charset="utf-8" src="/webedito/kindeditor-min.js"></script>
	  <script charset="utf-8" src="/webedito/lang/zh_CN.js"></script>
	  <script>
	  var editor;
	  KindEditor.ready(function(K) {
	  	  editor = K.create('textarea[id="copyright"]', {
	  	  	  allowFileManager : true,
			  filterMode : false,
			  afterBlur : function() {this.sync();}
	  	  });
	  });
	  </script>
	  <textarea name="webdbs[copyright]" id="copyright" style="width:650px;height:400px;visibility:hidden;display:none;"><?php
$value=htmlspecialchars($webdb[copyright]);
echo $value?></textarea></td>
		</tr>

		<tr bgcolor="#FFFFFF"> 
      	  <td align="right" bgcolor="#eff7ff">推荐是上传文件类型：</td>
      	  <td><font color="#0000FF">.rar .txt .jpg .gif .zip .mp3 .wma .mpeg .mpg .rm .ram .htm .doc .swf .avi .flv .sql</font></td>
		</tr>
		<tr bgcolor="#FFFFFF"> 
      	  <td align="right" bgcolor="#eff7ff">附件目录：</td>
		  <td> <input type='text' value='<?php echo $webdb[updir] ?>' name='webdbs[updir]' size='40'> <font color="#FF0000">(建议不要修改)</font></td>
		</tr>
		<tr bgcolor="#FFFFFF"> 
            <td colspan="2" align="center" ><input type="image" name="imageField" src="../images/dls_1.jpg" /><input type="hidden" name="actions" value="setsave" /></td>
        </tr>
      </table>
    </form>

    </td>
  </tr>
</table>

<?php require("../mx_foot.php");?>
<div class="clear"></div>
<div class="height10"></div>
</body>
</html>