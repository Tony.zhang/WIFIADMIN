<?php
$PurviewLevel=0;
require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<?php
if (!strpos($OtherPurview,'sys_') and $Purview!=1){
	$fun->popmassage("没有此管理权限","","popback");
	exit();
}
?>
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link href="../images/css.css" rel="stylesheet" type="text/css">
</head>
<script language="javascript" src="../js/left.js"></script>
<body>
<table width="180" height="100%" border="0" cellpadding="0" cellspacing="0" style="background:#DEEFFF url(../images/tu.jpg) bottom  no-repeat; border:#6EC1F5 solid 1px;">
  <tr>
    <td width="187" valign="top"><table width="180" border="0" cellpadding="0" cellspacing="0">
     <tr>
        <td width="187" height="31" background="../images/left-01.jpg"><table width="160" border="0" align="center" cellpadding="0" cellspacing="0" onClick="showOneHideOther(1,5)" id="b1">
            <tr>
              <td width="33"><img src="../images/left_1.png" width="21" height="22"></td>
              <td width="127" class="hui">系统设置</td>
            </tr>
       </table></td>
      </tr>
      <tr>
        <td colspan="3">
		<table width="160" border="0" align="center" cellpadding="0" cellspacing="0" style="display:block" id="t1">
		  <?php if ($Purview==1 || strpos($OtherPurview,'sys_config')){?>
		  <tr>
            <td width="29" height="30"><img src="../images/009.gif" width="16" height="16"></td>
            <td width="131" height="40"><a href="sys_config.php" class="font_sy1" target="main">系统设置</a></td>
          </tr>
          <tr><td height="1" colspan="2" bgcolor="#FFFFFF"></td></tr>
		  <?php }
		  if ($Purview==1 || strpos($OtherPurview,'sys_rconfig')){
		  ?>
          <tr>
            <td height="30"><img src="../images/029.gif" width="16" height="16"></td>
            <td height="40"><a href="sys_rconfig.php" class="font_sy1" target="main">设备设置</a></td>
          </tr>
          <tr><td height="1" colspan="2" bgcolor="#FFFFFF"></td></tr>
          <?php }
          if ($Purview==1 || strpos($OtherPurview,'sys_shebei')){
              ?>
              <tr>
                  <td height="30"><img src="../images/029.gif" width="16" height="16"></td>
                  <td height="40"><a href="sys_shebei.php" class="font_sy1" target="main">设备型号</a></td>
              </tr>
              <tr><td height="1" colspan="2" bgcolor="#FFFFFF"></td></tr>
		  <?php }
		  if ($Purview==1 || strpos($OtherPurview,'sys_renzheng')){
		  	?>
  		                <tr>
  		                    <td height="30"><img src="../images/264.gif" width="16" height="16"></td>
  		                    <td height="40"><a href="sys_renzheng.php" class="font_sy1" target="main">认证模版</a></td>
  		                </tr>
  		                <tr><td height="1" colspan="2" bgcolor="#FFFFFF"></td></tr>
  		  <?php }
		  if ($Purview==1 || strpos($OtherPurview,'sys_adminuser')){
		  ?>
		  <tr>
            <td height="30"><img src="../images/382.gif" width="16" height="16"></td>
            <td height="40"><a href="sys_member.php" class="font_sy1" target="main">用户管理</a></td>
          </tr>
		  <tr>
            <td height="1" colspan="2" bgcolor="#FFFFFF"></td>
          </tr>
		  <?php }
		  if ($Purview==1 || strpos($OtherPurview,'sys_danye')){
		  ?>
		  <!--<tr>
            <td height="30"><img src="../images/382.gif" width="16" height="16"></td>
            <td height="40"><a href="sys_danye.php" class="font_sy1" target="main">单页管理</a></td>
          </tr>
		  <tr>
            <td height="1" colspan="2" bgcolor="#FFFFFF"></td>
          </tr>-->
		  <?php }
		  ?>
          <tr>
            <td width="29" height="30"><img src="../images/046.gif" width="16" height="16"></td>
            <td width="131" height="40"><a href="sys_member_log.php" class="font_sy1" target="main">登陆日志</a></td>
          </tr>
          <tr><td height="1" colspan="2" bgcolor="#FFFFFF"></td></tr>
		  <?php
		  if ($Purview==1 || strpos($OtherPurview,'ModifyPwd')){
		  ?>
          <tr>
            <td height="30"><img src="../images/003.gif" width="16" height="16"></td>
            <td height="40"><a href="../Admin_chpass.php" class="font_sy1" target="main">修改密码</a></td>
          </tr>
		  <tr>
            <td height="1" colspan="2" bgcolor="#FFFFFF"></td>
          </tr>
		  <?php }?>
		 
       </table>
		</td>
      </tr>
      
       </table>
    </td>
  </tr>
     
     
	  <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
</table>
</body>
</html>