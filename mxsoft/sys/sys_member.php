<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=0;  
$PurviewLevel_Others="sys_adminuser";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
</head>
<SCRIPT language=javascript>
function ConfirmDel()
{
   if(confirm("确定要删除选中的新闻吗？一旦删除将不能恢复！"))
     return true;
   else
     return false;
	 
}
function CheckAdd()
{
  if(document.form1.username.value=="")
    {
      alert("用户名不能为空！");
	  document.form1.username.focus();
      return false;
    }
  if(document.form1.Password.value=="")
    {
	alert("密码不能为空！");
	document.form1.Password.focus();
	return false;
    }
  if((document.form1.Password.value)!=(document.form1.PwdConfirm.value))
    {
      alert("初始密码与确认密码不同！");
	  document.form1.PwdConfirm.select();
	  document.form1.PwdConfirm.focus();	  
      return false;
    }
  if (document.form1.Purview[1].checked==true){
	GetClassPurview();
  }
}
</SCRIPT>
</head>
<body> 
<?php
if ($actions=="saveadd"){
//添加管理员
  if ($username=="" || empty($username)){
  	$fun->popmassage("用户名不能为空，请正确填写！","","popback");
	exit();
  }
  if ($Password=="" || empty($Password)){
  	$fun->popmassage("密码不能为空，请正确填写！","","popback");
	exit();
  }
  if ($PwdConfirm=="" || empty($PwdConfirm)){
  	$fun->popmassage("密码确认不能为空，请正确填写！","","popback");
	exit();
  }
  if ($Password != $PwdConfirm){
  	$fun->popmassage("两次输入密码不一致，请重新输入！","","popback");
	exit();
  }
  $isok=intval($isok);
  //获得操作权限
  if (isset($U_OtherPurview)){
  foreach( $U_OtherPurview AS $key=>$value){
    if ($UOtherPurview=="" || empty($UOtherPurview)){$UOtherPurview.=$value;}
	else{$UOtherPurview.=",".$value;}
  }
  }
  
  if (isset($fiddb)){
  foreach( $fiddb AS $key=>$value){
	$UArticleIdPurview.="|".$value;
  }
  }
  $UArticleIdPurview.="|";
  
  if ($UPurview=="" || empty($UPurview)){$UPurview=2;}
  if ($UPurview=="1"){
    $UOtherPurview="";
    $UArticleIdPurview="";
  }
  $Password=md5($Password);
  
  $usernum=$db->getinfo("select top 1 username from mx_members where username='$username'");
  if (is_array($usernum)){
  	$fun->popmassage("此管理员已经存在，请重新输入！","","popback");
	exit();
  }
  else{
  	$ip=$_SERVER['REMOTE_ADDR'];
  	$files=array(
  	"username"          =>$username,
  	"realname"          =>$realname,
  	"password"          =>$Password,
  	"Purview"           =>$UPurview,
  	"createdate"        =>date("Y-m-d H:i:s"),
  	"createid"          =>$_SESSION['mxwifi']['userid'],
  	"ArticleIdPurview"  =>$UArticleIdPurview,
  	"OtherPurview"      =>$UOtherPurview,
  	"LastLoginIP"       =>$ip,
  	"LastLoginTime"     =>date("Y-m-d H:i:s"),
  	"LoginTimes"        =>0,
  	"isok"              =>$isok,
  	);
  	$db->exe_insert("mx_members",$files);
	jump2("操作成功","sys_member.php",2);
	exit();
  }
}
elseif($actions=="savemodi"){
//保存修改后的管理员信息
	global $db,$fun;
	$uid=intval($uid);
	if ($uid==0){
		$fun->popmassage("非法访问！","","popback");
		exit();
	}
	if (($Password=="" || empty($Password)) && !($PwdConfirm=="" || empty($PwdConfirm))){
		$fun->popmassage("密码不能为空，请正确填写！","","popback");
		exit();
	}
	if (($PwdConfirm=="" || empty($PwdConfirm)) && !($Password=="" || empty($Password))){
		$fun->popmassage("密码确认不能为空，请正确填写！","","popback");
		exit();
	}
	if (($Password != $PwdConfirm) && ($PwdConfirm=="" || empty($PwdConfirm)) && ($Password=="" || empty($Password))){
		$fun->popmassage("两次输入密码不一致，请重新输入！","","popback");
		exit();
	}
	$isok=intval($isok);
	//获得操作权限

	if (isset($U_OtherPurview)){
	foreach( $U_OtherPurview AS $key=>$value){
		if ($UOtherPurview=="" || empty($UOtherPurview)){$UOtherPurview.=$value;}
		else{$UOtherPurview.=",".$value;}
	}
	}
	if (isset($fiddb)){
	foreach( $fiddb AS $key=>$value){
	$UArticleIdPurview.="|".$value;
	}
	}
	$UArticleIdPurview.="|";

  
	if ($UPurview=="" || empty($UPurview)){$UPurview=2;}
	if ($UPurview=="1"){
		$UOtherPurview="";
    	$UArticleIdPurview="";
	}
  
	$files=array(
  	"realname"          =>$realname,
  	"Purview"           =>$UPurview,
  	"modidate"        =>date("Y-m-d H:i:s"),
  	"modiid"          =>$_SESSION['mxwifi']['userid'],
  	"ArticleIdPurview"  =>$UArticleIdPurview,
  	"OtherPurview"      =>$UOtherPurview,
  	"isok"              =>$isok,
	);
	if(!($PwdConfirm=="" || empty($PwdConfirm)) && !($Password=="" || empty($Password))){
    $files["password"]=md5($Password);
	}

  
	$db->exe_update("mx_members",$files,"uid='$uid'");
	$url=url_code($url);
	if (trim($url)==""){$url="sys_member.php";}
	jump2("操作成功","$FROMURL",2);
	exit();
}
elseif($actions=="delall"){
	global $db,$fun;
	if (!is_array($uid)){
		$fun->popmassage("您没有选择要操作信息，请重新输入！","","popback");
		exit();
	}
	foreach( $uid as $key=>$value){
		$db->excu("delete from mx_members where uid='$key' ");
	}
	jump2("操作成功","$FROMURL",2);
	exit();
}
elseif($actions=="delete"){
	global $db,$fun;
	$uid=intval($uid);
	if ($uid==0){
		$fun->popmassage("您没有选择要操作信息，请重新输入！","","popback");
		exit();
	}
	$db->excu("delete from mx_members where uid='$uid' ");
	jump2("操作成功","$FROMURL",2);
	exit();
}
elseif($actions=="unisok"){
	global $db,$fun;
	$uid=intval($uid);
	if ($uid==0){
		$fun->popmassage("您没有选择要操作信息，请重新输入！","","popback");
		exit();
	}
	$db->excu("update mx_members set isok='0' where uid='$uid' ");
	jump2("操作成功","$FROMURL",2);
	exit();
}
elseif($actions=="isok"){
	global $db,$fun;
	$uid=intval($uid);
	if ($uid==0){
		$fun->popmassage("您没有选择要操作信息，请重新输入！","","popback");
		exit();
	}
	$db->excu("update mx_members set isok='1' where uid='$uid' ");
	jump2("操作成功","$FROMURL",2);
	exit();
}
elseif($actions=="pisok"){
	global $db,$fun;
	if (!is_array($uid)){
		$fun->popmassage("您没有选择要操作信息，请重新输入！","","popback");
		exit();
	}
	foreach( $uid as $key=>$value){
		$db->excu("update mx_members set isok='1' where uid='$key' ");
	}
	jump2("操作成功","$FROMURL",2);
	exit();
}
elseif($actions=="punisok"){
	global $db,$fun;
	if (!is_array($uid)){
		$fun->popmassage("您没有选择要操作信息，请重新输入！","","popback");
		exit();
	}
	foreach( $uid as $key=>$value){
		$db->excu("update mx_members set isok='0' where uid='$key' ");
	}
	jump2("操作成功","$FROMURL",2);
	exit();
}


require("../mx_head.php");?>

<?php  
if ($actions=="add"){
//添加管理员
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/166.gif" width="16" height="16" /></td>
        <td class="xtgk1">新增后台用户</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form name="form1" method="post" action="sys_member.php" onSubmit="return CheckAdd();">
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td width="15%" align="right" bgcolor="#eff7ff">用 户 名：</td>
          <td width="35%" bgcolor="#FFFFFF"><input type="text" name="username" id="username" class="xtgk5" /></td>
          <td width="15%" align="right" bgcolor="#EFF7FF">真实姓名：</td>
          <td width="35%" bgcolor="#FFFFFF"><input type="text" name="realname" id="realname" class="xtgk5" /></td>
          </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">密&nbsp;&nbsp;&nbsp;&nbsp;码：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="Password" id="Password" class="xtgk5" /></td>
          <td align="right" bgcolor="#EFF7FF">密码确认：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="PwdConfirm" id="PwdConfirm" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">用户状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <input type="radio" name="isok" value="1" class="input_check" checked="checked"/>&nbsp;正常&nbsp;<input type="radio" name="isok" value="0" class="input_check"/>&nbsp;锁定
          </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">权限设置：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		    <input name="UPurview" type="radio" value="1" onClick="PurviewDetail.style.display='none'" class="input_check">超级管理员&nbsp;&nbsp;
		    <input type="radio" name="UPurview" value="2" onClick="PurviewDetail.style.display=''" class="input_check" checked="checked">普通管理员：
		  </td>
          </tr>
	<tr bgcolor="#FFFFFF">
      <td colspan="4">
	    <table id="PurviewDetail" width="100%" border="0" cellspacing="10" cellpadding="0">
          <tr valign="top">
             <td width="49%">           
            <fieldset>
            <legend>网站管理权限</legend>
            <table width="100%" border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td height="30" width="25%"><input name="U_OtherPurview[]" type="checkbox" value="daili_" class="input_check" <?php echo $U_OtherPurview["daili_"]?>>代理商管理</td>
                <td width="25%"><input name="U_OtherPurview[]" type="checkbox" value="shebei_" class="input_check" <?php echo $U_OtherPurview["shebei_"]?>>设备管理</td>
                <td width="25%"><input name="U_OtherPurview[]" type="checkbox" value="tongji_" class="input_check" <?php echo $U_OtherPurview["tongji_"]?>>商户统计</td>
                <td width="25%"><input name="U_OtherPurview[]" type="checkbox" value="jieru_" class="input_check" <?php echo $U_OtherPurview["jieru_"]?>>接入页面</td>
			  </tr>
			  <tr>
                <td height="30"><input name="U_OtherPurview[]" type="checkbox" value="sys_config" class="input_check" <?php echo $U_OtherPurview["sys_config"]?>>系统设置</td>
                <td><input name="U_OtherPurview[]" type="checkbox" value="sys_rconfig" class="input_check" <?php echo $U_OtherPurview["sys_config"]?>>系统设置</td>
                <td><input name="U_OtherPurview[]" type="checkbox" value="sys_adminuser" class="input_check" <?php echo $U_OtherPurview["sys_adminuser"]?>>后台用户管理</td>
                <td><input name="U_OtherPurview[]" type="checkbox" value="ModifyPwd" class="input_check" <?php echo $U_OtherPurview["ModifyPwd"]?>>修改密码</td>
              </tr>
            </table>
            </fieldset>
		  </td>
        </tr>
      </table>
	 </td>
    </tr>
        <tr>
          <td colspan="8" align="center" bgcolor="#FFFFFF"><input type="image" name="imageField" src="../images/dls_1.jpg" /><input type="hidden" name="actions" value="saveadd"></td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
}

elseif ($actions=="modipurview"){
//修改管理员
  global $db,$fun;
  $uid=intval($uid);
  if ($uid==0){
  	$fun->popmassage("此管理员不存在！","","popback");
	exit();
  }
  $rsmenber=$db->getinfo("select top 1 * from mx_members where uid='$uid'");
  if (!is_array($rsmenber)){
    $fun->popmassage("此管理员不存在！","","popback");
    exit();
  }

  foreach (explode(",",$rsmenber[OtherPurview]) as $key => $value){
    $U_OtherPurview[$value]="checked";
  }
  if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/166.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改后台用户</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form name="form1" method="post" action="sys_member.php" onSubmit="return CheckAdd();">
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td width="15%" align="right" bgcolor="#eff7ff">用 户 名：</td>
          <td width="35%" bgcolor="#FFFFFF"><?php echo $rsmenber['username'];?><input name="uid" type="hidden" value="<?php echo $rsmenber['uid'];?>"></td>
          <td width="15%" align="right" bgcolor="#EFF7FF">真实姓名：</td>
          <td width="35%" bgcolor="#FFFFFF"><input type="text" name="realname" id="realname" class="xtgk5" value="<?php echo trim($rsmenber['realname']);?>" /></td>
          </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">密&nbsp;&nbsp;&nbsp;&nbsp;码：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="Password" id="Password" class="xtgk5" /></td>
          <td align="right" bgcolor="#EFF7FF">密码确认：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="PwdConfirm" id="PwdConfirm" class="xtgk5" /></td>
        </tr>
		<tr bgcolor="#FFFFFF"><td colspan="4" style=" color:#FF0000" align="center">密码不修改请留空</td></tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">用户状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <input type="radio" name="isok" value="1" class="input_check" <?php if ($rsmenber['isok']==1){echo "checked";}?> />&nbsp;正常&nbsp;<input type="radio" name="isok" value="0" class="input_check" <?php if ($rsmenber['isok']==0){echo "checked";}?> />&nbsp;锁定
          </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">权限设置：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		    <input name="UPurview" type="radio" value="1" onClick="PurviewDetail.style.display='none'" class="input_check" <?php if ($rsmenber['Purview']==1){echo "checked";} ?>>超级管理员&nbsp;&nbsp;
		    <input type="radio" name="UPurview" value="2" onClick="PurviewDetail.style.display=''" class="input_check" <?php if ($rsmenber['Purview']==2){echo "checked";} ?>>普通管理员：
		  </td>
          </tr>
	<tr bgcolor="#FFFFFF">
      <td colspan="4">
	    <table id="PurviewDetail" width="100%" border="0" cellspacing="10" cellpadding="0" style="display:<?php if ($rsmenber['Purview']!=2){echo "none";} ?>;">
          <tr valign="top">
             <td width="49%">           
            <fieldset>
            <legend>网站管理权限</legend>
            <table width="100%" border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td height="30" width="25%"><input name="U_OtherPurview[]" type="checkbox" value="daili_guanli" class="input_check" <?php echo $U_OtherPurview["daili_guanli"]?>>代理商管理</td>
                <!--<td height="30" width="25%"><input name="U_OtherPurview[]" type="checkbox" value="daili_view" class="input_check" <?php echo $U_OtherPurview["daili_view"]?>>代理商查看</td>-->
                <td width="25%"><input name="U_OtherPurview[]" type="checkbox" value="shebei_" class="input_check" <?php echo $U_OtherPurview["shebei_"]?>>设备管理</td>
                <td width="25%"><input name="U_OtherPurview[]" type="checkbox" value="tongji_" class="input_check" <?php echo $U_OtherPurview["tongji_"]?>>商户统计</td>
                <td width="25%"><input name="U_OtherPurview[]" type="checkbox" value="jieru_" class="input_check" <?php echo $U_OtherPurview["jieru_"]?>>接入页面</td>
			  </tr>
			  <tr>
                <td height="30"><input name="U_OtherPurview[]" type="checkbox" value="sys_config" class="input_check" <?php echo $U_OtherPurview["sys_config"]?>>系统设置</td>
                <td><input name="U_OtherPurview[]" type="checkbox" value="sys_rconfig" class="input_check" <?php echo $U_OtherPurview["sys_config"]?>>系统设置</td>
                <td><input name="U_OtherPurview[]" type="checkbox" value="sys_adminuser" class="input_check" <?php echo $U_OtherPurview["sys_adminuser"]?>>后台用户管理</td>
                <td><input name="U_OtherPurview[]" type="checkbox" value="ModifyPwd" class="input_check" <?php echo $U_OtherPurview["ModifyPwd"]?>>修改密码</td>
              </tr>
            </table>
            </fieldset>
		  </td>
        </tr>
      </table>
	 </td>
    </tr>
        <tr>
          <td colspan="8" align="center" bgcolor="#FFFFFF"><input type="image" name="imageField" src="../images/dls_1.jpg" />
	    <input type="hidden" name="url" value="<?php echo $gotourl?>">
        <input type="hidden" name="actions" value="savemodi">
		  </td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
}

else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">后台用户列表</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="sys_member.php">用户管理</a></div>
		<div class="an2"><a href="sys_member.php?actions=add">添加用户</a></div>
		<div class="an2"><a href="sys_member.php?isok=1">正常</a></div>
		<div class="an2"><a href="sys_member.php?isok=0">禁用</a></div>
		<div class="an2"><a href="sys_member.php?memberpur=1">超级管理员</a></div>
		<div class="an2"><a href="sys_member.php?memberpur=2">普通管理员</a></div>
		</td>
		</tr>
		</table>
	</div>
      <table border="0" align="left" cellpadding="3" cellspacing="0">
	<form id="search_form" name="search_form" method="get" action="sys_member.php">
        <tr>
		<td>搜索类型：</td>
      	<td>
      	<select name="type">
      	<option value="">用户名</option>
      	<option value="uid">用户uid</option>
      	</select>
      	</td><td>&nbsp;关键字：</td><td><input name="keywords" type="text" id="keywords" value="" size="10" class="search_input"/></td>
      	<td>&nbsp;状态：</td>
      	<td><select name="isok"><option value="">不限</option><option value="1">正常</option><option value="0">锁定</option></select></td>
      	<td><select name="pagesize"><option value="20">20条/页</option><option value="30" <?php if ($pagesize==30){echo "selected";}?>>30条/页</option><option value="50" <?php if ($pagesize==50){echo "selected";}?>>50条/页</option></select></td>
      	<td><input type="image" name="imageField" src="../images/search.jpg" /></td>
        </tr>
	 </form>
      </table>
	 </div>
<?php
  $str="";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }

  if ($keywords!=""){
    if ($type=="uid"){
      $str.=" and uid='$keywords'";
	}
	else{
      $str.=" and username='$keywords' ";
	}
  }
  if($isok!=""){
  	$isok=intval($isok);
	$str.=" and isok='$isok' ";
	$s.="isok=$isok&";
  }
  
  if($memberpur!=""){
  	$memberpur=intval($memberpur);
	$str.=" and Purview='$memberpur' ";
	$s.="memberpur=$memberpur&";
  }
  
  
  $table=" mx_members ";
  $order=" order by uid desc";
  $column=" * ";
  $biaoid="uid";
  $c=$db->listcount($table,$str);
  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);

 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2">
      <tr bgcolor="#eff7ff">
        <td width="5%" align="center" bgcolor="#eff7ff">选择</td>
        <td width="10%" align="center" bgcolor="#eff7ff">ID</td>
        <td width="10%" align="center" bgcolor="#eff7ff">用户名</td>
        <td width="10%" align="center" bgcolor="#eff7ff">真实姓名</td>
        <td width="10%" align="center" bgcolor="#eff7ff">用户权限</td>
        <td width="15%" align="center" bgcolor="#eff7ff">最后登录</td>
        <td width="15%" align="center" bgcolor="#eff7ff">最后登录IP</td>
        <td width="10%" align="center" bgcolor="#eff7ff">状态</td>
        <td width="15%" align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="sys_member.php">
  <?PHp
  if (!is_array($list) or $c==0){
  echo "<tr class=\"daili1\"><td colspan=9><center>暂无信息</center></td></tr>";
  }
  else{
  for($i=0;$i<count($list);$i++){
	$list[$i][isok]=$list[$i][isok]?"<A HREF='?actions=unisok&uid=".$list[$i]["uid"]."' style='color:red;' title='锁定用户'>正常</A>":"<A HREF='?actions=isok&uid=".$list[$i]["uid"]."' style='color:blue;' title='设为正常'>锁定</A>";
  ?>
  <tr class="daili1" align="center"> 
    <td width="5%"><input type="checkbox" name="uid[<?php echo $list[$i]["uid"] ?>]" id="uid" value="<?php echo $list[$i]["uid"] ?>" class="input_check"></td>
    <td><?php echo $list[$i]["uid"] ?></td>
    <td><?php echo $list[$i]["username"] ?></td>
    <td><?php echo $list[$i]["realname"] ?></td>
    <td align="center"><?php if ($list[$i]["Purview"]==1 ){echo "超级管理员";}else {echo "普通管理员";}?></td>
    <td align="center"><?php echo $list[$i]["LastLoginTime"] ?></td>
    <td align="center"><?php echo $list[$i]["LastLoginIP"] ?></td>
    <td align="center"><?php echo $list[$i]["isok"] ?></td>
    <td align="center"><a href="sys_member.php?actions=modipurview&uid=<?php echo $list[$i]["uid"] ?>">修改</a>&nbsp;&nbsp;<a href="sys_member.php?actions=delete&uid=<?php echo $list[$i]["uid"] ?>" onClick="return confirm('你确实要删除此会员吗?')">删除</a></td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php if ($c>0){?>
      <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a href="javascript:CheckAll(document.form1,'')">反选</a></td>
          <td width="19"><img src="../images/del.jpg" width="14" height="14" /></td>
          <td width="80"><a href="javascript:if (confirm('确定要删除选中的信息吗？')){postdo('delall');}"/>删除选中</a></td>
          <td width="80"><a href="javascript:postdo('pisok');"/>设为正常</a></td>
          <td><a href="javascript:postdo('punisok');"/>禁用用户</a></td>
        </tr>
      </table>
	  <?php
	  }
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>

<?php 
}
?>

<?php
require("../mx_foot.php");
?>
<div class="clear"></div>
<div class="height10"></div>
</body>
</html>
