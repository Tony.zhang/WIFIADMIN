<?php
/**
 * 服务器文件选择框
 */
if (empty($activepath)) {
    $activepath = '';
}
if (empty($imgstick)) {
    $imgstick = '';
}
$imgstick = 'smallundefined';
$noeditor = isset($noeditor) ? $noeditor : '';
$activepath = $_GET['activepath'];
$activepath = str_replace('.', '', $activepath);
$activepath = preg_replace("#\/{1,}#", '/', $activepath);
$_path = dirname(__FILE__);
//获取网站根目录
$_path = substr($_path, 0, -11);
//判断文件路径
if (strlen($activepath) < strlen('/upload_files/gujianbao')) {
    $activepath = '/upload_files/gujianbao';
}
$inpath = $_path . $activepath;
$activeurl = '..' . $activepath;


if (empty($f)) {
    $f = 'form1.picname';
}
if (empty($v)) {
    $v = 'picview';
}
if (empty($comeback)) {
    $comeback = '';
}
$addparm = '';
if (!empty($CKEditor)) {
    $addparm = '&CKEditor=' . $CKEditor;
    $f = $CKEditor;
}
if (!empty($CKEditorFuncNum)) {
    $addparm .= '&CKEditorFuncNum=' . $CKEditorFuncNum;
}

if (!empty($noeditor)) {
    $addparm .= '&noeditor=yes';
}
?>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=<?php echo $cfg_soft_lang; ?>'>
    <title>固件包浏览</title>
    <link href='../../plus/../images/base.css' rel='stylesheet' type='text/css'>
    <style>
		body,html{ color:#0873A2;}
		a:link{color: #0873A2;text-decoration: none;  font-size:14px} 
		a:visited{color: #0873A2; text-decoration:none; font-size:14px}
		a:hover{color: #A10506;text-decoration:none;  font-size:14px}
		a:active{color: #0873A2; text-decoration:none; font-size:14px}
        .linerow {
            border-bottom: 1px solid #eff7ff;
        }

        .napisdiv {
            left: 40;
            top: 3;
            width: 150px;
            height: 100px;
            position: absolute;
            z-index: 3;
            display: none;
        }
    </style>
    <script>
        function nullLink() {
            return;
        }
        function ChangeImage(surl) {
            document.getElementById('picview').src = surl;
        }
    </script>
</head>
<body style="background:#fff;" leftmargin='0' topmargin='0'>
<div id="floater" class="napisdiv">
    <a href="javascript:nullLink();" onClick="document.getElementById('floater').style.display='none';"><img
            src='../images/picviewnone.gif' id='picview' border='0' alt='单击关闭预览'></a>
</div>
<SCRIPT language=JavaScript src="js/float.js"></SCRIPT>
<SCRIPT language=JavaScript>
    function nullLink() {
        return;
    }
    function ChangeImage(surl) {
        document.getElementById('floater').style.display = 'block';
        document.getElementById('picview').src = surl;
    }
    function TNav() {
        if (window.navigator.userAgent.indexOf("MSIE") >= 1) return 'IE';
        else if (window.navigator.userAgent.indexOf("Firefox") >= 1) return 'FF';
        else return "OT";
    }
    // 获取地址参数
    function getUrlParam(paramName) {
        var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i');
        var match = window.location.search.match(reParam);
        return (match && match.length > 1) ? match[1] : '';
    }

    function ReturnImg(reimg) {
        var funcNum = getUrlParam('CKEditorFuncNum');
        if (funcNum > 1) {
            var fileUrl = reimg;
            window.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
        }
        if (window.opener.document.<?php echo $f?> != null) {
            window.opener.document.<?php echo $f?>.value = reimg;
            if (window.opener.document.getElementById('div<?php echo $v?>')) {
                if (TNav() == 'IE') {
                    //window.opener.document.getElementById('div<?php echo $v?>').filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src = reimg;
                    window.opener.document.getElementById('div<?php echo $v?>').src = reimg;
                    window.opener.document.getElementById('div<?php echo $v?>').style.width = '150px';
                    window.opener.document.getElementById('div<?php echo $v?>').style.height = '100px';
                }
                else
                    window.opener.document.getElementById('div<?php echo $v?>').style.backgroundImage = "url(" + reimg + ")";
            }
            else if (window.opener.document.getElementById('<?php echo $v?>')) {
                window.opener.document.getElementById('<?php echo $v?>').src = reimg;
            }
            if (document.all) window.opener = true;
        }

        window.close();
    }
</SCRIPT>
<table width='100%' border='0' cellspacing='0' cellpadding='0' align="center">
    <tr>
        <td align='right'>
            <table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#fff'>
                <tr bgcolor='#fff'>
                    <td>
                        <table width='100%' border='0' cellspacing='1' cellpadding='6' bgcolor='#d8e8f2'>
                            <tr bgcolor="#eff7ff">
                                <td width="50%" align="center" bgcolor="#eff7ff" class='linerow'><strong>点击名称选择固件包</strong>
                                </td>
                                <td width="50%" align="center" bgcolor="#eff7ff" class='linerow'><strong>最后修改时间</strong>
                                </td>
                            </tr>
                            <?php
                            $dh = dir($inpath);
                            $ty1 = "";
                            $ty2 = "";
                            while ($file = $dh->read()) {
                                //-----计算文件大小和创建时间
                                if ($file != "." && $file != ".." && !is_dir("$inpath/$file")) {
                                    $filesize = filesize("$inpath/$file");
                                    $filesize = $filesize / 1024;
                                    if ($filesize != "")
                                        if ($filesize < 0.1) {
                                            @list($ty1, $ty2) = split("\.", $filesize);
                                            $filesize = $ty1 . "." . substr($ty2, 0, 2);
                                        } else {
                                            @list($ty1, $ty2) = split("\.", $filesize);
                                            $filesize = $ty1 . "." . substr($ty2, 0, 1);
                                        }
                                    $filetime = filemtime("$inpath/$file");
                                    $filetime = date("Y-m-d H:i:s", $filetime);
                                }

                                if ($file == ".") continue;
                                else if ($file == "..") {
                                    if ($activepath == "") continue;
                                    $tmp = preg_replace("#[\/][^\/]*$#i", "", $activepath);
                                    $line = "\n<tr>
                                   <td class='linerow' colspan='2' bgcolor='#fff'>
                                   <a href='select_images.php?imgstick=$imgstick&v=$v&f=$f&activepath=" . urlencode($tmp) . $addparm . "'><img src=../images/dir2.gif border=0 width=16 height=16 align=absmiddle>上级目录</a></td>
                                   </tr>
                                   ";
                                    echo $line;
                                } else if (is_dir("{$inpath}/{$file}")) {
                                    if (preg_match("#^_(.*)$#i", $file)) continue; #屏蔽FrontPage扩展目录和linux隐蔽目录
                                    if (preg_match("#^\.(.*)$#i", $file)) continue;
                                    $line = "\n<tr>
                                   <td bgcolor='#fff' class='linerow' colspan='2'>
                                   <a href='select_images.php?imgstick=$imgstick&v=$v&f=$f&activepath=" . urlencode("$activepath/$file") . $addparm . "'><img src=../images/dir.gif border=0 width=16 height=16 align=absmiddle>$file</a></td>
                                   </tr>";
                                    echo "$line";
                                } else if (preg_match("#\.(rar)#i", $file)) {
                                    $reurl = "$activeurl/$file";
                                    $reurl = preg_replace("#^\.\.#", "", $reurl);
                                    if ($cfg_remote_site == 'Y' && $remoteuploads == 1) {
                                        $reurl = $remoteupUrl . $reurl;
                                    } else {
                                        $reurl = $reurl;
                                    }

                                    if ($file == $comeback) $lstyle = " style='color:red' ";
                                    else  $lstyle = "";
                                    $line = "\n<tr>
                                   <td class='linerow' bgcolor='#fff'>
                                   <a href=# onclick=\"getVal('http://{$_SERVER[HTTP_HOST]}{$activepath}/{$file}');\" $lstyle><img src=../images/gif.gif border=0 width=16 height=16 align=absmiddle>$file</a></td>
                                   <td align='center' class='linerow' bgcolor='#fff'>$filetime</td>
                                   </tr>";
                                    echo "$line";
                                }
                            }
                            //End Loop
                            $dh->close();
                            ?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<script type="text/javascript">
    //把值传给父页面
    function getVal(obj) {

        window.parent.document.getElementById('web_adpic').value = obj;
        parentDialog.close();
    }
</script>