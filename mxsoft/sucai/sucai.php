<?php
@session_start();
require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
$fromurl = "sucai.php";
$table = "mx_sucai";
if($type == "delete"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	$mubaninfo=$db->getinfo("select top 1 * from mx_sucai where id='$id'");
	if (!is_array($mubaninfo)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	@unlink("../../$webdb[updir]$mubaninfo[picurl]");
	$db->excu("delete from mx_sucai where id='$id' ");
	jump2("素材删除成功",$fromurl."?pageindex=".$pageindex,1);
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>铭讯EOS网站后台管理</title>
<link rel="stylesheet" href="../images/css.css" type="text/css">
<style>
/* pinpai_biaoti */
.pinpai_biaoti{font-size: 14px;font-weight: bold;margin: 15px;position: relative;text-align: center;}
</style>
<link rel="stylesheet" href="/mxsoft/js/waterfall/css/reset.css" />
<link rel="stylesheet" href="/mxsoft/js/waterfall/css/main.css" />
<link rel="stylesheet" href="/mxsoft/js/waterfall/css/woo.css" />
<link rel="stylesheet" href="/mxsoft/js/waterfall/css/gotop.css" />
<link rel="stylesheet" href="/mxsoft/js/waterfall/css/pagine.css" />
<link rel="stylesheet" href="/mxsoft/js/waterfall/css/resize.css" />
<script src="/mxsoft/js/waterfall/js/jquery-1.6.1.min.js"></script>
<script src="/mxsoft/js/waterfall/js/browser.js"></script>
<script src="/mxsoft/js/waterfall/js/history.js"></script>
<script src="/mxsoft/js/waterfall/js/template.min.js"></script>
<script src="/mxsoft/js/waterfall/js/tabswitch.js"></script>
<script src="/mxsoft/js/waterfall/js/woo.js"></script>
<script src="/mxsoft/js/waterfall/js/masnunit.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

</head>
<body>
<?php 
	$str="";
	$s="";
	if ($title!=""){
		$str.=" and title like '%$title%' ";
		$s.="title=$title&";
	}
	if ($createid!=""){
		$str.=" and createid=".$createid;
		$s.="createid=$createid&";
	}
	if ($fid!=""){
		$str.=" and fid =".$fid;
		$s.="fid=$fid&";
	}
	if(empty($pageindex)){
      $pageindex=1;
  	}
	$pagesize=15;
	$order=" order by createdate desc,id asc";
	$column=" * ";
	$biaoid="id";
	$c=$db->listcount($table,$str);
	$list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);
	foreach($list as $k => $v){
		if($v['fid']){
			$list[$k]['fidName'] = $db->getsingle("select name from mx_hangye_class where fid=".$v['fid']);
		}
		if($v['createid']){
			$list[$k]['createName'] =$db->getsingle("SELECT username FROM mx_members WHERE uid=".$v['createid']);
		}
	}
?>

<div class="pinpai_biaoti">
	<form action="sucai.php" method="get" name="search_form" id="search_form">
      	<span>&nbsp;标题：<input type="text" class="search_input" size="12" value="" name="title"></span>
      	<span>&nbsp;创建人：<?php
      	$query=$db->excu("select uid,username,realname from mx_members");
      	$ar=array();
      	while($re=$db->fetch_array($query)){
      		array_unshift($ar,$re);
      	}
      	echo "<select name='createid'><option value=''>选择创建人</option>";
      	foreach($ar as $k => $v){
      	?>
      	<option value="<?php echo $v['uid'];?>"><?php echo $v['username'];?></option>
      	<?php echo "</select>"; }?>
      	</span>
      	<span>&nbsp;所属行业：<?php
          $sort_fup=$Guidedb->Select("mx_hangye_class","fid","","","",0,"","","","","","",'');
          echo $sort_fup;
		  ?></span>
      	<input type="image" src="../images/search.jpg" style="margin:-5px 0 0 10px;position: absolute;" name="imageField">
      </form>
</div>
<div id="content">
	<div id="woo-holder">
		<!-- 点击回到顶部时，会回到这个锚点所在处，此锚点可放置任意位置 -->
		<!-- 具体代码在 scrollTo() 方法中 -->
		<a name="woo-anchor"></a>
		<ul id="switchholder" style="display:none">
		<li class="woo-swa"><a href="javascript:;">我的图片</a></li>
		</ul>

		<div class="woo-swb">
			<div class="woo-pcont woo-masned my-pic" data-totalunits="440">
				<!-- .woo-pcont 节点内可预先放好若干个单元(个数没有限制) -->
				<!-- 预先放置的会被当做第一子页数据，后面会直接从第二子页开始 -->
				<!-- 可以选择不放置 -->
		<?php
		if($list){
		foreach($list as $k => $v){?>
			<div data-id="<?php echo $v['id'];?>" class="woo">
				<div class="j">
					<div class="d" style="text-align: center"><?php echo $v['title']?></div>
					<div class="mbpho">
						<a class="fancybox-effects-d a" data-fancybox-group="gallery" href="<?php echo get_photourl($v['picurl']);?>" title="<?php echo $v['title'];?><br/> 行业：<?php echo $v['fidName'];?>&nbsp;&nbsp;创建时间：<?php echo $v['createdate'];?>">
							<img src="<?php echo get_photourl($v['picurl']);?>"/>
						</a>
					</div>
					<div class="g">创建人：<?php echo $v['createName'];?></div>
					<div class="g">创建时间：<?php echo $v['createdate'];?></div>
					<div class="g"><span class="fidname">行业：<?php echo $v['fidName'];?></span>&nbsp;&nbsp;&nbsp;&nbsp;操作：<a href="edit_sucai.php?actions=modi&id=<?php echo $v['id'];?>&pageIndex=<?php echo $pageindex;?>">修改</a>&nbsp;<a href="javascript:void(0);" targetid="<?php echo $v['id'];?>" class="del" >删除</a></div>
				</div>
			</div>
		<?php }
		}else{?>
		<div style="border: medium none; height: auto; padding: 10px; width: auto;text-align:center;">
			<div style="background: none repeat scroll 0 0 #FFFFFF; border-radius: 8px; box-shadow: 0 0 11px #666666; height: auto;text-align:center; width: 100%;">
				暂无信息^_^.
			</div>
		</div>
		<?php }?>


			</div>
			<div class="woo-pager"></div>
		</div>

	</div>
</div>
<style>
.pinpai_page{font-size: 14px;font-weight: bold;margin-top: 25px;position: relative;text-align: center;}
</style>
<div class="pinpai_page"><?php
	  if ($c>$pagesize){?>
      <table border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?></div>
<script>
	DEBUG = true;
 	DEBUG_DATA = [];
</script>
<script>
$(function (){
	waterfall();
})

function waterfall(){
	// 可自行修改代码添加瀑布流的三个控制键 gonext gopre gotop
	$('<div id="mycontrols"><a href="javascript:;" id="gotop"></a></div>').appendTo('body');

	var conf = {
		// 每个请求对应的form 的id 前缀，和 arrform 标识数组结合。
		"formprefix" : '#woo-form-',
		// 标识数组，每个标识对应一个瀑布流，本例有四个瀑布流。
		// 容易看出每个标识前加上 formprefix 就能对应#win-holder 里的form 表单：
		// <form id="woo-form-(collect|original|album|favalbum)">
		"arrform" : ['album','favalbum','collect','original'], 

		// 请求地址在页码处split分开的后半截(例如/message/list/1/?type=hot)
		// 前半截在 <form action="/message/list/"> action 里，
		// 后半截作为 arrsplit 参数值，上例中其值为 '/?type=hot'。
		// 前后半截均不包含页码数。
		// 这是为了应对页码数被设计在url 中间的悲剧情况。
		// 请将页码数设计在地址最后的位置如 <form action="/people/list/?page=">
		// 那么对应的 arrsplit 参数值为空字符串。
		"arrsplit" : ['/?type=hot','','',''],

		// 瀑布流每一列扩展宽度，此宽度包含两列之间的间距。
		"arrmasnw" : [245,250,250,245],

		// 瀑布流两列之间的间距。此例第一个瀑布流的可视宽度为 245-21=224
		"arrmargin" : [21,42,42,21],


		// 第一列特殊宽度 默认等于 arrmasnw。
		// 如果想让第一列(可左数第一列，也可右数第一列)宽度不同于其它列，
		// arrfmasnw 应设置为第一列和其它列的宽度差值。
		// 为什么参数值是一个数字0 而不是数组[0,0,0,0]？
		// 这里简化了传参，如果数组里所有元素的值相同，可以只传这个值。
		// 同理，上方的参数 arrsplit arrmasnw arrmargin 包括 arrgap 均适用这一简化规则
		// arrform 不适用这一简化规则，原因不解释。
		"arrfmasnw" : 0,

		// 通过 gap 可以设置同一列单元块之间的垂直间距
		"arrgap" : 0,

		/////////////////// 【上方各参数必须填写，否则后果自负】

		// 可自由选择安装 $gopre $gonext $gotop 等操作键。
		"gotop" : '#gotop',

		// footer 选择器，只要处于瀑布流翻页器下方的都可视为 footer。
		// 它的隐藏和显示将会受到控制。
		"footer" : '#footer,#preserve',

		// window resize 时，是否重绘瀑布流，默认不重绘。此功能打开，resize.css 才有意义
		"resize" : true,

		"pageSwitch":false,//分页开关	added by Tony
		
		// scroll 过程中触发的方法，可用于登录弹出框等
		"onScroll" : function (tp){
			// tp 为当前的scrolltop
//			if( (typeof ALREADYNOTICED === 'undefined' || !ALREADYNOTICED) && tp >= 1000 ){
//				ALREADYNOTICED = true
//				alert("弹出登录框")
//			}
		}
	}

	// 启动瀑布流，好短
	$.Woo(conf);
}
</script>
<script type="text/javascript">
$(document).ready(function() {
	
	 $('.fancybox').fancybox({
		  padding : 0,
		  autoScale:true,
		  width:750,
		  openEffect: 'elastic'
		 });

	/*
	 *  Different effects
	 */

	// Set custom style, close if clicked, change title type and overlay color
	$(".fancybox-effects-c").fancybox({
		wrapCSS    : 'fancybox-custom',
		closeClick : true,

		openEffect : 'none',

		helpers : {
			title : {
				type : 'inside'
			},
			overlay : {
				css : {
					'background' : 'rgba(238,238,238,0.85)'
				}
			}
		}
	});

	// Remove padding, set opening and closing animations, close if clicked and disable overlay
	$(".fancybox-effects-d").fancybox({
		padding: 0,

		openEffect : 'elastic',
		openSpeed  : 150,

		closeEffect : 'elastic',
		closeSpeed  : 150,

		closeClick : true,

		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(238,238,238,0.85)'
				}
			}
		},

		afterLoad : function() {
			this.title = this.title;
		}
	});

});

$(".woo .del").live('click',function(){
	if(confirm('你确定要删除该素材吗？')){
		var targetid = $(this).attr('targetid');
		window.location.href="sucai.php?type=delete&id="+targetid+"&pageindex="+<?php echo $pageindex;?>;
	}
});
</script>
</body>
</html>