<?php
@session_start();
require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function ConfirmDel()
{
   if(confirm("确定要删除选中的信息吗？"))
     return true;
   else
     return false;
	 
}
function postdo(va){
	document.form2.jobs.value=va
	document.form2.submit();
}

var xmlobj; 
function CreateXMLHttpRequest()
{
	if(window.XMLHttpRequest)
	{//Mozilla浏览器
		xmlobj=new XMLHttpRequest();
		if(xmlobj.overrideMimeType)
		{//设置MIME类别
		xmlobj.overrideMimeType("text/xml");
		}
	}
	else if(window.ActiveXObject)
	{ //IE浏览器
		try
		{xmlobj=new ActiveXObject("Msxml2.XMLHttp");}
		catch(e)
		{
			try
			{xmlobj=new ActiveXobject("Microsoft.XMLHttp");}
			catch(e)
			{}
		}
	}

}

function trim(str){   
    str = str.replace(/^(\s|\u00A0)+/,'');   
    for(var i=str.length-1; i>=0; i--){   
        if(/\S/.test(str.charAt(i))){   
            str = str.substring(0, i+1);   
            break;   
        }   
    }   
    return str;   
}  

</SCRIPT>
</head>
<body> 
<?php
if ($actions=="saveadd"){
	if(trim($title)==""){
		$fun->popmassage("素材标题不能为空！","","popback");
		exit();
	}
	$fid=intval($fid);
	if($fid<=0){
		  $fun->popmassage("请选择所属行业！","","popback");
		  exit();
	}
	
	$img="";
	$file=$_FILES['picurl'];
	if (!empty($file["name"])){
		$iname = date("YmdHis");
		$picType = explode(".",$file["name"]);
		$picType = $picType[1];		
		$picPath = "../../{$webdb[updir]}/sucaipic/".date("Ym")."/".$iname.".".$picType;
		$img="sucaipic/".date("Ym")."/".$iname.".".$picType;
		movefile("","","../../{$webdb[updir]}/sucaipic/".date("Ym")."/");
		$imgSize = getimagesize($file['tmp_name']);
		$width = $imgSize[0];
		$height = $imgSize[1];
		if(move_uploaded_file($file['tmp_name'], $picPath))
		{
			chmod($picPath,0755);
		}else{
		}	
		@unlink("../../$webdb[updir]/$postdb[oldpicurl]");
	}
	if($img==""){
		  $fun->popmassage("请上传素材图！","","popback");
		  exit();
	}

  	$files=array(
  	"title"             =>$title,
  	"fid"              =>$fid,
  	"picurl"           =>$img,
  	"picwidth"           =>$width,
  	"picheight"           =>$height,
  	"createid"         =>$_SESSION['mxwifi']['userid'],
  	"createdate"       =>date("Y-m-d H:i:s"),
  	);
  	$db->exe_insert("mx_sucai",$files);
	jump2("素材添加成功","sucai.php",2);
	exit();
}
elseif ($actions=="savemodi"){
	$id=intval($id);
	if ($id<=0){
		$fun->popmassage("请选择要修改的信息","","popback");
		exit();
	}
	if(trim($title)==""){
		$fun->popmassage("素材标题不能为空！","","popback");
		exit();
	}
	$fid=intval($fid);
	if($fid<=0){
		  $fun->popmassage("请选择所属行业！","","popback");
		  exit();
	}
	
	$img="";
	$file=$_FILES['picurl'];
	if (!empty($file["name"])){
		$iname = date("YmdHis");
		$picType = explode(".",$file["name"]);
		$picType = $picType[1];		
		$picPath = "../../{$webdb[updir]}/sucaipic/".date("Ym")."/".$iname.".".$picType;
		$img="sucaipic/".date("Ym")."/".$iname.".".$picType;
		movefile("","","../../{$webdb[updir]}/sucaipic/".date("Ym")."/");
		$imgSize = getimagesize($file['tmp_name']);
		$width = $imgSize[0];
		$height = $imgSize[1];
		if(move_uploaded_file($file['tmp_name'], $picPath))
		{
			chmod($picPath,0755);
		}else{
		}	
		@unlink("../../$webdb[updir]/$oldpicurl");
	}
	else{
		$img=$oldpicurl;
	}
	
	if($img==""){
		  $fun->popmassage("请上传素材图！","","popback");
		  exit();
	}

  	$files=array(
  			"title"             =>$title,
  			"fid"              =>$fid,
  			"picurl"           =>$img,
		  	"modiid"          =>$_SESSION['mxwifi']['userid'],
		  	"modidate"        =>date("Y-m-d H:i:s"),
  	);
	if($imgSize){
		$files['picwidth'] = $width;
		$files['picheight'] = $height;
	}
  	$db->exe_update("mx_sucai",$files,"id='$id'");
	jump2("素材修改成功","sucai.php?pageindex=".$pageIndex,2);
    exit();
} elseif ($actions=="delpic"){
	if(!$id){
		$fun->popmassage("请选择要删除的图片","","popback");
		exit();
	}
	$pic=$db->getsingle("select picurl from mx_sucai WHERE id='$id'");
	if ($pic!=""){@unlink("../../$webdb[updir]$pic");}
	//删除附件
	$db->excu("update mx_sucai set picurl='' WHERE id='$id'");
	jump2("素材图删除成功","edit_sucai.php?actions=modi&id=".$id,1);
	exit();
}
require("../mx_head.php");?>
<?php
  if ($actions=="add"){
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("title","素材标题"))
		return false;
	
	if (!CheckLength("fid","所属行业"))
		return false;
		
	if (!CheckLength("picurl","素材图"))
		return false;

}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">新增素材</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="edit_sucai.php" onsubmit="return CheckAdd()" enctype="multipart/form-data">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff" width="15%">素材标题：</td>
          <td bgcolor="#FFFFFF" width="35%"><input type="text" name="title" id="title" class="xtgk5" value="<?php echo trim($rsdb["title"]);?>" />&nbsp;<span class="red14">*</span></td>
          <td align="right" bgcolor="#eff7ff" width="15%">所属行业：</td>
          <td bgcolor="#FFFFFF" width="35%"><?php
          $sort_fup=$Guidedb->Select("mx_hangye_class","fid",$rsdb["fid"],"","",0,"","","","","","",'');
          echo $sort_fup;
		  ?>&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">缩 略 图：</td>
          <td  bgcolor="#FFFFFF" colspan="3"><input name="picurl" id="picurl" type="file" />&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="actions" value="saveadd" />	
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="modi"){
//修改管理员
  global $db,$fun;
  $id=trim($id);
  if ($id==0){
  	$fun->popmassage("此信息不存在！","","popback");
	exit();
  }
  $rsdb=$db->getinfo("select top 1 * from mx_sucai where id='$id'");
  if (!is_array($rsdb)){
    $fun->popmassage("此信息不存在！","","popback");
    exit();
  }
  
  $isokdb[intval($rsdb[isok])]=' checked ';
  if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("name","模板名称"))
		return false;
	
	if (!CheckLength("filename","模板路径"))
		return false;
		
	if (!CheckLength("fid","所属行业"))
		return false;
		
	if (document.form1.picurl.value==""){
		if (document.form1.oldpicurl.value==""){
			alert("图片不能为空");
			return false;
		}
	}
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改素材</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="edit_sucai.php" onsubmit="return CheckAdd()" enctype="multipart/form-data">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff" width="15%">素材标题：</td>
          <td bgcolor="#FFFFFF" width="35%"><input type="text" name="title" id="title" class="xtgk5" value="<?php echo trim($rsdb["title"]);?>" />&nbsp;<span class="red14">*</span></td>
          <td align="right" bgcolor="#eff7ff" width="15%">所属行业：</td>
          <td bgcolor="#FFFFFF" width="35%"><?php
          $sort_fup=$Guidedb->Select("mx_hangye_class","fid",$rsdb["fid"],"","",0,"","","","","","",'');
          echo $sort_fup;
		  ?>&nbsp;<span class="red14">*</span></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">素 材 图：</td>
          <td  bgcolor="#FFFFFF" colspan="3"><input name="picurl" type="file" /><input type="hidden" name="oldpicurl" value="<?php echo trim($rsdb[picurl])?>">&nbsp;<span class="red14">*</span></td>
        </tr>
        <?php if(trim($rsdb[picurl])!=""){?>
        <tr>
			<td align="right" valign="top" bgcolor="#eff7ff">预览图片：</td>
			<td bgcolor="#FFFFFF" colspan="3"><img src="<?php echo get_photourl($rsdb[picurl]); ?>" style="max-width:150px; max-height:150px;">&nbsp;<a href="edit_sucai.php?actions=delpic&id=<?php echo $rsdb["id"]?>&gotourl=<?php echo $gotourl;?>">删除图片</a></td>
        </tr>
        <?php }?>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="type" value="<?php echo $type?>">
			<input type="hidden" name="pageIndex" value="<?php echo $pageIndex?>">
			<input type="hidden" name="url" value="<?php echo $gotourl?>">
			<input type="hidden" name="id" value="<?php echo $rsdb[id]?>">
			<input type="hidden" name="actions" value="savemodi">
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
?>

<?php require("../mx_foot.php");?>
</body>
</html>
