<?php
@session_start();
require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
$fromurl = "sucai.php";
$table = "mx_sucai";
if($type == "ajax"){
	$str="";
	if ($title!=""){
		$str.=" and title like '%$title%' ";
	}
	if ($createid!=""){
		$str.=" and createid=".$createid;
	}
	if ($fid!=""){
		$str.=" and fid =".$fid;
	}
	if(empty($pageindex)){
		$pageindex=1;
	}
	$pagesize=20;
	$order=" order by createdate desc,id asc";
	$column=" * ";
	$biaoid="id";
	$list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);
	$status = 0;
	$newList = array();
	if($list){
		foreach ($list as $k => $v){
			$newList[$k]['id'] = $v['id'];
			$newList[$k]['title'] = iconv('gb2312','utf-8',$v['title']);
			$newList[$k]['picurl'] = get_photourl($v['picurl']);
			$newList[$k]['createdate'] =  iconv('gb2312','utf-8',$v['createdate']);
			if($v['fid']){
				unset($tempFidName);
				$tempFidName = $db->getsingle("select name from mx_hangye_class where fid=".$v['fid']);
				$newList[$k]['fidName'] = iconv('gb2312','utf-8',$tempFidName);
			}
			if($v['createid']){
				unset($tempCreateName);
				$tempCreateName =$db->getsingle("SELECT realname FROM mx_members WHERE uid=".$v['createid']);
				$newList[$k]['createName'] = iconv('gb2312','utf-8',$tempCreateName);
			}
		}
		$status = 1;
	}
	echo json_encode(array('status'=>$status,'data'=>$newList));
	exit();
}elseif($type == "delete"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	$mubaninfo=$db->getinfo("select top 1 * from mx_sucai where id='$id'");
	if (!is_array($mubaninfo)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	@unlink("../../$webdb[updir]$mubaninfo[picurl]");
// 	$db->excu("delete from mx_sucai where id='$id' ");
	jump2("素材删除成功",$fromurl."?pageindex=".$pageindex,1);
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>铭讯EOS网站后台管理</title>
<link rel="stylesheet" href="../images/css.css" type="text/css">
<style type="text/css">
/* 标签重定义 */
/*body{padding:0;margin:0;background:#ddd url(../images/bg.jpg) repeat;font-family:"微软雅黑";}*/
img{border:none;}
a{text-decoration:none;color:#444;}
a:hover{color:#999;}
#title{width:600px;margin:20px auto;text-align:center;}
/* 定义关键帧 */
@-webkit-keyframes shade{
	from{opacity:1;}
	15%{opacity:0.4;}
	to{opacity:1;}
}
@-moz-keyframes shade{
	from{opacity:1;}
	15%{opacity:0.4;}
	to{opacity:1;}
}
@-ms-keyframes shade{
	from{opacity:1;}
	15%{opacity:0.4;}
	to{opacity:1;}
}
@-o-keyframes shade{
	from{opacity:1;}
	15%{opacity:0.4;}
	to{opacity:1;}
}
@keyframes shade{
	from{opacity:1;}
	15%{opacity:0.4;}
	to{opacity:1;}
}
/* wrap */
#wrap{width:auto;height:auto;margin:0 auto;position:relative;}
#wrap .box{width:280px;height:auto;padding:10px;border:none;float:left;}
#wrap .box .info{width:280px;height:auto;border-radius:8px;box-shadow:0 0 11px #666;background:#f5f5f5;}
#wrap .box .info .pic{width:260px;height:auto;margin:0 auto;}
#wrap .box .info .pic:hover{
	-webkit-animation:shade 3s ease-in-out 1;
	-moz-animation:shade 3s ease-in-out 1;
	-ms-animation:shade 3s ease-in-out 1;
	-o-animation:shade 3s ease-in-out 1;
	animation:shade 3s ease-in-out 1;
}
#wrap .box .info .pic img{width:260px;border-radius:3px;padding-top:5px;}
#wrap .box .info .title{width:260px;height:24px;margin:0 auto;line-height:24px;text-align:center;color:#666;font-size:16px;font-weight:1;overflow:hidden;padding-top:10px;}
#wrap .box .info .par{width:260px;height:24px;margin:0 auto;line-height:24px;text-align:center;color:#666;font-size:14px;font-weight:2;overflow:hidden;}
#wrap .box .info .intro{width:260px;height:24px;margin:0 auto;line-height:24px;text-align:center;color:#666;font-size:14px;font-weight:2;overflow:hidden;padding:5px 0px 10px;}
/* pinpai_biaoti */
.pinpai_biaoti{font-size: 14px;font-weight: bold;margin-top: 15px;position: relative;text-align: center;}
.pinpai_page{font-size: 14px;font-weight: bold;margin-top: 25px;position: relative;text-align: center;}
</style>
<script type="text/javascript">
var data = [];
window.onload = function(){
	//运行瀑布流主函数
	PBL('wrap','box');
// 	//设置滚动加载
// 	window.onscroll = function(){
// 		window.onscroll=function(){
// 		    if(($(document).height() - $(window).height())-$(document).scrollTop() < 300){
// 				//校验数据请求
// 				if(getCheck()){
// 					layout();
// 				}
// 			} 
// 		}
// 	}
}

var tag = true;
/**
 * 获取信息后布局
 * @author Tony
 */
function layout(){
	var pageIndex = parseInt($('#pageIndex').val());
	if(pageIndex && tag){
		tag = false;
		var newPageIndex = pageIndex+1;
		var parVal = $('#parVal').val();
		if(parVal){
			parVal = "?"+parVal;			
		}
		$.post('sucai.php'+parVal,{'type':'ajax','pageindex':pageIndex+1},function(msg){
			tag = true;
			if(msg.status == 1){
				$('#pageIndex').val(newPageIndex);
				data = msg.data;
			}else{
				$('#pageIndex').val('0');
				data = [];
			}
			createHtml();
		},'json');
	}
}
/**
 * 创建模板对象
 * @author Tony
 */
function createHtml(){
	var wrap = document.getElementById('wrap');
	for(i in data){
		//创建box
		var box = document.createElement('div');
		box.className = 'box';
		wrap.appendChild(box);
		//创建info
		var info = document.createElement('div');
		info.className = 'info';
		info.setAttribute('id',data[i].id);
		box.appendChild(info);
		//创建title
		var title = document.createElement('div');
		title.className = 'title';
		title.innerHTML = data[i].title;
		info.appendChild(title);
		//创建pic
		var pic = document.createElement('div');
		pic.className = 'pic';
		info.appendChild(pic);
		//创建a
		var a = document.createElement('a');
		a.className = 'fancybox-effects-d';
		a.setAttribute('data-fancybox-group','gallery');
		a.setAttribute('href',data[i].picurl);
		a.setAttribute('title',data[i].title+'<br/>行业：'+data[i].fidName+'&nbsp;&nbsp;创建时间：'+data[i].createdate);
		pic.appendChild(a);
		//创建img
		var img = document.createElement('img');
		img.src = data[i].picurl;
		img.style.height = 'auto';
		a.appendChild(img);
		//创建par1
		var par1 = document.createElement('div');
		par1.className = 'par';
		par1.innerHTML = "创建人："+data[i].createName;
		info.appendChild(par1);
		//创建par2
		var par2 = document.createElement('div');
		par2.className = 'par';
		par2.innerHTML = data[i].createdate;
		info.appendChild(par2);
		//创建intro
		var intro = document.createElement('div');
		intro.className = 'intro';
		info.appendChild(intro);

		var span = document.createElement('span');
		span.className = 'fidName';
		span.innerHTML = "行业："+data[i].fidName;
		intro.appendChild(span);

		var span2 = document.createElement('span');
		span.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;操作：<a class='fancybox fancybox.iframe' href='edit_sucai.php?actions=modi&type=ajax&id="+data[i].id+"'>修改</a>&nbsp;<a href='javascript:void(0);' targetid='"+data[i].id+"' class='del'>删除</a>";
		intro.appendChild(span);
	}
	PBL('wrap','box');
}

/**
* 瀑布流主函数
* @param  wrap	[Str] 外层元素的ID
* @param  box 	[Str] 每一个box的类名
*/
function PBL(wrap,box){
	//	1.获得外层以及每一个box
	var wrap = document.getElementById(wrap);
	var boxs  = getClass(wrap,box);
	//	2.获得屏幕可显示的列数
	var boxW = boxs[0].offsetWidth;
	var colsNum = Math.floor(document.documentElement.clientWidth/boxW);
	wrap.style.width = boxW*colsNum+'px';//为外层赋值宽度
	//	3.循环出所有的box并按照瀑布流排列
	var everyH = [];//定义一个数组存储每一列的高度
	for (var i = 0; i < boxs.length; i++) {
		if(i<colsNum){
			everyH[i] = boxs[i].offsetHeight;
		}else{
			var minH = Math.min.apply(null,everyH);//获得最小的列的高度
			var minIndex = getIndex(minH,everyH); //获得最小列的索引
			getStyle(boxs[i],minH,boxs[minIndex].offsetLeft,i);
			everyH[minIndex] += boxs[i].offsetHeight;//更新最小列的高度
		}
	}
	wrap.style.height=getLastH()+"px";
}
/**
* 获取类元素
* @param  warp		[Obj] 外层
* @param  className	[Str] 类名
*/
function getClass(wrap,className){
	var obj = wrap.getElementsByTagName('*');
	var arr = [];
	for(var i=0;i<obj.length;i++){
		if(obj[i].className == className){
			arr.push(obj[i]);
		}
	}
	return arr;
}
/**
* 获取最小列的索引
* @param  minH	 [Num] 最小高度
* @param  everyH [Arr] 所有列高度的数组
*/
function getIndex(minH,everyH){
	for(index in everyH){
		if (everyH[index] == minH ) return index;
	}
}

/**
* 数据请求检验
*/
function getCheck(){
	var documentH = document.documentElement.clientHeight;
	var scrollH = document.documentElement.scrollTop || document.body.scrollTop;
	return documentH+scrollH>=getLastH() ?true:false;
}
/**
* 获得最后一个box所在列的高度
*/
function getLastH(){
	var wrap = document.getElementById('wrap');
	var boxs = getClass(wrap,'box');
	return boxs[boxs.length-1].offsetTop+boxs[boxs.length-1].offsetHeight;
}
/**
* 设置加载样式
* @param  box 	[obj] 设置的Box
* @param  top 	[Num] box的top值
* @param  left 	[Num] box的left值
* @param  index [Num] box的第几个
*/
var getStartNum = 0;//设置请求加载的条数的位置
function getStyle(box,top,left,index){
    if (getStartNum>=index) return;
    $(box).css({
    	'position':'absolute',
        'top':top,
        "left":left,
        "opacity":"0"
    });
    $(box).stop().animate({
        "opacity":"1"
    },999);
    getStartNum = index;//更新请求数据的条数位置
}
</script>


</head>
<body>
<?php 
require("../mx_head.php");
	$str="";
	$s="";
	if ($title!=""){
		$str.=" and title like '%$title%' ";
		$s.="title=$title&";
	}
	if ($createid!=""){
		$str.=" and createid=".$createid;
		$s.="createid=$createid&";
	}
	if ($fid!=""){
		$str.=" and fid =".$fid;
		$s.="fid=$fid&";
	}
	
	if(empty($pageindex)){
      $pageindex=1;
  	}
	$pagesize=20;
	$order=" order by createdate desc,id asc";
	$column=" * ";
	$biaoid="id";
	$c=$db->listcount($table,$str);
	$list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);
	foreach($list as $k => $v){
		if($v['fid']){
			$list[$k]['fidName'] = $db->getsingle("select name from mx_hangye_class where fid=".$v['fid']);
		}
		if($v['createid']){
			$list[$k]['createName'] =$db->getsingle("SELECT username FROM mx_members WHERE uid=".$v['createid']);
		}
	}
?>

<div class="pinpai_biaoti">
	<form action="sucai.php" method="get" name="search_form" id="search_form">
      	<span>&nbsp;标题：<input type="text" class="search_input" size="12" value="" name="title"></span>
      	<span>&nbsp;创建人：<?php
      	$query=$db->excu("select uid,username,realname from mx_members");
      	$ar=array();
      	while($re=$db->fetch_array($query)){
      		array_unshift($ar,$re);
      	}
      	echo "<select name='createid'><option value=''>选择创建人</option>";
      	foreach($ar as $k => $v){
      	?>
      	<option value="<?php echo $v['uid'];?>"><?php echo $v['username'];?></option>
      	<?php echo "</select>"; }?>
      	</span>
      	<span>&nbsp;所属行业：<?php
          $sort_fup=$Guidedb->Select("mx_hangye_class","fid","","","",0,"","","","","","",'');
          echo $sort_fup;
		  ?></span>
      	<input type="image" src="../images/search.jpg" style="margin:-5px 0 0 10px;position: absolute;" name="imageField">
      </form>
</div>
	<div id="wrap">
		<input type="hidden" value="1" id="pageIndex" />
		<input type="hidden" value="<?php echo $s;?>" id="parVal"/>
		<?php
		if($list){
		foreach($list as $k => $v){?>
		<div class="box">
			<div class="info" id="sucai_<?php echo $v['id'];?>">
				<div class="title"><?php echo $v['title']?></div>
				<div class="pic"><a class="fancybox-effects-d" data-fancybox-group="gallery" href="<?php echo get_photourl($v['picurl']);?>" title="<?php echo $v['title'];?><br/> 行业：<?php echo $v['fidName'];?>&nbsp;&nbsp;创建时间：<?php echo $v['createdate'];?>"><img src="<?php echo get_photourl($v['picurl']);?>"></a></div>
				<div class="par">创建人：<?php echo $v['createName'];?></div>
				<div class="par">创建时间：<?php echo $v['createdate'];?></div>
				<div class="intro"><span class="fidname">行业：<?php echo $v['fidName'];?></span>&nbsp;&nbsp;&nbsp;&nbsp;操作：<a href="edit_sucai.php?actions=modi&type=ajax&id=<?php echo $v['id'];?>&pageIndex=<?php echo $pageindex;?>">修改</a>&nbsp;<a href="javascript:void(0);" targetid="<?php echo $v['id'];?>" class="del" >删除</a></div>
			</div>
		</div>
		<?php }
		}else{?>
		<div style="border: medium none; height: auto; padding: 10px; width: auto;text-align:center;">
			<div style="background: none repeat scroll 0 0 #FFFFFF; border-radius: 8px; box-shadow: 0 0 11px #666666; height: auto;text-align:center; width: 100%;">
				暂无信息^_^.
			</div>
		</div>
		<?php }?>
		
	</div>
	<div class="pinpai_page"><?php
	  if ($c>$pagesize){?>
      <table border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?></div>
	
<!-- Add jQuery library -->
<script type="text/javascript" src="/fancybox/lib/jquery-1.10.1.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	 $('.fancybox').fancybox({
		  padding : 0,
		  autoScale:true,
		  width:750,
		  openEffect: 'elastic'
		 });

	/*
	 *  Different effects
	 */

	// Set custom style, close if clicked, change title type and overlay color
	$(".fancybox-effects-c").fancybox({
		wrapCSS    : 'fancybox-custom',
		closeClick : true,

		openEffect : 'none',

		helpers : {
			title : {
				type : 'inside'
			},
			overlay : {
				css : {
					'background' : 'rgba(238,238,238,0.85)'
				}
			}
		}
	});

	// Remove padding, set opening and closing animations, close if clicked and disable overlay
	$(".fancybox-effects-d").fancybox({
		padding: 0,

		openEffect : 'elastic',
		openSpeed  : 150,

		closeEffect : 'elastic',
		closeSpeed  : 150,

		closeClick : true,

		helpers : {
			css : {
				'background' : 'rgba(238,238,238,0.85)'
			}
		},

		afterLoad : function() {
			this.title = this.title;
		}
	});

});
/**
 * ajax修改后回调函数
 * @author Tony
 */
function modifSucai(data){
	$.fancybox.close(); 
	if(data.id){
		var target = $("#sucai_"+data.id);
		var title = data.title;
		var pic = data.picurl;
		var fidName = data.fidName;
		var createdate = data.createdate;
		target.find('.title').html(title);
		target.find('.pic a').attr('href',pic);
		target.find('.pic a img').attr('src',pic);
		target.find('.pic a').attr('title',title+'<br/>'+'行业：'+fidName+'&nbsp;&nbsp;创建时间：'+createdate);
		target.find('.intro .fidname').html("行业："+fidName);
	}
}

$(".box .del").on('click',function(){
	if(confirm('你确定要删除该素材吗？')){
		var targetid = $(this).attr('targetid');
		window.location.href="sucai.php?type=delete&id="+targetid+"&pageindex="+<?php echo $pageindex;?>;
	}
});
</script>
</body>
</html>