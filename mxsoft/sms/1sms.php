<?php
@session_start();
require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
</head>
<body> 
<?php
if ($actions=="pass"){

	if (trim($id)=="" and !is_array($id)){
		$fun->popmassage("请选择要需要通过的数据","","popback");
		exit();
	}
	$askTable = 'mx_shanghu_sms_ask';
	$logTable=" mx_shanghu_sms_log ";
	$sendStatus = 1;
	if(!is_array($id)){
		$id=intval($id);
		if ($id==0){
			$fun->popmassage("请选择要需要通过的数据","","popback");
			exit();
		}
		//获取短信信息
		$info=$db->getinfo("select top 1 * from ".$askTable." where id='$id'");
		if (is_array($info)){
			$smsCount = $info['sendCount'];
			$smsnum = $db->getsingle("SELECT smsnum FROM mx_shanghu WHERE id = '".$info['shanghuid']."'");
			if($smsCount > $smsnum){
				$sendStatus = 3;				
			}
			if($sendStatus == 1){
				if ($info["CellNumbers"]!=""){
					$CellNumberArray=explode(",",$info["CellNumbers"]);
					foreach ($CellNumberArray as $key =>$value){
						$pattern = "/^(13|15|16|18)\d{9}$/";
						$pass = preg_match($pattern,$value);
						if($pass){
							$smsTablefiles = array(
									"CellNumber"         =>$value,
									"SMSContent"          =>$info['SMSContent'],
									"CreateTime"      =>date("Y-m-d H:i:s"),
									"shanghuid"    =>$info['shanghuid'],
									"sendType"		=>$info['sendType'],
									"sendStatus"		=>$sendStatus,
								);
							$smsfiles=array(
									"CellNumber"         =>$value,
									"SMSContent"          =>$info['SMSContent'],
									"CreateTime"      	=>date("Y-m-d H:i:s"),
									"userid"    =>		$info['shanghuid'],
							);
							$db->exe_insert('SMS', $smsfiles);
							$db->exe_insert($logTable, $smsTablefiles);
						}
					}
				}
			}
			$db->excu("update ".$askTable." set sendStatus=".$sendStatus.",UpdateTime='".date("Y-m-d H:i:s")."' where id='$id'");
			if($sendStatus == 1){
				$files['smsnum'] = $smsnum-$info['sendCount'];
				$db->exe_update("mx_shanghu",$files," id='".$info['shanghuid']."'");
			}
		}
	}
	else{
		foreach( $id as $k=>$v){
			$info=$db->getinfo("select top 1 * from ".$askTable." where id='$v'");
			if (is_array($info)){
				$smsCount = $info['sendCount'];
				$smsnum = $db->getsingle("SELECT smsnum FROM mx_shanghu WHERE id = '".$info['shanghuid']."'");
				if($smsCount > $smsnum){
					$sendStatus = 3;
				}
				if($sendStatus == 1){
					if ($info["CellNumbers"]!=""){
						$CellNumberArray=explode(",",$info["CellNumbers"]);
						foreach ($CellNumberArray as $key =>$value){
							$pattern = "/^(13|15|16|18)\d{9}$/";
							$pass = preg_match($pattern,$value);
							if($pass){
								$smsTablefiles = array(
										"CellNumber"         =>$value,
										"SMSContent"          =>$info['SMSContent'],
										"CreateTime"      =>date("Y-m-d H:i:s"),
										"shanghuid"    =>$info['shanghuid'],
										"sendType"		=>$info['sendType'],
										"sendStatus"		=>$sendStatus,
								);
								$smsfiles=array(
										"CellNumber"         =>$value,
										"SMSContent"          =>$info['SMSContent'],
										"CreateTime"      	=>date("Y-m-d H:i:s"),
										"userid"    =>		$info['shanghuid'],
								);
								$db->exe_insert('SMS', $smsfiles);
								$db->exe_insert($logTable, $smsTablefiles);
							}
						}
					}
				}
				$db->excu("update ".$askTable." set sendStatus=".$sendStatus.",UpdateTime='".date("Y-m-d H:i:s")."' where id='$v'");
				if($sendStatus == 1){
					$files['smsnum'] = $smsnum-$info['sendCount'];
					$db->exe_update("mx_shanghu",$files," id='".$info['shanghuid']."'");
				}
			}
		}
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("短信通过成功",$url,1);
	exit();
}
elseif ($actions=="ban"){
	if (trim($id)=="" and !is_array($id)){
		$fun->popmassage("请选择要需要禁止的数据","","popback");
		exit();
	}
	$askTable = 'mx_shanghu_sms_ask';
	$logTable=" mx_shanghu_sms_log ";
	$sendStatus = 2;
	if(!is_array($id)){
		$id=intval($id);
		if ($id==0){
			$fun->popmassage("请选择要需要禁止的数据","","popback");
			exit();
		}
		//获取短信信息
		$info=$db->getinfo("select top 1 * from ".$askTable." where id='$id'");
		if (is_array($info)){
			if ($info["CellNumbers"]!=""){
				$CellNumberArray=explode(",",$info["CellNumbers"]);
				foreach ($CellNumberArray as $key =>$value){
					$smsTablefiles = array(
							"CellNumber"         =>$value,
							"SMSContent"          =>$info['SMSContent'],
							"CreateTime"      =>date("Y-m-d H:i:s"),
							"shanghuid"    =>$info['shanghuid'],
							"sendType"		=>$info['sendType'],
							"sendStatus"		=>$sendStatus,
						);
					$db->exe_insert($logTable, $smsTablefiles);
				}
			}
			$db->excu("update ".$askTable." set sendStatus=".$sendStatus.",UpdateTime='".date("Y-m-d H:i:s")."' where id='$id'");
		}
	}
	else{
		foreach( $id as $k=>$v){
			$info=$db->getinfo("select top 1 * from ".$askTable." where id='$v'");
			if (is_array($info)){
				if ($info["CellNumbers"]!=""){
					$CellNumberArray=explode(",",$info["CellNumbers"]);
					foreach ($CellNumberArray as $key =>$value){
						$smsTablefiles = array(
								"CellNumber"         =>$value,
								"SMSContent"          =>$info['SMSContent'],
								"CreateTime"      =>date("Y-m-d H:i:s"),
								"shanghuid"    =>$info['shanghuid'],
								"sendType"		=>$info['sendType'],
								"sendStatus"		=>$sendStatus,
						);
						$db->exe_insert($logTable, $smsTablefiles);
					}
				}
				$db->excu("update ".$askTable." set sendStatus=".$sendStatus.",UpdateTime='".date("Y-m-d H:i:s")."' where id='$v'");
			}
		}
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("短信禁止成功",$url,1);
	exit();
}

require("../mx_head.php");?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/264.gif" width="16" height="16" /></td>
        <td class="xtgk1">短信审核管理</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
<?php
  $str="";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=10;
  }

  $table=" mx_shanghu_sms_ask ";
  $order=" order by sendStatus asc,CreateTime desc";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);
  $shanghuTable=" mx_shanghu";
  foreach($list as $k => $v){
	if($v['shanghuid']){
		$list[$k]['srealname'] = $db->getsingle('select srealname from '.$shanghuTable.' where id='.$v['shanghuid']);
	}
  }
 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2" class="marb15">
      <tr bgcolor="#eff7ff">
        <td width="5%" align="center" bgcolor="#eff7ff">选择</td>
        <td width="15%" align="center" bgcolor="#eff7ff">时间</td>
        <td width="15%" align="center" bgcolor="#eff7ff">商户名</td>
        <td width="10%" align="center" bgcolor="#eff7ff">发送类型</td>
        <td width="10%" align="center" bgcolor="#eff7ff">短信条数</td>
        <td width="35%" align="center" bgcolor="#eff7ff">短信内容</td>
        <td width="10%" align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="sms.php">
  <?PHp
  if (!is_array($list) or $c==0){
  echo "<tr class=\"daili1\"><td colspan=8><center>暂无信息</center></td></tr>";
  }
  else{
	$SENDTYPE = array('1'=>'当前用户','2'=>'自行导入');
  for($i=0;$i<count($list);$i++){
  ?>
  <tr class="daili1" align="center"> 
    <td width="5%"><?php if($list[$i]['sendStatus'] == 0){?><input type="checkbox" name="id[<?php echo $list[$i]["id"] ?>]" id="id" value="<?php echo $list[$i]["id"] ?>" class="input_check"><?php }else{echo '&nbsp;';}?></td>
    <td><?php echo $list[$i]["CreateTime"] ?></td>
    <td><?php echo $list[$i]['srealname']?></td>
    <td><?php echo $SENDTYPE[$list[$i]['sendType']]?></td>
    <td><?php echo $list[$i]['sendCount'];?></td>
    <td><?php echo $list[$i]["SMSContent"]; ?></td>
    <td>
    <?php if($list[$i]['sendStatus'] == 1 || $list[$i]['sendStatus'] == 3){?>
    	<font color="#11B222">已通过</font>
    <?php }elseif($list[$i]['sendStatus'] == 2){?>
		<font color="red">已禁用</font>
		<a href="sms.php?actions=pass&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要审核通过此短信吗?')">通过</a>
    <?php }else{?>
		<a href="sms.php?actions=pass&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要审核通过此短信吗?')">通过</a>
		<a href="sms.php?actions=ban&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要禁止发送此短信吗?')">禁止</a>
	<?php }?>
	</td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php if ($c>0){?>
      <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a href="javascript:CheckAll(document.form1,'')">反选</a></td>
          <td width="19"><img src="../images/stop.jpg" width="14" height="14" /></td>
          <td width="80"><a href="javascript:if (confirm('确定要通过选中的设备吗？')){postdo('pass');}"/>通过选中</a></td>
          <td width="19"><img src="../images/del.jpg" width="14" height="14" /></td>
          <td width="80"><a href="javascript:if (confirm('确定要禁止选中的设备吗？')){postdo('ban');}"/>禁止选中</a></td>
          <td width="19"></td>
          <td>&nbsp;</td>
        </tr>
      </table>
	  <?php }
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>

<?php require("../mx_foot.php");?>
</body>
</html>
