<?php
@session_start();
require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<link rel="stylesheet" href="/webedito/themes/default/default.css" />

</head>
<style>
.upload_img{max-width:40px; max-height:40px; _width:40px; _height:40px;}
.upload_imgdiv{width:40px; height:65px; overflow:hidden;margin-bottom:5px; margin-right:5px; text-align:center; display:block; float:left;}
.upload_imgdiv .img_div{ width:40px; height:40px; overflow:hidden;}
.upload_imgdiv span{cursor:pointer; color:#FF0000;}
</style>
<body> 
<?php
require("../mx_head.php");?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/264.gif" width="16" height="16" /></td>
        <td class="xtgk1">短信充值记录</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<?php
  $str=" ";
  $s="";
  if ($shanghuid){
  	  $shanghuid=intval($shanghuid);
	  $str.=" and shanghuid='$shanghuid' ";
	  $s="shanghuid=$shanghuid&";
  }
  if ($dailiid){
	  $dailiid=intval($dailiid);
		$str.=" and dailiid='$dailiid' ";
	  $s="dailiid=$dailiid&";
  }
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }
  $s.="pagesize=$pagesize&";
  if($skeyword!=""){
  	$paytype=intval($paytype);
	$str.=" and shanghuid in (select id from mx_shanghu where sname like '%$skeyword%' or srealname like '%$skeyword%') ";
	$s.="skeyword=$skeyword&";
  }
  if($paytype!=""){
  	$paytype=intval($paytype);
	$str.=" and paytype='$paytype' ";
	$s.="paytype=$paytype&";
  }
  if ($mindate!=""){
  	$mindate=formatdate($mindate);
	$str.=" and paydate>='$mindate 00:00:00' ";
	$s.="mindate=$mindate&";
  }
  if ($maxdate!=""){
  	$maxdate=formatdate($maxdate);
	$str.=" and paydate<='$maxdate 23:59:59' ";
	$s.="maxdate=$maxdate&";
  }
  if ($ispay!=""){
  	$ispay=intval($ispay);
	$str.=" and ispay='$ispay' ";
	$s.="ispay=$ispay&";
  }
  $table=" mx_sms_order ";
  
  $order=" order by paydate desc,createdate desc, id desc";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="sms_order.php">所有记录</a></div>
		<div class="an2"><a href="sms_order.php?ispay=1">已支付</a></div>
		<div class="an2"><a href="sms_order.php?ispay=0">未支付</a></div>
		</td>
		<td align="right">
		总计已收：<b>
<?php 
		$gettotal = $db->getsingle("select sum(shiprice) from mx_sms_order where ispay = 1");
		echo $gettotal?$gettotal:0;
?>
		</b>元&nbsp;&nbsp;总计未收：<b>
<?php 
		$ungettotal = $db->getsingle("select sum(shiprice) from mx_sms_order where ispay = 0");
		echo $ungettotal?$ungettotal:0;
?>
		</b>元
		</td>
		</tr>
		</table>
	</div>
      <table border="0" align="left" cellpadding="3" cellspacing="0">
	<form id="search_form" name="search_form" method="get" action="sms_order.php">
        <tr>
      	<td>&nbsp;商户：</td><td><input name="skeyword" type="text" id="skeyword" value="<?php echo $skeyword;?>" size="16" class="search_input"/></td>
      	<td>&nbsp;支付日期：</td><td><input name="mindate" type="text" id="mindate" value="<?php echo $mindate;?>" size="8" class="search_input" style="width:80px;"/></td><td> 至 </td><td><input name="maxdate" type="text" id="maxdate" value="<?php echo $maxdate;?>" size="8" class="search_input" style="width:80px;"/></td>
	  <script language="javascript" type="text/javascript">
	  Calendar.setup({
	  	  inputField     :    "mindate",
	  	  ifFormat       :    "%Y-%m-%d",
	  	  showsTime      :    false,
	  	  timeFormat     :    "24"
	  });
	  Calendar.setup({
	  	  inputField     :    "maxdate",
	  	  ifFormat       :    "%Y-%m-%d",
	  	  showsTime      :    false,
	  	  timeFormat     :    "24"
	  });
	  </script>
      	<td>&nbsp;支付方式：</td>
      	<td>
		<select name="paytype" id="paytype">
			<option value="" <?php if ($paytype == ""){echo "selected";}?>>不限</option>
			<option value="1" <?php if ($paytype == 1){echo "selected";}?>>财富通</option>
			<option value="2" <?php if ($paytype == 2){echo "selected";}?>>支付宝</option>
			<option value="3" <?php if ($paytype == 3){echo "selected";}?>>微信支付</option>
		</select>
		</td>
      	<td>&nbsp;状态：</td>
      	<td><select name="ispay">
        <option value="" <?php if ($ispay == ""){echo "selected";}?>>不限</option>
        <option value="1" <?php if ($ispay == 1){echo "selected";}?>>已支付</option>
        <option value="0" <?php if ($ispay == 0){echo "selected";}?>>未支付</option>
        </select></td>

      	<td><input type="image" name="imageField" src="../images/search.jpg" /></td>
        </tr>
	 </form>
      </table>
	 </div>
<?php  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);
 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2" class="mart15 marb15">
      <tr bgcolor="#eff7ff">
        <td align="center" bgcolor="#eff7ff">商户名称</td>
        <td align="center" bgcolor="#eff7ff">代理</td>
        <td align="center" bgcolor="#eff7ff">支付时间</td>
        <td align="center" bgcolor="#eff7ff">支付金额</td>
        <td align="center" bgcolor="#eff7ff">支付方式</td>
        <td align="center" bgcolor="#eff7ff">短信数量</td>
        <td align="center" bgcolor="#eff7ff">支付状态</td>
      </tr>
  <form name="form1" id="from1" method="post" action="shanghu_save.php">
  <?PHp
  if (!is_array($list) or $c==0){
  	echo "<tr class=\"daili1\"><td colspan=8><center>暂无信息</center></td></tr>";
  }else{
  	for($i=0;$i<count($list);$i++){
		$dailiinfo = $db->getinfo("select drealname,type from mx_daili where id = ".$list[$i]['dailiid']);
	  switch ($list[$i]["paytype"])
	  {
		  case     1:     $paytype = "财富通";        break;
		  case     2:     $paytype = "支付宝";        break;
		  case     3:     $paytype = "微信支付";      break;
		  default   :     $paytype = "其他";
	  }
  ?>
  <tr class="daili1" align="center" bordercolor="#FFFFFF"> 
    <td><a href="?shanghuid=<?php echo $list[$i]["shanghuid"]; ?>"><?php echo $db->getsingle("select srealname from mx_shanghu where id  = ".$list[$i]['shanghuid']);?></a></td>
    <td><a href="?dailiid=<?php echo $list[$i]["dailiid"]; ?>">【<?php if($dailiinfo['type']==1){echo "OEM";}elseif ($dailiinfo['type']==2){echo "渠道";};?>】<?php echo $dailiinfo['drealname'];?></a></td>
    <td><?php echo $list[$i]["paydate"] ?></td>
    <td><?php echo $list[$i]["shiprice"];?></td>
    <td><?php echo $paytype;?></td>
    <td><?php echo $list[$i]["smsnum"];?></td>
    <td><?php if($list[$i]["ispay"]==1){echo "已支付";}else{echo "未支付";}?></td>
  </tr>
<?PHP
	}
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>

<?php require("../mx_foot.php");?>
</body>
</html>
