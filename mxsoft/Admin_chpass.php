<?php
@session_start();
$PurviewLevel=3;
$CheckChannelID=0;  
$PurviewLevel_Others="ModifyPwd";

require("../inc/common.inc.php");
require_once(dirname(__FILE__)."/"."Admin_ChkPurview.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="images/css.css" type="text/css">
</head>
<body>
<?php
if ($actions=="save"){
//修改密码
	if($postdb[oldpass]=="" or empty($postdb[oldpass])){
		$fun->popmassage("旧密码不能为空，请重新输入!","","popback");
		exit();
	} 
    $postdb[oldpass]=md5($postdb[oldpass]);
	$rsdb=$db->getinfo("SELECT top 1 password FROM mx_members WHERE uid='".$_SESSION['mxwifi']['userid']."' and password='$postdb[oldpass]'");
	if(!is_array($rsdb)){
		$fun->popmassage("旧密码不正确，请重新输入!","","popback");
		exit();
	}
	
	if($postdb[password]=="" or empty($postdb[password])){
		$fun->popmassage("新密码不能为空，请重新输入!","","popback");
		exit();
	} 
	
	if($postdb[password2]=="" or empty($postdb[password2])){
		$fun->popmassage("确认密码不能为空，请重新输入!","","popback");
		exit();
	} 
	
	if($postdb[password]!=$postdb[password2]){
		$fun->popmassage("两次输入密码不一致，请重新输入!","","popback");
		exit();
	} 
	$postdb[password]=md5($postdb[password]);


	$files=array(
  	"password"    =>$postdb[password]
	);

	
	$db->exe_update("mx_members",$files," uid='".$_SESSION['mxwifi']['userid']."' and password='$postdb[oldpass]'");

	$fun->popmassage("操作成功!","Admin_chpass.php","popgotourl");
	exit();
}

require("mx_head.php");?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="images/gk_1.jpg" width="4" height="39" /></td>
    <td background="images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="images/166.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改密码</td>
      </tr>
    </table></td>
    <td width="5"><img src="images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
		<form name="all" action="Admin_chpass.php" method="post" onSubmit="return dianji();" id="all">
		<tr bgcolor="#FFFFFF" class="list">
		  <td width="45%" align="right" bgcolor="#eff7ff">用&nbsp;户&nbsp;名：</td>
		  <td width="55%" align="left"><?php echo $_SESSION['mxwifi']['username'];?></td>
		</tr>
		<tr bgcolor="#FFFFFF">
		  <td align="right" bgcolor="#eff7ff">旧&nbsp;密&nbsp;码：</td>
		  <td align="left"><input type="password" name="postdb[oldpass]" id="oldpass" maxlength="30"/></td>
		</tr>
		<tr bgcolor="#FFFFFF">
		  <td align="right" bgcolor="#eff7ff">新&nbsp;密&nbsp;码：</td>
		  <td align="left"><input type="password" name="postdb[password]" id="password" maxlength="30"/></td>
		</tr>
		<tr bgcolor="#FFFFFF">
		  <td align="right" bgcolor="#eff7ff">密码确认：</td>
		  <td align="left"><input type="password" name="postdb[password2]" id="password2" maxlength="30"/></td>
		</tr>
		<tr bgcolor="#FFFFFF"><td colspan="2" align="center"><input type="image" name="imageField" src="images/dls_1.jpg" /><input type="hidden" name="actions" value="save"></td></tr>
		</form>
      </table>
    </td>
  </tr>
</table>

<script language="javascript">
 function dianji(){
  
    if(document.all.oldpass.value==""){
	  alert("请输入旧密码！");
	  document.all.oldpass.focus();
	  return false;
	}else{
	 if(document.all.oldpass.value.length<4){
	    alert("旧密码不能少于4个字符!"); 
	    document.all.oldpass.focus();
		return false;
	 }
	}
	if(document.all.password.value==""){
	  alert("请输入新密码！");
	  document.all.password.focus();
	  return false;
	}else{
	 if(document.all.password.value.length<5){
	    alert("新密码不能少于5个字符!"); 
	    document.all.password.focus();
		return false;
	 }
	}
	if(document.all.password2.value==""){
	  alert("请输入确认密码！");
	  document.all.password2.focus();
	  return false;
	}else{
	  if(document.all.password2.value==document.all.password.value){
	   
	  }else{
	     alert("两次输入的密码不同！");
	     document.all.password2.focus();
		 return false;
	  }
	}
}
//重新填写内容
function setreset(){
document.all.oldpass.value="";
document.all.password.value="";
document.all.password2.value="";	
}
</script>
<?php
require("mx_foot.php");?>
</body>
</html>
