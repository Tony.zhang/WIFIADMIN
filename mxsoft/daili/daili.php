<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=1;  
$PurviewLevel_Others="daili_";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function ConfirmDel()
{
   if(confirm("确定要删除选中的信息吗？"))
     return true;
   else
     return false;
	 
}
function postdo(va){
	document.form2.jobs.value=va
	document.form2.submit();
}

var xmlobj; 
function CreateXMLHttpRequest()
{
	if(window.XMLHttpRequest)
	{//Mozilla浏览器
		xmlobj=new XMLHttpRequest();
		if(xmlobj.overrideMimeType)
		{//设置MIME类别
		xmlobj.overrideMimeType("text/xml");
		}
	}
	else if(window.ActiveXObject)
	{ //IE浏览器
		try
		{xmlobj=new ActiveXObject("Msxml2.XMLHttp");}
		catch(e)
		{
			try
			{xmlobj=new ActiveXobject("Microsoft.XMLHttp");}
			catch(e)
			{}
		}
	}

}

function StatHandler_xucode() { //用于处理状态的函数
	if(xmlobj.readyState == 4 && xmlobj.status == 200) //如果URL成功访问，则输出网页 
	{
	    if (xmlobj.responseText=="1"){
			alert("此代理编号已经存在");
		}
	}
}

function check_daili(dname){
	if (dname!=""){
		CreateXMLHttpRequest(); 
		var showurl = 'check_dname.php?dname='+dname; 
		xmlobj.open("GET", showurl, true);
	
		xmlobj.onreadystatechange = StatHandler_xucode;
		xmlobj.send(null); 
	}
}

</SCRIPT>
</head>
<body> 
<?php
if ($actions=="saveadd"){
	$devicenum=intval($devicenum);
	$uppassword="";
	if(trim($dname)==""){
		$fun->popmassage("代理商编号不能为空！","","popback");
		exit();
	}
	$havedl=$db->getinfo("select top 1 * from mx_daili  WHERE dname='$dname'");
	if(is_array($havedl)){
		$fun->popmassage("此代理商编号已经存在！","","popback");
		exit();
	}
	if(trim($dpassword)==""){
		$fun->popmassage("登录密码不能为空！","","popback");
		exit();
	}
	if(trim($dpassword2)==""){
		  $fun->popmassage("密码确认不能为空！","","popback");
		  exit();
	}
	if ($dpassword!=$dpassword2){
		$fun->popmassage("两次输入密码不一致，请重新输入！","","popback");
		exit();
	}
	if(trim($drealname)==""){
		  $fun->popmassage("代理商名称不能为空！","","popback");
		  exit();
	}
	if(trim($mobile)==""){
		  $fun->popmassage("手机号码不能为空！","","popback");
		  exit();
	}
	if (trim($mobile)!=""){
		if(!eregi("^1[0-9]{10}$",$mobile))
		{
			  $fun->popmassage("手机号码格式不正确","","popback");
			  exit();
		}
	}

	$isok=intval($isok);
  	$files=array(
  	"dname"             =>$dname,
  	"dpassword"         =>md5($dpassword),
  	"drealname"         =>$drealname,
  	"tel"               =>$tel,
  	"mobile"            =>$mobile,
  	"qq"                =>$qq,
  	"weixin"            =>$weixin,
  	"province"          =>$province,
  	"city"              =>$city,
  	"devicenum"         =>$devicenum,
  	"mone"              =>dvHTMLEncode($mone),
  	"isok"              =>$isok,
  	"createdate"        =>date("Y-m-d H:i:s"),
  	"createid"          =>$_SESSION['mxwifi']['userid'],
  	);
  	$db->exe_insert("mx_daili",$files);
	jump2("代理商添加成功","daili.php",2);
	exit();
}
elseif ($actions=="savemodi"){
	$id=intval($id);
	$sex=intval($sex);
	if ($id==0){
		$fun->popmassage("请选择要修改的信息","","popback");
		exit();
	}
	$dpassword=trim($dpassword);
	$dpassword2=trim($dpassword2);
	if ($dpassword!="" and $dpassword2!=""){
		if ($dpassword != $dpassword2){
			$fun->popmassage("两次输入密码不一致，请重新输入","","popback");
			exit();
		}
	}
	if(trim($drealname)==""){
		  $fun->popmassage("代理商名称不能为空！","","popback");
		  exit();
	}
	if(trim($mobile)==""){
		  $fun->popmassage("手机号码不能为空！","","popback");
		  exit();
	}
	if (trim($mobile)!=""){
		if(!eregi("^1[0-9]{10}$",$mobile))
		{
			  $fun->popmassage("手机号码格式不正确","","popback");
			  exit();
		}
	}

	$isok=intval($isok);
  	$files=array(
  	"drealname"         =>$drealname,
  	"tel"               =>$tel,
  	"mobile"            =>$mobile,
  	"qq"                =>$qq,
  	"weixin"            =>$weixin,
  	"province"          =>$province,
  	"city"              =>$city,
  	"devicenum"         =>$devicenum,
  	"mone"              =>dvHTMLEncode($mone),
  	"isok"              =>$isok,
  	"createdate"        =>date("Y-m-d H:i:s"),
  	"createid"          =>$_SESSION['mxwifi']['userid'],
  	);
	if (trim($dpassword)!=""){
	$files["dpassword"]=md5($dpassword);
	}
  	$db->exe_update("mx_daili",$files,"id='$id'");

    $url=url_code($url);
	jump2("代理商修改成功","$url",2);
    exit();
}
elseif ($actions=="delete"){
	  global $db,$fun;
	  $id=intval($id);
	  if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $db->excu("delete from mx_daili where id='$id' ");
	  $url=$fromurl?$fromurl:$FROMURL;
	  jump2("代理商删除成功",$url,1);
	  exit();
}
elseif ($actions=="delall"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("delete from mx_daili where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("代理商删除成功",$url,1);
	exit();
  }
elseif ($actions=="isok"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要操作的信息","","popback");
		exit();
	}
	$upsql="update mx_daili set isok='1',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$id'";
	
	$db->excu($upsql);
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="noisok"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("请选择要操作的信息","","popback");
		exit();
	}
	$upsql="update mx_daili set isok='0',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$id'";
	$db->excu($upsql);
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="pisok"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("update mx_daili set isok='1',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}
elseif ($actions=="pnoisok"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("update mx_daili set isok='0',modidate='".date("Y-m-d H:i:s")."',modiid='".$_SESSION['mxadmin']['userid']."' where id='$key' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("操作成功",$url,1);
	exit();
}


require("../mx_head.php");?>
<?php
  if ($actions=="add"){
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("dname","代理商编号"))
		return false;
	
	
	if (!CheckLength("dpassword","登录密码"))
		return false;
	if (!CheckLength("dpassword2","密码确认"))
		return false;
	if(document.form1.dpassword.value!=document.form1.dpassword2.value){
		alert("两次输入的密码不一致！");
		document.form1.dpassword2.focus();
		return false;
	}
	if (!CheckSelect("mobile","手机号码"))
		return false;
	if (!CheckMobile("mobile"))
		return false;
	if (!CheckSelect("devicenum","初始机器数量"))
		return false;
	if (!CheckNum2("devicenum","初始机器数量"))
		return false;
		
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">新增代理商</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="daili.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商编号：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="dname" id="dname" class="xtgk5" onblur="check_daili(this.value)" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">登陆密码：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="dpassword" id="dpassword" class="xtgk5" /></td>
          <td align="right" bgcolor="#eff7ff">密码确认：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="dpassword2" id="dpassword2" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商名称（单位）</td>
          <td  bgcolor="#FFFFFF"><input type="text" name="drealname" id="drealname" class="xtgk5" /></td>
          <td  align="right" bgcolor="#EFF7FF">联系电话：</td>
          <td  bgcolor="#FFFFFF"><input type="text" name="tel" id="tel" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">手机：</td>
          <td bgcolor="#FFFFFF"><input type="text" name="mobile" id="mobile" class="xtgk5" /></td>
          <td align="right" bgcolor="#EFF7FF">QQ：</td>
          <td bgcolor="#FFFFFF"><input type="text" name="qq" id="qq" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">初始机器数量：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="devicenum" id="devicenum" class="xtgk5" /></td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">备注信息：</td>
          <td colspan="3" bgcolor="#FFFFFF">
            <textarea name="mone" id="mone" rows="10" class="xtgk6"></textarea>          </td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <input type="radio" name="isok" value="1" class="input_check" checked="checked"/>&nbsp;正常&nbsp;<input type="radio" name="isok" value="0" class="input_check"/>&nbsp;锁定
          </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="actions" value="saveadd" />	
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="modipurview"){
//修改管理员
  global $db,$fun;
  $id=trim($id);
  if ($id==0){
  	$fun->popmassage("此信息不存在！","","popback");
	exit();
  }
  $rsdb=$db->getinfo("select top 1 * from mx_daili where id='$id'");
  if (!is_array($rsdb)){
    $fun->popmassage("此信息不存在！","","popback");
    exit();
  }
  
  $isokdb[intval($rsdb[isok])]=' checked ';
  if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckLength("dname","代理商编号"))
		return false;
	
	if (document.form1.dpassword.value!="" || document.form1.dpassword2.value!=""){
		if (!CheckLength("dpassword","登录密码"))
			return false;
		if (!CheckLength("dpassword2","密码确认"))
			return false;
		if(document.form1.dpassword.value!=document.form1.dpassword2.value){
			alert("两次输入的密码不一致！");
			document.form1.dpassword2.focus();
			return false;
		}
	}
	
	if (!CheckSelect("mobile","手机号码"))
		return false;
	if (!CheckMobile("mobile"))
		return false;
	if (!CheckSelect("devicenum","初始机器数量"))
		return false;
	if (!CheckNum2("devicenum","初始机器数量"))
		return false;
		
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改代理商</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="daili.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商编号：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo $rsdb["dname"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">登陆密码：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="dpassword" id="dpassword" class="xtgk5" /></td>
          <td align="right" bgcolor="#eff7ff">密码确认：</td>
          <td bgcolor="#FFFFFF"><input type="password" name="dpassword2" id="dpassword2" class="xtgk5" /></td>
        </tr>
        <tr>
          <td colspan="4" bgcolor="#FFFFFF" align="center" style="color:#FF0000;">密码不修改请留空</td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商名称（单位）</td>
          <td  bgcolor="#FFFFFF"><input type="text" name="drealname" id="drealname" class="xtgk5" value="<?php echo trim($rsdb["drealname"]);?>" /></td>
          <td  align="right" bgcolor="#EFF7FF">联系电话：</td>
          <td  bgcolor="#FFFFFF"><input type="text" name="tel" id="tel" class="xtgk5" value="<?php echo trim($rsdb["tel"]);?>" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">手机：</td>
          <td bgcolor="#FFFFFF"><input type="text" name="mobile" id="mobile" class="xtgk5" value="<?php echo trim($rsdb["mobile"]);?>" /></td>
          <td align="right" bgcolor="#EFF7FF">QQ：</td>
          <td bgcolor="#FFFFFF"><input type="text" name="qq" id="qq" class="xtgk5" value="<?php echo trim($rsdb["qq"]);?>" /></td>
        </tr>
        <tr style="display:none;">
          <td align="right" bgcolor="#eff7ff">代理区域</td>
          <td colspan="3" bgcolor="#FFFFFF">
            <select name="select">
              <option>北京市</option>
              <option>天津市</option>
              <option selected="selected">山东省</option>
              <option>广东省</option>
              <option>新疆维吾尔自治区</option>
            </select>
            <select name="select2">
              <option>济南市</option>
              <option>章丘市</option>
            </select>
            <select name="select3">
              <option>历城区</option>
              <option>历下区</option>
            </select></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">初始机器数量：</td>
          <td colspan="3" bgcolor="#FFFFFF"><input type="text" name="devicenum" id="devicenum" class="xtgk5"  value="<?php echo trim($rsdb["devicenum"]);?>" /></td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">备注信息：</td>
          <td colspan="3" bgcolor="#FFFFFF"><textarea name="mone" id="mone" rows="10" class="xtgk6"><?php echo dvHTMLCode(trim($rsdb["mone"]));?></textarea></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <input type="radio" name="isok" value="1" class="input_check" <?php echo $isokdb[1];?>/>&nbsp;正常&nbsp;<input type="radio" name="isok" value="0" class="input_check" <?php echo $isokdb[0];?>/>&nbsp;锁定
          </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="url" value="<?php echo $gotourl?>">
			<input type="hidden" name="id" value="<?php echo $rsdb[id]?>">
			<input type="hidden" name="actions" value="savemodi">
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="view"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("此代理商信息不存在！","","popback");
		exit();
	}
	$rsdb=$db->getinfo("select top 1 * from mx_daili where id='$id'");
	if (!is_array($rsdb)){
		$fun->popmassage("此代理商信息不存在！","","popback");
		exit();
	}
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">代理商信息</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="daili.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商编号：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo $rsdb["dname"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商名称（单位）</td>
          <td  bgcolor="#FFFFFF"><?php echo trim($rsdb["drealname"]);?></td>
          <td  align="right" bgcolor="#EFF7FF">联系电话：</td>
          <td  bgcolor="#FFFFFF"><?php echo trim($rsdb["tel"]);?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">手机：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["mobile"]);?></td>
          <td align="right" bgcolor="#EFF7FF">QQ：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["qq"]);?></td>
        </tr>
        <tr style="display:none;">
          <td align="right" bgcolor="#eff7ff">代理区域</td>
          <td colspan="3" bgcolor="#FFFFFF">
            <select name="select">
              <option>北京市</option>
              <option>天津市</option>
              <option selected="selected">山东省</option>
              <option>广东省</option>
              <option>新疆维吾尔自治区</option>
            </select>
            <select name="select2">
              <option>济南市</option>
              <option>章丘市</option>
            </select>
            <select name="select3">
              <option>历城区</option>
              <option>历下区</option>
            </select></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">设备总数量：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["devicenum"]);?></td>
          <td align="right" bgcolor="#eff7ff">已使用设备：</td>
          <td bgcolor="#FFFFFF"><?php echo trim($rsdb["usenum"]);?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">未使用设备：</td>
          <td bgcolor="#FFFFFF" colspan="3"><?php echo trim($rsdb["unusenum"]);?></td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">备注信息：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo $rsdb["mone"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">代理商状态：</td>
          <td colspan="3" bgcolor="#FFFFFF">
		  <?php if ($rsdb["isok"]==1){echo "正常";}else{echo "锁定";}?>
		  </td>
		</tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
		  <input type="button" name="back" value=" 返 回 " class="n_button" onclick="history.back();">
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/365.gif" width="16" height="16" /></td>
        <td class="xtgk1">代理商列表</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="daili.php">代理商管理</a></div>
		<div class="an2"><a href="daili.php?actions=add">添加代理商</a></div>
		<div class="an2"><a href="daili.php?isok=1">正常</a></div>
		<div class="an2"><a href="daili.php?isok=0">禁用</a></div>
		</td>
		</tr>
		</table>
	</div>
      <table border="0" align="left" cellpadding="3" cellspacing="0">
	<form id="search_form" name="search_form" method="get" action="daili.php">
        <tr>
      	<td>&nbsp;编号：</td><td><input name="dname" type="text" id="dname" value="" size="10" class="search_input"/></td>
      	<td>&nbsp;姓名：</td><td><input name="drealname" type="text" id="drealname" value="" size="10" class="search_input"/></td>
      	<td>&nbsp;手机号：</td><td><input name="mobile" type="text" id="mobile" value="" size="10" class="search_input"/></td>
      	<td>&nbsp;设备数量：</td><td><input name="minnum" type="text" id="minnum" value="" size="5" class="search_input"/> — <input name="maxnum" type="text" id="maxnum" value="" size="5" class="search_input"/></td>
      	<td>&nbsp;状态：</td>
      	<td><select name="isok"><option value="">不限</option><option value="1">正常</option><option value="0">禁用</option></select></td>
      	<td><select name="pagesize"><option value="20">20条/页</option><option value="30" <?php if ($pagesize==30){echo "selected";}?>>30条/页</option><option value="50" <?php if ($pagesize==50){echo "selected";}?>>50条/页</option></select></td>
      	<td><input type="image" name="imageField" src="../images/search.jpg" /></td>
        </tr>
	 </form>
      </table>
	 </div>
<?php
  $str="";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }

  if ($dname!=""){
	$str.=" and dname like '%$dname%' ";
	$s.="dname=$dname&";
  }
  if ($drealname!=""){
	$str.=" and drealname like '%$drealname%' ";
	$s.="drealname=$drealname&";
  }
  
  if ($mobile!=""){
	$str.=" and mobile like '%$mobile%' ";
	$s.="mobile=$mobile&";
  }
  if($memberpur!=""){
  	$memberpur=intval($memberpur);
	$str.=" and Purview='$memberpur' ";
	$s.="memberpur=$memberpur&";
  }
  
  if ($minnum!=""){
  	$minnum=intval($minnum);
	$str.=" and devicenum>='%$minnum%' ";
	$s.="minnum=$minnum&";
  }
  if ($maxnum!=""){
  	$minnum=intval($minnum);
	$str.=" and devicenum<='%$maxnum%' ";
	$s.="maxnum=$maxnum&";
  }
  if ($isok!=""){
  	$isok=intval($isok);
	$str.=" and isok='$isok' ";
	$s.="isok=$isok&";
  }
  
  $table=" mx_daili ";
  $order=" order by createdate desc, id desc";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);

 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2">
      <tr bgcolor="#eff7ff">
        <td width="5%" align="center" bgcolor="#eff7ff">选择</td>
        <td width="15%" align="center" bgcolor="#eff7ff">编号</td>
        <td width="15%" align="center" bgcolor="#eff7ff">姓名（单位）</td>
        <td width="15%" align="center" bgcolor="#eff7ff">手机号码</td>
        <td width="10%" align="center" bgcolor="#eff7ff">总设备数</td>
        <td width="10%" align="center" bgcolor="#eff7ff">已使用</td>
        <td width="10%" align="center" bgcolor="#eff7ff">未使用</td>
        <td width="10%" align="center" bgcolor="#eff7ff">状态</td>
        <td width="15%" align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="daili.php">
  <?PHp
  if (!is_array($list) or $c==0){
  echo "<tr class=\"daili1\"><td colspan=9><center>暂无信息</center></td></tr>";
  }
  else{
  for($i=0;$i<count($list);$i++){
	$list[$i][isok]=$list[$i][isok]?"<A HREF='?actions=noisok&id=".$list[$i]["id"]."' style='color:red;' title='锁定用户'>正常</A>":"<A HREF='?actions=isok&id=".$list[$i]["id"]."' style='color:blue;' title='设为正常'>锁定</A>";
  ?>
  <tr class="daili1" align="center"> 
    <td width="5%"><input type="checkbox" name="id[<?php echo $list[$i]["id"] ?>]" id="id" value="<?php echo $list[$i]["id"] ?>" class="input_check"></td>
    <td><a href="daili.php?actions=view&id=<?php echo $list[$i]["id"]; ?>"><?php echo $list[$i]["dname"] ?></a></td>
    <td><a href="daili.php?actions=view&id=<?php echo $list[$i]["id"]; ?>"><?php echo $list[$i]["drealname"] ?></a></td>
    <td><?php echo $list[$i]["mobile"] ?></td>
    <td><?php echo (float)$list[$i]["devicenum"] ?></td>
    <td><?php echo (float)$list[$i]["usenum"] ?></td>
    <td><?php echo (float)$list[$i]["unusenum"] ?></td>
    <td><?php echo $list[$i]["isok"] ?></td>
    <td><a href="daili.php?actions=modipurview&id=<?php echo $list[$i]["id"] ?>">修改</a>&nbsp;&nbsp;<a href="daili.php?actions=delete&id=<?php echo $list[$i]["id"] ?>" onClick="return confirm('你确实要删除此会员吗?')">删除</a></td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php if ($c>0){?>
      <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a href="javascript:CheckAll(document.form1,'')">反选</a></td>
          <td width="19"><img src="../images/del.jpg" width="14" height="14" /></td>
          <td width="80"><a href="javascript:if (confirm('确定要删除选中的信息吗？')){postdo('delall');}"/>删除选中</a></td>
          <td width="80"><a href="javascript:postdo('pisok');"/>设为正常</a></td>
          <td><a href="javascript:postdo('pnoisok');"/>禁用用户</a></td>
        </tr>
      </table>
	  <?php }
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>
<?php 
  }

?>

<?php require("../mx_foot.php");?>
</body>
</html>
