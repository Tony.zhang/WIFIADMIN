<?php
@session_start();
$PurviewLevel=2;
$CheckChannelID=1;  
$PurviewLevel_Others="daili_";

require("../../inc/common.inc.php");
require_once("../Admin_ChkPurview.php");
//供货商数组
$ghsarr="";
$ghsarr=$db->get_gonghuoshangarr(1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<TITLE>铭讯EOS网站后台管理</TITLE>
<META http-equiv=Content-Type content="text/html; charset=gb2312">
<Meta name="Keywords" Content="济南网站建设 济南网站制作 山东网站建设 济南铭讯软件有限公司">
<Meta name="Description" Content="济南网站制作：铭讯软件专业服务优秀企业，致力于网站建、设网站推广、网络营销一体的全访位网络服务商">
<link rel="stylesheet" href="../images/css.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript">
function ConfirmDel()
{
   if(confirm("确定要删除选中的信息吗？"))
     return true;
   else
     return false;
	 
}
function postdo(va){
	document.form2.jobs.value=va
	document.form2.submit();
}

var xmlobj; 
function CreateXMLHttpRequest()
{
	if(window.XMLHttpRequest)
	{//Mozilla浏览器
		xmlobj=new XMLHttpRequest();
		if(xmlobj.overrideMimeType)
		{//设置MIME类别
		xmlobj.overrideMimeType("text/xml");
		}
	}
	else if(window.ActiveXObject)
	{ //IE浏览器
		try
		{xmlobj=new ActiveXObject("Msxml2.XMLHttp");}
		catch(e)
		{
			try
			{xmlobj=new ActiveXobject("Microsoft.XMLHttp");}
			catch(e)
			{}
		}
	}

}

function StatHandler_xucode() { //用于处理状态的函数
	if(xmlobj.readyState == 4 && xmlobj.status == 200) //如果URL成功访问，则输出网页 
	{
	    if (xmlobj.responseText=="1"){
			alert("此代理编号已经存在");
		}
	}
}

function check_daili(dname){
	if (dname!=""){
		CreateXMLHttpRequest(); 
		var showurl = 'check_dname.php?dname='+dname; 
		xmlobj.open("GET", showurl, true);
	
		xmlobj.onreadystatechange = StatHandler_xucode;
		xmlobj.send(null); 
	}
}

</SCRIPT>
</head>
<body> 
<?php
if ($actions=="savemodi"){
	$id=intval($id);
	$sex=intval($sex);
	if ($id==0){
		$fun->popmassage("请选择要修改的信息","","popback");
		exit();
	}
	$gonghuoshid=intval($gonghuoshid);
	if($gonghuoshid==0){
		  $fun->popmassage("请选择生产厂商！","","popback");
		  exit();
	}
	
	if(trim($ghsarr[$gonghuoshid])==""){
		  $fun->popmassage("请选择正确的生产厂商！","","popback");
		  exit();
	}
	$pici=trim($pici);
	if($pici==""){
		  $fun->popmassage("生产批次不能为空！","","popback");
		  exit();
	}
	$xinghao=trim($xinghao);
	if($xinghao==""){
		  $fun->popmassage("设备型号不能为空！","","popback");
		  exit();
	}

  	$files=array(
  	"gonghuoshid"     =>$gonghuoshid,
  	"pici"            =>$pici,
  	"xinghao"         =>$xinghao,
  	"mone"            =>dvHTMLEncode($mone),
  	"modidate"        =>date("Y-m-d H:i:s"),
  	"modiid"          =>$_SESSION['mxwifi']['userid'],
  	);

  	$db->exe_update("mx_shebei",$files,"id='$id' and ischuku='0'");

    $url=url_code($url);
	jump2("设备修改成功","$url",2);
    exit();
}
elseif ($actions=="delete"){
	  global $db,$fun;
	  $id=intval($id);
	  if ($id==0){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	  }
	  $db->excu("delete from mx_shebei where id='$id' and ischuku='0'");
	  $url=$fromurl?$fromurl:$FROMURL;
	  jump2("设备删除成功",$url,1);
	  exit();
}
elseif ($actions=="delall"){
	if ($id=="" || empty($id)){
		$fun->popmassage("请选择要删除的信息","","popback");
		exit();
	}
	if(!is_array($id)){
		$fun->popmassage("请选择要更改的信息","","popback");
		exit();
	}
	foreach( $id as $key=>$value){
		$db->excu("delete from mx_shebei where id='$key'and ischuku='0' ");
	}
	$url=$fromurl?$fromurl:$FROMURL;
	jump2("设备删除成功",$url,1);
	exit();
  }


require("../mx_head.php");?>
<?php
if ($actions=="saveadd"){
	$gonghuoshid=intval($gonghuoshid);
	if($gonghuoshid==0){
		$fun->popmassage("请选择供应商！","","popback");
		exit();
	}
	
	if(trim($ghsarr[$gonghuoshid])==""){
		$fun->popmassage("请正确选择供应商！","","popback");
		exit();
	}

	if(trim($pici)==""){
		$fun->popmassage("请填写生产批次！","","popback");
		exit();
	}
	if(trim($xinghao)==""){
		$fun->popmassage("请填写设备型号！","","popback");
		exit();
	}
	
	if(trim($macfile)==""){
		  $fun->popmassage("请先上传MAC列表！","","popback");
		  exit();
	}
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/267.gif" width="16" height="16" /></td>
        <td class="xtgk1">导入设备</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6" id="loadtable">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<link rel="stylesheet" href="m.css" type="text/css">
	<div style="width:100%; text-align:center;"><br><br><br>
	<FONT style="font-size:20px;font-weight:bold;color:red"><b>数据导入中，请稍后……</FONT>
	<br><br> 
	<marquee direction="right" scrollamount="8" scrolldelay="1" style="margin:auto;">
	<span class="progressBarHandle-0"></span>
	<span class="progressBarHandle-1"></span>
	<span class="progressBarHandle-2"></span>
	<span class="progressBarHandle-3"></span>
	<span class="progressBarHandle-4"></span>
	<span class="progressBarHandle-5"></span>
	<span class="progressBarHandle-6"></span>
	<span class="progressBarHandle-7"></span>
	<span class="progressBarHandle-8"></span>
	<span class="progressBarHandle-9"></span>
	</marquee>
	</div>
    </td>
  </tr>
</table>
<?php

	
	echo '<script>document.getElementById("loadtable").style.display="block";document.getElementById("showbody").style.display="none";</script>';flush();
	$handle = fopen("../..{$webdb[updir]}/$macfile","r");
	$i=0;
	$ishavemac="";
	while ($data = fgets($handle, 100000)) {
	$i++;
    if ($i==1){continue;}
	$data=explode(",",$data);
	$data[0]=strtolower($data[0]);
	
	$n = gmp_init($data[0],16); //16进制输入
	$n = gmp_add($n,1); //加1
	$thismac=gmp_strval($n,16);
	
	$ishave="";
	$ishave=$db->getinfo("select top 1 rmac from mx_shebei where rmac='$thismac'");
	if (!is_array($ishave)){
		$db->excu("BEGIN TRANSACTION DEPS02_DEL");
		$fields=array(
			"pici"            =>$pici,
			"xinghao"         =>$xinghao,
			"gonghuoshid"     =>$gonghuoshid,
			"rmac"            =>$thismac,
			"iscandoing"      =>1,
			"rukudate"        =>date("Y-m-d H:i:s"),
			"ischuku"         =>0,
			"createdate"      =>date("Y-m-d H:i:s"),
			"createid"        =>$_SESSION['mxwifi']['userid'],
		);
		$result=$db->exe_insert("mx_shebei",$fields);
		$db->excu("COMMIT TRANSACTION DEPS02_DEL");
	}
	else{
		$ishavemac[]=$data[0];
	}

	if (($i % 1000) ==0){sleep(5);}
	}
	echo $ishavemac;exit();
	@fclose("../..{$webdb[updir]}/$macfile");
	@unlink("../..{$webdb[updir]}/$macfile");
	flush();
	echo '<script>document.getElementById("loadtable").style.display="none";</script>';
	if ($ishavemac==""){
		jump2("设备导入成功","shebei.php",1);
	}
	else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6" id="loadtable">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<table width="95%" cellpadding="0" cellspacing="0" align="center">
	<tr><td>
	<p style="font-size:14px; color:#FF0000;">注：以下MAC已经存在系统中，未再次导入</p>
	<p>
	<?php
	if (!is_array($ishavemac)){echo $ishavemac;}
	else{
		foreach ($ishavemac as $key =>$value){
			if ($key>0){
				echo "、";
			}
			echo $value;
		}
	}
	?>
	</p>
	</td></tr>
	<tr><td height="40" align="center"><a href="shebei.php" class="link_but1">确定</a></td></tr>
	</table>
    </td>
  </tr>
</table>
<?php
	}	
	exit();
}
  elseif ($actions=="add"){
$n = gmp_init('18c8e720374e',16); //16进制输入
$n = gmp_add($n,1); //加1
echo gmp_strval($n,16).'<br/>'; //16进制显示
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckSelect("gonghuoshid","供货商"))
		return false;
	if (!CheckLength("pici","生产批次"))
		return false;
	if (!CheckLength("xinghao","设备型号"))
		return false;
	if (!CheckLength("macfile","MAC列表"))
		return false;
		
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/267.gif" width="16" height="16" /></td>
        <td class="xtgk1">导入设备</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="shebei.php" onsubmit="return CheckAdd()" enctype="multipart/form-data">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td align="right" width="15%" bgcolor="#eff7ff">供 货 商：</td>
          <td width="35%"  bgcolor="#FFFFFF"><select name="gonghuoshid" id="gonghuoshid"><option value="">请选择</option><?php echo show_select($ghsarr,$rsdb["gonghuoshid"]);?></select></td>
          <td align="right" width="15%" bgcolor="#EFF7FF">生产批次：</td>
          <td  width="35%" bgcolor="#FFFFFF"><input type="text" name="pici" id="pici" class="xtgk5" value="<?php echo trim($rsdb["pici"]);?>" /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">设备型号：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="xinghao" id="xinghao" class="xtgk"  /></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">MAC列表：</td>
          <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="macfile" id="macfile"  />&nbsp;<a href="javascript:" onClick="javascript:show_uplode('upfile','macfile','macfile')"><font color="#FF0000">点击上传文件</font></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="mac列表.csv" target="_self" style="color:#0066FF;">点击下载模板</a></td>
        </tr>
        <tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="actions" value="saveadd" />	
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="modipurview"){
//修改管理员
  global $db,$fun;
  $id=trim($id);
  if ($id==0){
  	$fun->popmassage("此信息不存在！","","popback");
	exit();
  }
  $rsdb=$db->getinfo("select top 1 * from mx_shebei where id='$id' and ischuku='0'");
  if (!is_array($rsdb)){
    $fun->popmassage("此信息不存在！","","popback");
    exit();
  }
  
  $isokdb[intval($rsdb[isok])]=' checked ';
  if ($gotourl==""){$gotourl=url_encode($_SERVER['HTTP_REFERER']);}
  
?>
<script language="javascript">
setFormName("form1");
function CheckAdd(){
	if (!CheckSelect("gonghuoshid","供货商"))
		return false;
	
	if (document.form1.dpassword.value!="" || document.form1.dpassword2.value!=""){
	if (!CheckLength("pici","生产批次"))
		return false;
		
}
</script>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/267.gif" width="16" height="16" /></td>
        <td class="xtgk1">修改设备信息</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="shebei.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td width="15%" align="right" bgcolor="#eff7ff">设备MAC：</td>
          <td width="35%" bgcolor="#FFFFFF"><?php echo $rsdb["rmac"];?></td>
          <td width="15%" align="right" bgcolor="#eff7ff">入库时间：</td>
          <td width="35%" bgcolor="#FFFFFF"><?php echo $rsdb["rukudate"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">供 货 商：</td>
          <td  bgcolor="#FFFFFF"><select name="gonghuoshid" id="gonghuoshid"><option value="">请选择</option><?php echo show_select($ghsarr,$rsdb["gonghuoshid"]);?></select></td>
          <td  align="right" bgcolor="#EFF7FF">生产批次：</td>
          <td  bgcolor="#FFFFFF"><input type="text" name="pici" id="pici" class="xtgk5" value="<?php echo trim($rsdb["pici"]);?>" /></td>
        </tr>
        <tr>
          <td  align="right" bgcolor="#EFF7FF">设备型号：</td>
          <td  bgcolor="#FFFFFF" colspan="3"><input type="text" name="xinghao" id="xinghao" class="xtgk" value="<?php echo trim($rsdb["xinghao"]);?>" /></td>
        </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">备注信息：</td>
          <td colspan="3" bgcolor="#FFFFFF"><textarea name="mone" id="mone" rows="10" class="xtgk6"><?php echo dvHTMLCode(trim($rsdb["mone"]));?></textarea></td>
        </tr>
          <td colspan="4" align="center" bgcolor="#FFFFFF">
            <input type="image" name="imageField" src="../images/dls_1.jpg" />
			<input type="hidden" name="url" value="<?php echo $gotourl?>">
			<input type="hidden" name="id" value="<?php echo $rsdb[id]?>">
			<input type="hidden" name="actions" value="savemodi">
		</td>
          </tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  elseif ($actions=="view"){
	$id=intval($id);
	if ($id==0){
		$fun->popmassage("此设备信息不存在！","","popback");
		exit();
	}
	$rsdb=$db->getinfo("select top 1 * from mx_shebei where id='$id' and ischuku='0'");
	if (!is_array($rsdb)){
		$fun->popmassage("此设备信息不存在！","","popback");
		exit();
	}
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/267.gif" width="16" height="16" /></td>
        <td class="xtgk1">设备信息</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF"><form id="form1" name="form1" method="post" action="shebei.php" onsubmit="return CheckAdd()">
      <table width="95%" border="0" align="center" cellpadding="10" cellspacing="1" bgcolor="#d8e8f2" class="marb15 mart15">
        <tr>
          <td width="15%" align="right" bgcolor="#eff7ff">设备MAC：</td>
          <td width="35%" bgcolor="#FFFFFF"><?php echo $rsdb["rmac"];?></td>
          <td width="15%" align="right" bgcolor="#eff7ff">入库时间：</td>
          <td width="35%" bgcolor="#FFFFFF"><?php echo $rsdb["rukudate"];?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">供 货 商：</td>
          <td  bgcolor="#FFFFFF"><?php echo $ghsarr[$rsdb["gonghuoshid"]];?></td>
          <td  align="right" bgcolor="#EFF7FF">生产批次：</td>
          <td  bgcolor="#FFFFFF"><?php echo trim($rsdb["pici"]);?></td>
        </tr>
        <tr>
          <td align="right" bgcolor="#eff7ff">设备型号：</td>
          <td  bgcolor="#FFFFFF" colspan="3"><?php echo $rsdb["xinghao"];?></td>
         </tr>
        <tr>
          <td align="right" valign="top" bgcolor="#eff7ff">备注信息：</td>
          <td colspan="3" bgcolor="#FFFFFF"><?php echo trim($rsdb["mone"]);?></td>
        </tr>
        <tr><td colspan="4" align="center" bgcolor="#FFFFFF"><input type="button" name="back" value=" 返 回 " class="button" onclick="history.back();"></td></tr>
      </table>
        </form>
    </td>
  </tr>
</table>
<?php
  }
  else{
?>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="mart15">
  <tr>
    <td width="4"><img src="../images/gk_1.jpg" width="4" height="39" /></td>
    <td background="../images/gk_2.jpg"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="23" class="xtgk2"><img src="../images/267.gif" width="16" height="16" /></td>
        <td class="xtgk1">库存设备列表</td>
      </tr>
    </table></td>
    <td width="5"><img src="../images/gk_3.jpg" width="5" height="39" /></td>
  </tr>
</table>
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c9e5f6">
  <tr>
    <td valign="top" bgcolor="#FFFFFF">
	<div class="main_topdiv">
	<div class="t">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
		<div class="an1"><a href="shebei.php">设备管理</a></div>
		<div class="an2"><a href="shebei.php?actions=add">导入设备</a></div>
		</td>
		</tr>
		</table>
	</div>
      <table border="0" align="left" cellpadding="3" cellspacing="0">
	<form id="search_form" name="search_form" method="get" action="shebei.php">
        <tr>
      	<td>&nbsp;MAC：</td><td><input name="rmac" type="text" id="rmac" value="" size="6" class="search_input"/></td>
      	<td>&nbsp;批次：</td><td><input name="pici" type="text" id="pici" value="" size="6" class="search_input"/></td>
      	<td>&nbsp;生产商：</td><td><select name="gonghuoshang" id="gonghuoshang"><option value="">不限</option><?php echo show_select($ghsarr);?></select></td>
      	<td>&nbsp;入库人：</td><td><input name="rukuren" type="text" id="rukuren" value="" size="6" class="search_input"/></td>
      	<td>&nbsp;入库时间：</td><td>
		<input name="mindate" type="text" id="mindate" value="" size="12" class="search_input"/> — <input name="maxdate" type="text" id="maxdate" value="" size="12" class="search_input"/>
	  <script language="javascript" type="text/javascript">
	  Calendar.setup({
	  	  inputField     :    "mindate",
	  	  ifFormat       :    "%Y-%m-%d",
	  	  showsTime      :    false,
	  	  timeFormat     :    "24"
	  });
	  Calendar.setup({
	  	  inputField     :    "maxdate",
	  	  ifFormat       :    "%Y-%m-%d",
	  	  showsTime      :    false,
	  	  timeFormat     :    "24"
	  });
	  </script>
		</td>
      	<td><input type="image" name="imageField" src="../images/search.jpg" /></td>
        </tr>
	 </form>
      </table>
	 </div>
<?php
  $str="and ischuku='0'";
  $s="";
  if(empty($pageindex)){
      $pageindex=1;
  }
  $pagesize=intval($pagesize);
  if ($pagesize==0){
  	$pagesize=20;
  }

  if ($rmac!=""){
	$str.=" and rmac like '%$rmac%' ";
	$s.="rmac=$rmac&";
  }
  if ($pici!=""){
	$str.=" and pici like '%$pici%' ";
	$s.="pici=$pici&";
  }
  
  if ($gonghuoshang!=""){
  	$gonghuoshang=intval($gonghuoshang);
	$str.=" and gonghuoshid='$gonghuoshang' ";
	$s.="gonghuoshang=$gonghuoshang&";
  }
  if($memberpur!=""){
  	$memberpur=intval($memberpur);
	$str.=" and Purview='$memberpur' ";
	$s.="memberpur=$memberpur&";
  }
  
  if ($mindate!=""){
  	$mindate=formatdate($mindate);
	$str.=" and createdate>='$mindate 00:00:00' ";
	$s.="mindate=$mindate&";
  }
  if ($maxdate!=""){
  	$maxdate=formatdate($maxdate);
	$str.=" and createdate<='$maxdate 23:59:59' ";
	$s.="maxdate=$maxdate&";
  }
  if ($rukuren!=""){
	$str.=" and createid in (select uid from mx_members where username like '%rukuren%' or realname like '%$rukuren%') ";
	$s.="rukuren=$rukuren&";
  }
  
  $table=" mx_shebei ";
  $order=" order by createdate desc, id desc";
  $column=" * ";
  $biaoid="id";
  $c=$db->listcount($table,$str);
  
  $list=$db->list1($pageindex,$pagesize,$table,$column,$str,$order,$biaoid);

 ?>
    <table id="mytable" width="95%" border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#d8e8f2" class="marb15">
      <tr bgcolor="#eff7ff">
        <td width="5%" align="center" bgcolor="#eff7ff">选择</td>
        <td width="10%" align="center" bgcolor="#eff7ff">MAC</td>
        <td width="10%" align="center" bgcolor="#eff7ff">批次</td>
        <td width="10%" align="center" bgcolor="#eff7ff">型号</td>
        <td width="15%" align="center" bgcolor="#eff7ff">供货商</td>
        <td width="15%" align="center" bgcolor="#eff7ff">备注</td>
        <td width="10%" align="center" bgcolor="#eff7ff">入库时间</td>
        <td width="10%" align="center" bgcolor="#eff7ff">入库人</td>
        <td width="10%" align="center" bgcolor="#eff7ff">操作</td>
      </tr>
  <form name="form1" id="from1" method="post" action="shebei.php">
  <?PHp
  if (!is_array($list) or $c==0){
  echo "<tr class=\"daili1\"><td colspan=9><center>暂无信息</center></td></tr>";
  }
  else{
  for($i=0;$i<count($list);$i++){
  ?>
  <tr class="daili1" align="center"> 
    <td width="5%"><input type="checkbox" name="id[<?php echo $list[$i]["id"] ?>]" id="id" value="<?php echo $list[$i]["id"] ?>" class="input_check"></td>
    <td><a href="shebei.php?actions=view&id=<?php echo $list[$i]["id"]; ?>"><?php echo $list[$i]["rmac"] ?></a></td>
    <td><?php echo $list[$i]["pici"] ?></td>
    <td><?php echo $list[$i]["xinghao"] ?></td>
    <td><?php echo $ghsarr[$list[$i]["gonghuoshid"]] ?></td>
    <td><?php echo $list[$i]["mone"]?></td>
    <td><?php echo formatdate($list[$i]["rukudate"]) ?></td>
    <td><?php
	if (!is_array($memberarr[$list[$i]["createid"]])){
		$memberarr[$list[$i]["createid"]]=$db->get_members($list[$i]["createid"]);
	}
	$thismemberarr=$memberarr[$list[$i]["createid"]];
	echo $thismemberarr["realname"];
	?></td>
    <td><a href="shebei.php?actions=modipurview&id=<?php echo $list[$i]["id"] ?>">修改</a><!--&nbsp;&nbsp;<a href="shebei.php?actions=delete&id=<?php //echo $list[$i]["id"] ?>" onClick="return confirm('你确实要删除此会员吗?')">删除</a>--></td>
  </tr>
  <?PHP
  }
  }
  ?>
  <input type="hidden" name="actions" value="" />
  </form>
    </table>
	  <?php if ($c>0){?>
      <table width="95%" height="46" border="0" align="center" cellpadding="0" cellspacing="0" >
        <tr>
          <td width="90"><a href="javascript:CheckAll(document.form1,'all')">全选</a>&nbsp;&nbsp;<a href="javascript:CheckAll(document.form1,'')">反选</a></td>
          <td width="19"><img src="../images/del.jpg" width="14" height="14" /></td>
          <td><a href="javascript:if (confirm('确定要删除选中的信息吗？')){postdo('delall');}"/>删除选中</a></td>
        </tr>
      </table>
	  <?php }
	  if ($c>$pagesize){?>
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="marb15">
        <tr>
          <td>
			<?PHP
			echo $db->ThePage_mxsoft($s,$pageindex,$c,$pagesize)
			?>
		  </td>
        </tr>
      </table>
	  <?php }?>
	  </td>
  </tr>
</table>
<script language="javascript">
window.onload=function showtable(){
var tablename=document.getElementById("mytable");
var li=tablename.getElementsByTagName("tr");
for (var i=0;i<=li.length;i++){
li[i].style.backgroundColor="#fff";
li[i].onmouseover=function(){
this.style.backgroundColor="#e8f4ff";
}
li[i].onmouseout=function(){
this.style.backgroundColor="#fff"
}
}
}


function postdo(va){
	document.form1.actions.value=va;
	document.form1.submit();
}

</script>
<?php 
  }

?>

<?php require("../mx_foot.php");?>
</body>
</html>
