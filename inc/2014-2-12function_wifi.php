<?php
/*
  * @author 小白 <chuanwei1234@163.com>  
  * @QQ 86844077  
  * @date 2014-01-13
*/
function trim_array($arr=""){
	if (is_array($arr)){
		foreach ($arr as $key =>$value){
			$arr[$key]=trim($value);
		}
	}
	return $arr;
}

//获取商户信息
function get_shanghuinfo_isok($uname="",$shanghuid=0,$sql=""){
	global $db;
	$shanghuid=intval($shanghuid);
	if ($shanghuid>0){
		$shanginfo=$db->getinfo("select top 1 * from mx_view_shanghu where id='$shanghuid' $sql");
	}
	elseif (trim($uname)!=""){
		$shanginfo=$db->getinfo("select top 1 * from mx_view_shanghu where sname='$uname' $sql");
	}
	return $shanginfo;
}
//获取设备信息(在线)
function get_shebeiinfo_online($Rid=0,$sql=""){
	global $db;
	$Rid=intval($Rid);
	if ($Rid>0){
		$shebei=$db->getinfo("select top 1 * from mx_view_onlineshenbei where id='$Rid' {$sql}");
	}
	return $shebei;
}
//获取设备信息(已使用)
function get_shebeiinfo_use($Rid=0,$sql=""){
	global $db;
	$Rid=intval($Rid);
	if ($Rid>0){
		$shebei=$db->getinfo("select top 1 * from mx_view_useshebei where id='$Rid' {$sql}");
	}else{
		$shebei=$db->getinfo("select top 1 * from mx_view_useshebei where 1=1 {$sql}");
	}
	return $shebei;
}


//设备认证
function wifi_getRconfig($rmac="",$uname="",$sign=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:s");
	$upsql="";
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	if ($rmac=="" or $uname=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$rmac{mx}$uname{wei}")){
		return $result;
	}
	//获取商户
	$shanginfo=$db->getinfo("select top 1 * from mx_view_shanghu where sname='$uname'");
	if (!is_array($shanginfo)){
		return $result;
	}
	//获取设备是否激活
	$shebei=get_shebeiinfo_use(0," and shanghuid='$shanginfo[id]' and rmac='$rmac' ");
	if (!is_array($shebei)){
		return $result;
	}
	//判断是否在有效期内
	if (formatdate($shebei["startdate"])>date("Y-m-d") or formatdate($shebei["enddate"])<date("Y-m-d") or formatdate($shanginfo["startdate"])>date("Y-m-d") or formatdate($shanginfo["enddate"])<date("Y-m-d") or $shanginfo["isok"]==0){
		$shebei["iscandoing"]=0;
	}
	
	if ($shebei["isok"]==0){
		$upsql.=",isok='1',jihuodate='$hostthistime'";
	}
	$upsql.=",rennum=rennum+1 ";
	$rconfig=$db->getinfo("select top 1 * from mx_rconfig ");
	if(trim($shanginfo["reurl"])==""){
		$shanginfo["reurl"]="http://wifi.mxsoft.cn/tongyong/index.php";
	}
	if(trim($shanginfo["gotourl"])==""){
		$shanginfo["gotourl"]="http://weixin.mxsoft.cn/article.php?id=45";
	}
	$shanginfo["gotourl"]=url_encode($shanginfo["gotourl"]);
	if (strpos($shanginfo["reurl"],"?")){
		//$shanginfo["reurl"].="&gotourl=$shanginfo[gotourl]&sid=$shanginfo[id]&sname=".urlencode($shanginfo["srealname"]);
	}
	else{
		//$shanginfo["reurl"].="?gotourl=$shanginfo[gotourl]&sid=$shanginfo[id]&sname=".urlencode($shanginfo["srealname"]);
	}
	//$shebei["ssid"]=iconv("UTF-8","GB2312",$shebei["ssid"]);
	$resultarr=array("res"=>"1","rid"=>$shebei["id"],"ssid"=>$shebei["ssid"],"iscandoing"=>$shebei["iscandoing"],"reurl"=>$shanginfo["reurl"],"whitelist"=>$shebei["whitelist"],"blacklist"=>$shebei["blacklist"],"tshebeirestart"=>$rconfig["tshebeirestart"],"twifire"=>$rconfig["twifire"],"twifiuser"=>$rconfig["twifiuser"],"twhitelist"=>$rconfig["twhitelist"],"tblacklist"=>$rconfig["tblacklist"],"trestart"=>$rconfig["trestart"],"tupgrade"=>$rconfig["tupgrade"],"tchange"=>$rconfig["tchange"],"treload"=>$rconfig["treload"],"tfree"=>$rconfig["tfree"],"wanopen"=>1);
	$resultarr=trim_array($resultarr);
	
	if ($shebei["iscandoing"]==1){
		$db->excu("BEGIN TRANSACTION DEPS02_DEL");
		//更新设备状态
		wifi_r_updatedoing($shebei["id"],$upsql,$shebei);
		
		$db->excu("update mx_shenbei_log set islast='0' where rid='$shebei[id]'");
		//创建设备认证记录
		$db->excu("insert into mx_shenbei_log(shanghuid,rid,bw_up,bw_down,bw_up_last,bw_down_last,zongtime,lastdotime,createdate,islast)values('$shebei[shanghuid]','$shebei[id]','0','0','0','0','0','$hostthistime','$hostthistime','1')");
		if ($shebei["isok"]==0){
			//更新代理商设备数量
			$db->excu("update mx_daili set usenum=usenum+1,unusenum=devicenum-usenum-1 where id='$shebei[dailiid]'");
			//更新商户设备数量
			$db->excu("update mx_shanghu set usenum=usenum+1,unusenum=devicenum-usenum-1 where id='$shebei[shanghuid]'");
		}
		//事务提交
		$db->excu("COMMIT TRANSACTION DEPS02_DEL");
	}
	$result=json_encode($resultarr);
	//$result=str_replace($result,"\/","/");
	return $result;
}

//更新设备状态
function wifi_r_updatedoing($rid=0,$upsql="",$shebei=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:00");
	$rid=intval($rid);
	if ($rid>0){
		if (!is_array($shebei)){
			$shebei=get_shebeiinfo_online($Rid," startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."' and iscandoing='1'");
			if (!is_array($shebei)){
				return $result;
			}
		}

		$shebeicha=get_time_miao($shebei["lastsetdoingtime"],$hostthistime);
		$shebeicha=intval($shangcha/60);
		$upsql.=",doingtime=doingtime+$shebeicha,lastsetdoingtime='$hostthistime'";
		$db->excu("update mx_shebei set lastdotime='$hostthistime',isonlien='1' {$upsql} where id='$rid' and iscandoing='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	}
}


//Wifi用户首次接入接口
function wifi_setwifiuserre($Rid="",$uname="",$mac="",$sign=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:s");
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $mac=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=$db->getinfo("select top 1 * from mx_view_shanghu where sname='$uname' and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	
	//获取设备信息
	$shebei=get_shebeiinfo_online($Rid," and shanghuid='$shanginfo[id]' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."' and iscandoing='1'");
	if (!is_array($shebei)){
		return $result;
	}
	//判断是否在白名单或者黑名单中
	if(strpos("##,".$shanginfo["whitelist"].",",",$mac,")){$iswhite=1;}
	if(strpos("##,".$shanginfo["blacklist"].",",",$mac,")){$isbalck=1;}
	
	if ($isbalck==1){
		$resultarr=array("res"=>"1");
		$result=json_encode($resultarr);
		return $result;
	}
	//判断商家mac是否存在
	$shangmac=$db->getinfo("select * from mx_shang_maclist where mac='$mac' and shanghuid='$shanginfo[id]'");
	$rconfig=$db->getinfo("select top 1 * from mx_rconfig ");
	$canusetime=$rconfig["treload"]*60;
	$db->excu("BEGIN TRANSACTION DEPS02_DEL");
	if (is_array($shangmac)){
		if ($shangmac["isrenzheng"]==1 or $iswhite==1){
			if ($shangmac["isonline"]==1){
				//已经在线的用户
				$db->excu("update mx_mac_log set rid='$Rid' where islast='1' and macid='$shangmac[macid]' and shanghuid='$shanginfo[id]'");
				$resultarr=array("res"=>"2","bw_up"=>intval($shanginfo["bw_up"]),"bw_down"=>intval($shanginfo["bw_up"]),"canusetime"=>"$canusetime");
			}
			else{
				//不在线
				$shangmacsql="update mx_shang_maclist set ispop='0'  where macid='$shangmac[macid]' and shanghuid='$shanginfo[id]'";
				$db->excu($shangmacsql);
				//操作原有记录
				$macloginfo=$db->getinfo("select * from mx_view_maclog_last where macid='$shangmac[id]' and shanghuid='$shangmac[shanghuid]'");
				if (is_array($macloginfo)){
					$db->excu("update mx_shang_maclist set bw_up=bw_up+$macloginfo[bw_up],bw_down=bw_down+$macloginfo[bw_down],zongrenum=zongrenum+1,isonline=1,ispop='0' where macid='$shangmac[macid]' and shanghuid='$shanginfo[id]'");
					$db->excu("update mx_mac_log set islast='0' where macid='$shangmac[macid]' and shanghuid='$shanginfo[id]' and islast='1'");
				}
				$maclogsql="insert into mx_mac_log (macid,shanghuid,rid,bw_up,bw_down,lastdotime,lastsetdoingtime,mactype,createdate,islast)values('$shangmac[macid]','$shanginfo[id]','$Rid','0','0','$hostthistime','$hostthistime','$mactype','$hostthistime','1')";
				//是否漫游
				$thismanyou=0;
				if ($shangmac["mactype"]==1){$thismanyou=1;}
				$db->excu("update mx_maclist set lastrid='$Rid',lasttime='$hostthistime',lastdotime='$hostthistime',lastsetdoingtime='$hostthistime',isonline='1',manyou=manyou+{$thismanyou} where id='{$shangmac[macid]}'");
				//$resultarr=array("res"=>"2","bw_up"=>intval($shanginfo["bw_up"]),"bw_down"=>intval($shanginfo["bw_up"]),"canusetime"=>"$canusetime");
				$resultarr=array("res"=>"1");
			}
		}else{
			$resultarr=array("res"=>"1");
		}
	}
	else{
		//判读是否已经存在此用户
		$macinfo=$db->getinfo("select * from mx_maclist where mac='$mac'");
		if (!is_array($macinfo)){
			$biaoshi=$mac;
			$db->excu("insert into mx_maclist(biaoshi,mac,firstrid,firsttime,lastrid,lasttime,lastsetdoingtime,createdate)values('$biaoshi','$mac','$Rid','$hostthistime','$Rid','$hostthistime','$hostthistime','$hostthistime')");
			$macinfo["firstrid"]=$shanginfo["id"];
	//		$macid=$db->getsingle("SELECT @@IDENTITY AS 'Identity'");
	//		$shangmacsql="insert into mx_shang_maclist(macid,shanghuid,biaoshi,mac,firsttime,lasttime,lastdotime,mactype,createdate)vlaues('$macid','$shanginfo[id]','$biaoshi','$mac','$hostthistime','$hostthistime','$hostthistime','0','$hostthistime')";
			if ($iswhite!=1){
				$resultarr=array("res"=>"1");
			}
		}
	
		if ($macinfo["isrenzheng"]==1 or $iswhite==1){
			if ($shanginfo["id"]==$macinfo["firstrid"]){$mactype=0;}else{$mactype=1;}
			$shangmacsql="insert into mx_shang_maclist(macid,shanghuid,biaoshi,mac,firsttime,lasttime,renzhengtype,isrenzheng,ispop,zongrenum,lastdotime,lastsetdoingtime,mactype,createdate)values('$macinfo[id]','$shanginfo[id]','$macinfo[biaoshi]','$macinfo[mac]','$hostthistime','$hostthistime','$macinfo[renzhengtype]','$macinfo[isrenzheng]','0','0','$hostthistime','$hostthistime','$mactype','$hostthistime')";
			$db->excu($shangmacsql);
			$maclogsql="insert into mx_mac_log (macid,shanghuid,rid,bw_up,bw_down,lastdotime,lastsetdoingtime,mactype,createdate,islast)values('$macinfo[id]','$shanginfo[id]','$Rid','0','0','$hostthistime','$hostthistime','$mactype','$hostthistime','1')";
			$resultarr=array("res"=>"1");
			//$resultarr=array("res"=>"2","bw_up"=>intval($shanginfo["bw_up"]),"bw_down"=>intval($shanginfo["bw_up"]),"canusetime"=>"$canusetime");
		}
		else{
			$resultarr=array("res"=>"1");
		}
	}
	if (trim($maclogsql)!=""){
	$db->excu($maclogsql);
	}
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	$db->excu("COMMIT TRANSACTION DEPS02_DEL");
	$result=json_encode($resultarr);
	return $result;
}


//Wifi用户认证查询接口
function wifi_getwifiuserre($Rid="",$uname="",$mac="",$sign=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:s");
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $mac=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	
	//获取设备信息
	$shebei=get_shebeiinfo_online($Rid," and shanghuid='$shanginfo[id]' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."' and iscandoing='1'");
	if (!is_array($shebei)){
		return $result;
	}
	//判断是否在白名单或者黑名单中
	if(strpos("##,".$shanginfo["whitelist"].",",",$mac,")){$iswhite=1;}
	if(strpos("##,".$shanginfo["blacklist"].",",",$mac,")){$isbalck=1;}
	$macarr=explode(",",$mac);
	$rconfig=$db->getinfo("select top 1 * from mx_rconfig ");
	$canusetime=$rconfig["treload"]*60;
	
	
	foreach ($macarr as $key =>$value){
		$restatus=0;
		$value=trim($value);
		$macinfo=$db->getinfo("select * from mx_shang_maclist where mac='$value' and shanghuid='$shanginfo[id]'");
		if (is_array($macinfo)){
			//生成认证状态
			if (intval($macinfo["ispop"])==1){$restatus=1;}
			if(strpos("##,".$shanginfo["whitelist"].",",",$value,")){$restatus=1;}
			if(strpos("##,".$shanginfo["blacklist"].",",",$value,")){$restatus=0;}
			if ($restatus==1){
				wifi_update_shangmaclog($Rid,$macinfo["macid"],0,0,0,$shebei,$macinfo,"");
			}
			$shanginfo["bw_up"]=intval($shanginfo["bw_up"]);
			$shanginfo["bw_down"]=intval($shanginfo["bw_down"]);
			$remac[]=array("mac"=>$value,"restatus"=>"$restatus","bw_up"=>"$shanginfo[bw_up]","bw_down"=>"$shanginfo[bw_up]",candotime=>"$canusetime");
		}
		else{
			$remac[]=array("mac"=>$value,"restatus"=>"0");
			wifi_setwifiuserre($Rid,$uname,$value,$sign);
		}
	}
	$resultarr["res"]="1";
	if (is_array($remac)){
	$resultarr["maclist"]=$remac;
	}
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	$result=json_encode($resultarr);
	return $result;
}

//用户在网信息
function wifi_setwifiuser($Rid="",$uname="",$wifilist="",$sign=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:s");
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $wifilist=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	
	//获取设备信息
	$shebei=get_shebeiinfo_online($Rid,"and shanghuid='$shanginfo[id]' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."' and iscandoing='1'");
	if (!is_array($shebei)){
		return $result;
	}
	
	$wifiarr=explode("@@",$wifilist);
	foreach ($wifiarr as $key =>$value){
		$value=trim($value);
		if ($value!=""){
			$thisarr=explode("@",$value);
			$macinfo=$db->getinfo("select * from mx_shang_maclist where mac='$thisarr[0]' and shanghuid='$shanginfo[id]'");
			if (is_array($macinfo)){
			wifi_update_shangmaclog($Rid,$macinfo["macid"],$thisarr[1],$thisarr[2],$thisarr[3],$shebei,$macinfo,"");
			}
		}
	}
	
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	$resultarr=array("res"=>"1");
	$result=json_encode($resultarr);
	return $result;
}

//更新mac最后状态和上网总量
function wifi_update_shangmaclog($Rid=0,$macid=0,$bw_up=0,$bw_down=0,$zongtime=0,$shebei="",$shagmacinfo="",$macinfo=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:00");
	$macid=intval($macid);
	if ($macid>0){
	
		if (!is_array($shebei)){
			$shebei=get_shebeiinfo_online($Rid," and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."' and iscandoing='1'");
			if (!is_array($shebei)){
				exit();
			}
		}
		if (!is_array($macinfo)){
			$macinfo=$db->getinfo("select * from mx_maclist where id='$macid'");
			if (!is_array($macinfo)){
				exit();
			}
		}
		if (!is_array($shagmacinfo)){
			$shagmacinfo=$db->getinfo("select * from mx_shang_maclist where macid='$macid' and shanghuid='$shebei[id]'");
			if (!is_array($shagmacinfo)){
				exit();
			}
		}
		$db->excu("BEGIN TRANSACTION DEPS02_DEL");
		//更新maclog
		$db->excu("update mx_mac_log set bw_up='$bw_up',bw_down='$bw_down',zongtime='$zongtime',lastdotime='$hostthistime' where macid='$macinfo[id]' and shanghuid='$shagmacinfo[shanghuid]' and rid='$Rid' and islast='1'");
		//计算相隔时间
		$shangcha=get_time_miao($shagmacinfo["lastsetdoingtime"],$hostthistime);
		$shangcha=intval($shangcha/60);
		$maccha=get_time_miao($macinfo["lastsetdoingtime"],$hostthistime);
		$maccha=intval($maccha/60);
		//更新商户mac列表
		$db->excu("update mx_shang_maclist set zongtime=zongtime+{$shangcha},lastdotime='$hostthistime',lastsetdoingtime='$hostthistime',isonline='1'  where macid='$macinfo[id]' and shanghuid='$shagmacinfo[shanghuid]'");
		if ($shagmacinfo["mactype"]==1){$macsql=",manyoutime=manyoutime+{$maccha}";}
		//更新mac列表
		$db->excu("update mx_maclist set lastdotime='$hostthistime',lastsetdoingtime='$hostthistime',isonline='1',zonlinetime=zonlinetime+{$maccha}  {$macsql} where id='$macinfo[id]'");
		$db->excu("COMMIT TRANSACTION DEPS02_DEL");
	}
}

//获取wan口流量
function wifi_setwaninfo($Rid="",$uname="",$bw_up=0,$bw_down=0,$sign=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:00");
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $sign=="" or ($bw_up==0 and $bw_up==0)){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	//获取设备信息
	$shebei=$db->getinfo("select isrestart from mx_view_shebei_isrestart where id='$Rid' and shanghuid='$shanginfo[id]'");
	if (!is_array($shebei)){
		return $result;
	}
	
	$shebeilog=$db->getinfo("select * from mx_view_shebeilog_last where id='$Rid' and shanghuid='$shanginfo[id]' and islast='1'");
	if (!is_array($shebei)){
		return $result;
	}
	$db->excu("BEGIN TRANSACTION DEPS02_DEL");
	//计算相隔时间
	$shangcha=get_time_miao($shebeilog["lastdotime"],$hostthistime);
	$shangcha=intval($shangcha/60);
	

	$bw_up_cha=$bw_up-$shebeilog["$bw_up_last"];
	$bw_down_cha=$bw_down-$shebeilog["$bw_down_last"];
	$upsql=",bw_up=bw_up+$bw_up_cha,bw_down=bw_up+$bw_down_cha";
	//更新shebeilog
	$db->excu("update mx_shenbei_log set bw_up='$bw_up',bw_down='$bw_down',bw_up_last='$bw_up',bw_down_last='$bw_down',zongtime=zongtime+$shangcha,lastdotime='$hostthistime' where shanghuid='$shanginfo[id]' and rid='$Rid' and islast='1'");
	wifi_r_updatedoing($Rid,$upsql,$shebei);
	$db->excu("COMMIT TRANSACTION DEPS02_DEL");
	
	$resultarr=array("res"=>"1");
	$result=json_encode($resultarr);
	return $result;
}

//获取黑白名单
function wifi_getmaclist($Rid="",$uname="",$sign=""){
	global $db;
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	//获取设备信息
	$shebei=$db->getinfo("select isrestart from mx_view_shebei_isrestart where id='$Rid' and shanghuid='$shanginfo[id]'");
	if (!is_array($shebei)){
		return $result;
	}
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	$resultarr=array("res"=>"1","whitelist"=>$shanginfo["whitelist"],"blacklist"=>$shanginfo["blacklist"]);
	$result=json_encode($resultarr);
	return $result;
}

//获取设备是否重启
function wifi_getrestart($Rid="",$uname="",$sign=""){
	global $db;
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	
	//获取设备信息
	$shebei=$db->getinfo("select isrestart from mx_view_shebei_isrestart where id='$Rid' and shanghuid='$shanginfo[id]'");
	if (!is_array($shebei)){
		return $result;
	}
	$shebei["isrestart"]=intval($shebei["isrestart"]);
	$resultarr["res"]="$shebei[isrestart]";
	$changestr="";
	if ($shebei["changestr"]!=""){
		$changearr=explode(";",$shebei["changestr"]);
		foreach ($changearr as $key =>$value){
			if ($value!="" and $value!="getrestart"){
				if ($changestr==""){$changestr=$value;}
				else{$changestr.=";".$value;}
			}
		}
	}
	
	if ($changestr==""){$ischange=0;}
	else{$ischange=1;}
	$db->excu("update mx_shebei set isrestart='0',ischange='$ischange',changestr='$changestr' where id='$Rid' and shanghuid='$shanginfo[id]' and isrestart='1'");
	
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	$result=json_encode($resultarr);
	return $result;
}
//获取设备是否远程升级
function wifi_getupgrade($Rid="",$uname="",$sign=""){
	global $db;
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	
	//获取设备信息
	$shebei=$db->getinfo("select isupgrade from mx_view_shebei_isupgrade where id='$Rid' and shanghuid='$shanginfo[id]'");
	if (!is_array($shebei)){
		return $result;
	}
	$rconfig=$db->getinfo("select top 1 * from mx_rconfig ");
	if(trim($rconfig["driveurl"])==""){
		return $result;
	}
	$shebei["isupgrade"]=intval($shebei["isupgrade"]);
	$resultarr["res"]="$shebei[isupgrade]";
	$resultarr["geturl"]=trim($rconfig["driveurl"]);
	$resultarr["md5str"]=trim($rconfig["driverversionmd5"]);
	
	$changestr="";
	if ($shebei["changestr"]!=""){
		$changearr=explode(";",$shebei["changestr"]);
		foreach ($changearr as $key =>$value){
			if ($value!="" and $value!="getupgrade"){
				if ($changestr==""){$changestr=$value;}
				else{$changestr.=";".$value;}
			}
		}
	}
	if ($changestr==""){$ischange=0;}
	else{$ischange=1;}
	$db->excu("update mx_shebei set isupgrade='0',ischange='$ischange',changestr='$changestr' where id='$Rid' and shanghuid='$shanginfo[id]' and isupgrade='1'");
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	
	$result=json_encode($resultarr);
	return $result;
}
//获取设备是否有更新
function wifi_getrchange($Rid="",$uname="",$sign=""){
	global $db;
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	$shebei=get_shebeiinfo_use($Rid," and shanghuid='$shanginfo[id]' ");
	
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	if (trim($shebei["changestr"])==""){
		$resultarr=array("res"=>"2","changelist"=>"");
	}else{
		$resultarr=array("res"=>"1","changelist"=>trim($shebei["changestr"]));
	}
	$result=json_encode($resultarr);
	return $result;
}
//获取设备驱动版本号
function wifi_setupgrade($Rid="",$uname="",$driverversion="",$sign=""){
	global $db;
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	//判断参数是否完整
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $driverversion=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=get_shanghuinfo_isok($uname,""," and isok='1' and startdate<='".date("Y-m-d 23:59:59")."' and enddate>='".date("Y-m-d 00:00:00")."'");
	if (!is_array($shanginfo)){
		return $result;
	}
	
//	//获取设备信息
//	$shebei=$db->getinfo("select isupgrade from mx_view_useshebei where id='$Rid' and shanghuid='$shanginfo[id]'");
//	if (!is_array($shebei)){
//		return $result;
//	}
	
	$db->excu("update mx_shebei set driverversion='$driverversion' where id='$Rid' and shanghuid='$shanginfo[id]'");
	
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	$resultarr=array("res"=>"1");
	$result=json_encode($resultarr);
	return $result;
}
//获取设备基本配置
function wifi_getchangeRconfig($Rid=0,$uname="",$sign=""){
	global $db;
	$hostthistime=date("Y-m-d H:i:s");
	//初始错误字符
	$resultarr=array("res"=>"0");
	$result=json_encode($resultarr);
	$Rid=intval($Rid);
	if ($Rid==0 or $uname=="" or $sign==""){
		return $result;
	}
	//判断验证参数是否正确
	if ($sign!=md5("$Rid{mx}{wei}$uname")){
		return $result;
	}
	//获取商户
	$shanginfo=$db->getinfo("select top 1 * from mx_view_shanghu where sname='$uname'");
	if (!is_array($shanginfo)){
		return $result;
	}
	//获取设备是否激活
	$shebei=get_shebeiinfo_online($Rid," and shanghuid='$shanginfo[id]' ");
	if (!is_array($shebei)){
		return $result;
	}
	//判断是否在有效期内
	if (formatdate($shebei["startdate"])>date("Y-m-d") or formatdate($shebei["enddate"])<date("Y-m-d") or formatdate($shanginfo["startdate"])>date("Y-m-d") or formatdate($shanginfo["enddate"])<date("Y-m-d") or $shanginfo["isok"]==0){
		$shebei["iscandoing"]=0;
	}
	
	//更新设备状态
	wifi_r_updatedoing($Rid,"",$shebei);
	
	$rconfig=$db->getinfo("select top 1 * from mx_rconfig ");
	
	$resultarr=array("res"=>"1","rid"=>$Rid,"ssid"=>$shebei["ssid"],"iscandoing"=>$shebei["iscandoing"],"reurl"=>$shebei["reurl"],"whitelist"=>$shebei["whitelist"],"blacklist"=>$shebei["blacklist"],"tshebeirestart"=>$rconfig["tshebeirestart"],"twifire"=>$rconfig["twifire"],"twifiuser"=>$rconfig["twifiuser"],"twhitelist"=>$rconfig["twhitelist"],"tblacklist"=>$rconfig["tblacklist"],"trestart"=>$rconfig["trestart"],"tupgrade"=>$rconfig["tupgrade"],"tchange"=>$rconfig["tchange"],"treload"=>$rconfig["treload"],"tfree"=>$rconfig["tfree"],"wanopen"=>1);
	$resultarr=trim_array($resultarr);
	
	$result=json_encode($resultarr);
	return $result;
}



?>