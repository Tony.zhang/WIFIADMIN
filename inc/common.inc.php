<?php
/*
  * @author 小白 <chuanwei1234@163.com>  
  * @QQ 86844077  
  * @date 2014-01-13
*/
@date_default_timezone_set('PRC'); //东八时区
error_reporting(7);
set_magic_quotes_runtime(0);

$speed_headtime=explode(' ',microtime());
$speed_headtime=$speed_headtime[0]+$speed_headtime[1];

if(PHP_VERSION < '4.1.0') {
	$_GET = &$HTTP_GET_VARS;
	$_POST = &$HTTP_POST_VARS;
	$_COOKIE = &$HTTP_COOKIE_VARS;
	$_SERVER = &$HTTP_SERVER_VARS;
	$_ENV = &$HTTP_ENV_VARS;
	$_FILES = &$HTTP_POST_FILES;
}

if(!get_magic_quotes_gpc()){
	$_POST=Add_S($_POST);
	$_GET=Add_S($_GET);
	$_COOKIE=Add_S($_COOKIE);
}
function Add_S($array){
	foreach($array as $key=>$value){
		if(!is_array($value)){
			$array[$key]=addslashes($value);
		}else{
			$array[$key]=Add_S($array[$key]);
		}
	}
	return $array;
}

if(!ini_get('register_globals')){
	@extract($_COOKIE,EXTR_SKIP);
	@extract($_FILES,EXTR_SKIP);
}

foreach($_POST as $_key=>$_value){
	!ereg("^\_[A-Z]+",$_key) && $$_key=$_POST[$_key];
}
foreach($_GET as $_key=>$_value){
	!ereg("^\_[A-Z]+",$_key) && $$_key=$_GET[$_key];
}
define('MX_PATH', substr(dirname(__FILE__), 0, -4).'/');

require_once(MX_PATH."inc/config.php");
require_once(MX_PATH."inc/mssql.php");
global $db;
$db=new mssql();
$db->link(DB_SEREVER,DB_DBNAME,DB_USER,DB_PASSWORD,DB_PORT);


global $webdb;
$webrs=$db->excu("select * from mx_config");
if ($db->num_rows($webrs)>0){
	while ($webrsdb=$db->fetch_array($webrs)){
		$webdb[$webrsdb["c_key"]]=addslashes($webrsdb["c_value"]);
	}
}


require_once(MX_PATH."inc/function.php");
require_once(MX_PATH."inc/function_wifi.php");
require_once(MX_PATH."inc/daili.php");
require_once(MX_PATH."inc/shanghu.php");
require(MX_PATH."inc/class.inc.php");
require(MX_PATH."inc/checkip.php");
global $FROMURL,$WEBURL;
$PHP_SELF_TEMP=$_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
$PHP_SELF=$_SERVER['REQUEST_URI']?$_SERVER['REQUEST_URI']:$PHP_SELF_TEMP;
$HTTP_HOST=$_SERVER['HTTP_HOST']?$_SERVER['HTTP_HOST']:$HTTP_SERVER_VARS['HTTP_HOST'];
$WEBURL='http://'.$HTTP_HOST.$PHP_SELF;
$FROMURL=$_SERVER["HTTP_REFERER"]?$_SERVER["HTTP_REFERER"]:$HTTP_SERVER_VARS["HTTP_REFERER"];
if($webdb['web_open']==0 and strpos("$PHP_SELF","mxsoft")<=0){
    @header('Content-Type:text/html;charset= gb2312');
    echo $webdb['close_why'];
	exit();
}

$FROMURL=filtrate($FROMURL);
$WEBURL=filtrate($WEBURL);
global $fun,$DL,$SH;
$fun=new myfunction;
$DL=new daili_class();
$SH=new shanghu_class();
global $Guidedb;
$Guidedb=new Guide_DB;
/*
* 全局变量
*/
global $FENJIWAIYU;

?>
