<?php
require_once("inc/common.inc.php");

if($actions=="getRconfig"){
	//设备认证
	$rmac=trim($rmac);
	$uname=trim($uname);
	$sign=trim($sign);
	$result=wifi_getRconfig($rmac,$uname,$sign);
	echo $result;
}
elseif ($actions=="setwifiuserre"){
	//初次接入
	$Rid=trim($Rid);
	$uname=trim($uname);
	$mac=trim($mac);
	$sign=trim($sign);
	$result=wifi_setwifiuserre($Rid,$uname,$mac,$sign);
	echo $result;
}
elseif ($actions=="getwifiuserre"){
	//获取用户认证状态
	$Rid=trim($Rid);
	$uname=trim($uname);
	$mac=trim($mac);
	$sign=trim($sign);
	$result=wifi_getwifiuserre($Rid,$uname,$mac,$sign);
	echo $result;
}
elseif ($actions=="setwifiuser"){
	//更新在网用户信息
	$Rid=trim($Rid);
	$uname=trim($uname);
	$wifilist=trim($wifilist);
	$sign=trim($sign);
	$result=wifi_setwifiuser($Rid,$uname,$wifilist,$sign);
	echo $result;
}
elseif ($actions=="getmaclist"){
	//获取黑白名单
	$Rid=trim($Rid);
	$uname=trim($uname);
	$sign=trim($sign);
	$result=wifi_getmaclist($Rid,$uname,$sign);
	echo $result;
}
elseif ($actions=="setwaninfo"){
	//WAN口流量
	$Rid=trim($Rid);
	$uname=trim($uname);
	$bw_up=(float)$bw_up;
	$bw_down=(float)$bw_down;
	$sign=trim($sign);
	$result=wifi_setwaninfo($Rid,$uname,$bw_up,$bw_down,$sign);
	echo $result;
}
elseif ($actions=="getrestart"){
	//获取设备是否重启
	$Rid=trim($Rid);
	$uname=trim($uname);
	$sign=trim($sign);
	$result=wifi_getrestart($Rid,$uname,$sign);
	echo $result;
}
elseif ($actions=="getupgrade"){
	//获取设备是否远程升级
	$Rid=trim($Rid);
	$uname=trim($uname);
	$sign=trim($sign);
	$result=wifi_getupgrade($Rid,$uname,$sign);
	echo $result;
}
elseif ($actions=="getrchange"){
	//获取设备是否更改配置
	$Rid=trim($Rid);
	$uname=trim($uname);
	$sign=trim($sign);
	$result=wifi_getrchange($Rid,$uname,$sign);
	echo $result;
}
elseif ($actions=="setupgrade"){
	//获取设备驱动版本号
	$Rid=trim($Rid);
	$uname=trim($uname);
	$driverversion=trim($driverversion);
	$sign=trim($sign);
	$result=wifi_setupgrade($Rid,$uname,$driverversion,$sign);
	echo $result;
}
elseif ($actions=="getchangeRconfig"){
	//获取设备是否更改配置
	$Rid=trim($Rid);
	$uname=trim($uname);
	$sign=trim($sign);
	$result=wifi_getchangeRconfig($Rid,$uname,$sign);
	echo $result;
}
else{
	$resultarr=array("res"=>"-1");
	echo json_encode($resultarr);
}
?>